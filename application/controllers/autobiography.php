<?php
  class Autobiography extends CI_Controller{
    function __construct()
    {
      parent::__construct();

      //model
      $this->load->model('front/main_model', 'mmodel');

      //library
      $this->load->library('common/initial', 'initial');
    }

    public function index($basicId = 0){

      //網頁View配置初始化
			$data=$this->initial->init();

			//是否登入過檢查
			// $loginMember = $this->session->userdata('loginMember'); 
			$memberLevel = $this->session->userdata('memberLevel');
			$userId = $this->session->userdata('userId');

      //登入後右側欄位的View變化
			if(!empty($memberLevel)){
				$data['memberLevel']=$memberLevel;
				if(!empty($userId)){
					$data['userId']=$userId;
				}
			}

      //頁面檢查功能分類
			$data['pageName']= "autobiography";

      //是否登入過檢查
			$loginMember = $this->session->userdata('loginMember'); 
			$memberLevel = $this->session->userdata('memberLevel');
			$userId = $this->session->userdata('userId');

      $menuItemContext1 = '登入會員系統';
      $menuItemIconName1 = 'menuIcon_login.png';
      $loginSucess = false;
      $data['loginChk']="fail";

      //登入成功
      if(!empty($loginMember) && !empty($memberLevel) && !empty($userId)){
        $menuItemContext1 = '登出會員系統';
        $menuItemIconName1 = 'menuIcon_logout.png';
        $loginSucess = true;
        $data['loginChk']="sucess";
        //登入資訊(登入時)
			  $memberData = $this->mmodel->getAllUserInfoByUserId($userId);
			  $data['memberName'] = $memberData[0]['m_name'];
			  $data['loginInfo'] = "您登入了".$memberData[0]['m_login']."次";
      }else{
        //登入資訊(登出時)
			  $data['memberName'] = "訪客";
			  $data['loginInfo'] = "您尚未登入";
      }
      //右側Menu項目
			$menu_arr = array('menuBtn_login'=>$menuItemContext1, 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_forgotPw'=>"忘記密碼", 'menuBtn_applyMember'=>"申請會員");
			//右側Menu項目icon檔名
			$icon_arr = array('menuBtn_login'=>$menuItemIconName1, 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_forgotPw'=>"menuIcon_forgot.png", 'menuBtn_applyMember'=>"menuIcon_apply.png");
      if($loginSucess){
        unset($menu_arr["menuBtn_forgotPw"]);
        unset($menu_arr["menuBtn_applyMember"]);
        unset($icon_arr["menuBtn_forgotPw"]);
        unset($icon_arr["menuBtn_applyMember"]);
      }
			//右側Menu印出內文
			$data['menuContext'] = $this->initial->menuBarPrintedContext($menu_arr, $icon_arr, $data['images_root']);

      //頁面資料
      $cityInfo= $this->mmodel->getCityInfoBy();
      $data['cityInfo'] = $cityInfo;

      //出生年月日選擇資料產生
      for($y=1911; $y<=intval(date('Y')); $y++){
        $data['birthYear'][$y] = "西元 ".$y."/民國 ".(intval($y)-1911);
      }

      for($m=1; $m<=12; $m++){
        $data['birthMonth'][$m] = $m;
      }

      //軍種選擇
      $data['militaryService'] = array(1=>"陸軍", 2=>"海軍", 3=>"空軍", 4=>"憲兵");

      //編輯預設資料呈現
      if($basicId != 0){
        //如已經登入，則紀錄寫入Session，視窗不再出現
        $this->session->set_userdata("loginAutobiography", "sucess");
        $data['loginAutobiography'] = $this->session->userdata("loginAutobiography");

        $lastData = $this->mmodel->getAutobiographyData($basicId, 'ASC');
        $data['basicData'] = $lastData[0];

        //生日資料的處理
        $data['birthYearVal'] = substr($lastData[0]["birthday"], 0, 4);
        $data['birthMonthVal'] = substr($lastData[0]["birthday"], 5, 2);
        $data['birthDayVal'] = substr($lastData[0]["birthday"], 8, 2);

        //最後工作經驗與最高學歷
        $workingExpDataColumnArr = array('job-name', 'working-term-start', 'working-term-end', 'company-name', 'job-title','working-position', 'working-content');
        $educationDataColumnArr = array('school-name', "learning-term-start", "learning-term-end", 'dept-name', "graduate-status");
        $workingExpData = $this->mmodel->getAllExpInfo($workingExpDataColumnArr, $basicId, "autobiography_working_exp", "autobiography-working-exp-id");//工作經驗
        $educationData = $this->mmodel->getAllExpInfo($educationDataColumnArr, $basicId, "autobiography_education", "autobiography-education-id");//學歷

        $data["lastCompanyName"] = $workingExpData[0]["company-name"];
        $data["lastJobName"] = $workingExpData[0]["job-name"];
        $data["highestEducationName"] = $educationData[0]["school-name"];
        $data["deptHighestEducation"] = $educationData[0]["dept-name"];

        //資料轉換:聯絡方式
        foreach(json_decode($lastData[0]["contact-method"]) as $key => $checkedId){
          $data["contactMethod".$checkedId] = $checkedId;
        }
        //資料轉換:駕駛執照
        foreach(json_decode($lastData[0]["driving-licence"]) as $key => $checkedId){
          $data["drivingLicence".$checkedId] = $checkedId;
        }
        //資料轉換:交通工具
        foreach(json_decode($lastData[0]["driving-tool"]) as $key => $checkedId){
          $data["drivingTool".$checkedId] = $checkedId;
        }

        //資料轉換:居住位置
        $livingPosition = $lastData[0]["living-position"];
        $cityPosition = substr($livingPosition, 0, 9);
        $districtPosition = substr($livingPosition, 9, strlen($livingPosition));
        $data['cityPosition'] = $cityPosition; //城市名
        $data['districtPosition'] = $districtPosition; //行政區名
        $addressPre = $cityPosition.$districtPosition;
        $totalAddr = $lastData[0]["address"];
        $data['addressSuf'] = substr($totalAddr, strlen($addressPre), strlen($totalAddr)); //地址後段
        $cityInfo = $this->mmodel->getCityInfoBy('name', $cityPosition);
        $districtInfo = $this->mmodel->getDistrictBy('name', $districtPosition);
        $data['cityId'] = $cityInfo[0]['id'];
        $data['districtId'] = $districtInfo[0]['id'];
        $data['districtArr'] = $this->mmodel->getDistrictInfo($cityInfo[0]['id']);

        //資料處理:聯絡時段
        $data['contactMomentStart'] = substr($lastData[0]["contact-moment-start"], 0, 5);
        $data['contactMomentEnd'] = substr($lastData[0]["contact-moment-end"], 0, 5);

        //去除學經歷資料重複的部分
        foreach($educationData as $key=> $arr){
          if($educationData[$key] == $educationData[$key-1]){
            unset($educationData[$key]);
          }
        }
        foreach($workingExpData as $key=> $arr){
          if($workingExpData[$key] == $workingExpData[$key-count($educationData)]){
            unset($workingExpData[$key]);
          }
        }
        //學歷View的新增
        for($i=2; $i<count($educationData)+1; $i++){
          $data['educationView'][$i] = $this->getEducationView($i);
        }

        //工作經歷View的新增
        for($i=2; $i<count($workingExpData)+1; $i++){
          $data['workingExpView'][$i] = $this->getWorkingExpView($i);
        }

        $itemNum1 = 1;
        $itemNum2 = 1;

        //學歷資料 
        foreach($educationData as $arr){
          $data['educationData'][$itemNum1] = $arr;
          $itemNum1++;
        }
        //工作經歷資料
        foreach($workingExpData as $key => $arr){
          $data['workingExpData'][$itemNum2] = $arr;
          foreach($arr as $index => $val){
            if($index == "working-position"){
              $cityData = $this->mmodel->getCityInfoBy("name", substr($val, 0, 9));
              $districtData = $this->mmodel->getDistrictBy('name', substr($val, 9, strlen($val)));
              $data['workingExpData'][$itemNum2]["working-position-city"] = $cityData[0]['id'];
              $data['workingExpData'][$itemNum2]["working-position-district"] = $districtData[0]['id'];
            }
          }
          $itemNum2++;
        }
      }

      $this->load->view('front/home_header', $data);
      $this->load->view('front/autobiography_body_header', $data);
      $this->load->view('front/autobiography_body_1', $data);
      $this->load->view('front/autobiography_body_2', $data);
      $this->load->view('front/autobiography_body_3', $data);
      $this->load->view('front/autobiography_body_footer', $data);
      $this->load->view('front/autobiography_body_menu', $data);
      $this->load->view('front/home_footer', $data);
      
    }

    public function addEducation(){
      echo $this->getEducationView($_POST['data'][0]);
    }

    public function getEducationView($addNum){

      $result = $this->mmodel->getAutobiographyViewById(1);
      $html = $result[0]['content'];

      return str_replace('addNum', $addNum, $html);
    } 

    public function addWorkExperience(){
      echo $this->getWorkingExpView($_POST['data'][0]);
    }

    public function getWorkingExpView($addNum){
      $result1 = $this->mmodel->getAutobiographyViewById(2);
      $result2 = $this->mmodel->getAutobiographyViewById(3);
      $html1 = $result1[0]['content'];
      $html2 = $result2[0]['content'];
      $newHtml1 = str_replace('addNum', $addNum, $html1);
      $newHtml2 = str_replace('addNum', $addNum, $html2);

      $cityInfo= $this->mmodel->getCityInfoBy();
      $optionStr = '';
      foreach($cityInfo as $infoArr){
        $optionStr .='<option value="'.$infoArr['id'].'">'.$infoArr['name'].'</option>';
      }
      return $newHtml1.$optionStr.$newHtml2;
    }

    public function selectDistrict(){
      if($_SERVER['REQUEST_METHOD'] == "POST"){
        if($_POST['selectId'] != 0){
          $result = $this->mmodel->getDistrictInfo($_POST['selectId']);
          echo json_encode($result);
        }else{
          echo '資料選擇有誤!';
        }
      }else{
        echo '非法資料傳遞!';
      }
    }
    
    public function selectDay(){
      if($_SERVER['REQUEST_METHOD'] == "POST"){
        if($_POST['selectYear'] != 0){
          if($_POST['selectMonth'] != 0){
            $lastDayMonth = 0;
            $selectYear = $_POST['selectYear'];
            $selectMonth = $_POST['selectMonth'];
            switch($selectMonth){
              case 1:
              case 3:
              case 5:
              case 7:
              case 8:
              case 10:
              case 12:
                $lastDayMonth = 31;
                break;
              case 2:
                $lastDayMonth = 28;
                if($selectYear % 4 == 0){
                  $lastDayMonth = 29;
                }
                break;
              case 4:
              case 6:
              case 9:
              case 11:    
                $lastDayMonth = 30;
                break;               
            }
            echo $lastDayMonth;
          }else{
            echo '請選擇出生月份!';
          }
        }else{
          echo '請選擇出生年份!';
        }
      }else{
        echo '非法資料傳遞!';
      }
    }

    public function edit($basicId = 0){

      $inputData = array();
      $jsonStr1 = "";
      $jsonStr2 = "";
      $jsonStr3 = "";
      $resultArr = array();
      $msg = '';
      $tmpStr = '';
      $unsetKeyArray1 = array("exempt-service-reason-2", "living-city-position","living-district-position", "birth-year-selector", "birth-month-selector", "birth-day-selector","address-1", "address-2", "address-3", "contact-method-1", "contact-method-2", "contact-method-3", "driving-licence-1", "driving-licence-2", "driving-licence-3", "driving-tool-1", "driving-tool-2", "driving-tool-3");
      $unsetKeyArray2 = array("school-name", "learning-term-start", "learning-term-end", "dept-name", "graduate-status", "job-name", "working-term-start", "working-term-end", "company-name", "job-title", "working-position-city", "working-position-district", "working-content");

      if($_SERVER['REQUEST_METHOD'] == 'POST'){
        if($basicId == 0){ //新增資料
          foreach($_POST as $key => $data){
            $tmpArr = array();
            //一般資料存入陣列
            $inputData[$key] = $data;
            // 一般資料(需重處理)存入陣列
            $inputData = $this->inputBasicData($inputData, $key, $data);

            //生日日期資料處理
            if(strpos($key, "birth") !== false){
              $tmpStr .= $data."-";
              $inputData["birthday"] = substr($tmpStr, 0, strlen($tmpStr)-1);
            }

            //聯絡方式資料處理
            if(strpos($key, "contact-method") !== false && $key != "other-contact-method"){
              $jsonStr1 .= '"'.$data.'",';
            }
            //駕照資料處理
            if(strpos($key, "driving-licence") !== false && $key != "other-driving-licence"){
              $jsonStr2 .= '"'.$data.'",';
            }
            //駕駛工具資料處理
            if(strpos($key, "driving-tool") !== false && $key != "other-driving-tool"){
              $jsonStr3 .= '"'.$data.'",';
            }

            // 學歷資料處理
            $inputData = $this->inputEducationData($inputData, $key, $data);
            // 工作經歷資料處理
            $inputData = $this->inputWorkingExpData($inputData, $key, $data);
            //工作區域資料處理
            if(strpos($key, "working-position-city") !== false){
              $tmpArr = $this->mmodel->getCityInfoBy('id', $data);
              $index = substr($key, strlen($key)-1);
              $inputData['working-exp'][$index]["working-position"] = $tmpArr[0]["name"];
            }
            if(strpos($key, "working-position-district") !== false){
              $tmpArr = $this->mmodel->getDistrictBy('id', $data);
              $index = substr($key, strlen($key)-1);
              $inputData['working-exp'][$index]["working-position"] .= $tmpArr[0]["name"];
            }
          }
  
          //將所需資料組為Json格式 
          $inputData["contact-method"] = $this->combineJsonStr($jsonStr1);
          $inputData["driving-licence"] = $this->combineJsonStr($jsonStr2);
          $inputData["driving-tool"] = $this->combineJsonStr($jsonStr3);
  
          //移除不必要的資料
          $inputData = $this->unsetUselessData2($inputData, $unsetKeyArray1);
          $inputData = $this->unsetUselessData2($inputData, $unsetKeyArray2);
  
          $foreignKeyArr1 = array();
          $foreignKeyArr2 = array();
  
          //資料寫入至Autobiography Education表
          foreach($inputData['education'] as $key => $arr){
            array_push($foreignKeyArr1, $this->mmodel->addAutobiographyEducationData($arr));
          }
  
          //資料寫入至Autobiography Education表
          foreach($inputData['working-exp'] as $key => $arr){
            array_push($foreignKeyArr2, $this->mmodel->addAutobiographyWorkingExpData($arr));
          }
  
          //資料寫入至Autobiography Basic表
          $tmpInputData = $inputData;
          $result = true;
          unset($tmpInputData["education"]);
          unset($tmpInputData["working-exp"]);
  
          $basicId = $this->mmodel->addAutobiographyBasicData($tmpInputData);
  
          foreach($foreignKeyArr1 as $key1){
            foreach($foreignKeyArr2 as $key2){
              array_push($resultArr, $this->mmodel->addAutobiographyIntermediateTableData($basicId, $key1, $key2));
            }
          }

          foreach($resultArr as $checkVal){
            if($checkVal == false){
              $result = false;
              break;
            }
          }
  
          if($result){
            $msg = $basicId;
          }else{
            $msg = 'fail';
          }

        }else{ //編輯資料
          $columnArray1 = array("school-name", "learning-term-start", "learning-term-end", "dept-name", "graduate-status");
          $columnArray2 = array("job-name", "working-term-start", "working-term-end", "company-name", "job-title", "working-position-city", "working-position-district", "working-content");
          $columnArray3 = array("real-name", "living-city-position", "living-district-position", "hope-job", "gender", "age", "birth-year-selector", "birth-month-selector", "birth-day-selector", "military-service-status", "military-service", "exempt-service-reason-2", "job-status", "mobile-phone", "telphone", "address","eng-name", "contact-method", "other-contact-method", "contact-moment-start", "contact-moment-end", "driving-licence", "other-driving-licence", "driving-tool", "other-driving-tool", "height", "weight");
          $editDataArr1 = array(); //學歷資料
          $editDataArr2 = array(); //工作經歷資料
          $editDataArr3 = array(); //基本資料
          $tmpArr = array();
          foreach($_POST as $key => $data){
            foreach($columnArray1 as $columnName){
              if(strpos($key, $columnName) > -1){
                $editDataArr1[$key] = $data;
                $editDataArr1 = $this->inputEducationData($editDataArr1, $key, $data);
              }
            }
            foreach($columnArray2 as $columnName){
              if(strpos($key, $columnName) > -1){
                $editDataArr2[$key] = $data;
                //工作區域資料處理
                $editDataArr2 = $this->inputWorkingExpData($editDataArr2, $key, $data);
                if(strpos($key, "working-position-city") !== false){
                  $tmpArr = $this->mmodel->getCityInfoBy('id', $data);
                  $index = substr($key, strlen($key)-1);
                  $editDataArr2['working-exp'][$index]["working-position"] = $tmpArr[0]["name"];
                }
                if(strpos($key, "working-position-district") !== false){
                  $tmpArr = $this->mmodel->getDistrictBy('id', $data);
                  $index = substr($key, strlen($key)-1);
                  $editDataArr2['working-exp'][$index]["working-position"] .= $tmpArr[0]["name"];
                }
              }
            }
            foreach($columnArray3 as $columnName){
              if(strpos($key, $columnName) > -1){
                // 其他資料處理
                $editDataArr3 = $this->inputBasicData($editDataArr3, $key, $data);
                // 一般資料處理
                $editDataArr3[$key] = $data;
                //生日日期資料處理
                if(strpos($key, "birth") !== false){
                  $tmpStr .= $data."-";
                  $editDataArr3["birthday"] = substr($tmpStr, 0, strlen($tmpStr)-1);
                }
                //聯絡方式資料處理
                if(strpos($key, "contact-method") !== false && $key != "other-contact-method"){
                  $jsonStr1 .= '"'.$data.'",';
                }
                //駕照資料處理
                if(strpos($key, "driving-licence") !== false && $key != "other-driving-licence"){
                  $jsonStr2 .= '"'.$data.'",';
                }
                //駕駛工具資料處理
                if(strpos($key, "driving-tool") !== false && $key != "other-driving-tool"){
                  $jsonStr3 .= '"'.$data.'",';
                }
              }
            }
          }

          //將所需資料組為Json格式 
          $editDataArr3["contact-method"] = $this->combineJsonStr($jsonStr1);
          $editDataArr3["driving-licence"] = $this->combineJsonStr($jsonStr2);
          $editDataArr3["driving-tool"] = $this->combineJsonStr($jsonStr3);
          //移除不必要的資料
          $editDataArr1 = $this->unsetUselessData2($editDataArr1, $unsetKeyArray2);
          $editDataArr2 = $this->unsetUselessData2($editDataArr2, $unsetKeyArray2);
          $editDataArr3 = $this->unsetUselessData($editDataArr3, $unsetKeyArray1);
          //寫入資料表
          $idArr1 = array();
          $idArr2 = array();

          foreach($editDataArr1['education'] as $index => $arr){ //寫入autobiography_education 表
            $resultArr = $this->mmodel->getAutobiographyEducationData('school-name', $arr['school-name']);
            if(empty($resultArr)){
              array_push($idArr1, $this->mmodel->addAutobiographyEducationData($arr));
            }else{
              $this->mmodel->updateAutobiographyEducation($arr, $resultArr[0]['id']);
            }
          }

          foreach($editDataArr2['working-exp'] as $index => $arr){ //寫入autobiography_working_exp 表
            $resultArr = $this->mmodel->getAutobiographyWorkingExpData('company-name', $arr['company-name']);
            if(empty($resultArr)){
              array_push($idArr2, $this->mmodel->addAutobiographyWorkingExpData($arr));
            }else{
              $this->mmodel->updateAutobiographyWorkingExp($arr, $resultArr[0]['id']);
            }
          }

          //寫入autobiography_basic_edu_working 表
          $idDataArr = $this->mmodel->getAutobiographyIntermediateTableData($basicId);
          if(empty($idArr1) && !empty($idArr2)){// 當學歷未新增項目時
            foreach($idDataArr as $arr){
              array_push($idArr1, $arr['autobiography-education-id']);
            }
          }else if(!empty($idArr1) && empty($idArr2)){// 當工作經歷未新增項目時
            foreach($idDataArr as $arr){
              array_push($idArr2, $arr['autobiography-working-exp-id']);
            }
          }
          foreach(array_unique($idArr1) as $id1){
            foreach(array_unique($idArr2) as $id2){
              $this->mmodel->addAutobiographyIntermediateTableData($basicId, $id1, $id2);
            }
          }
          
          //兵役狀態的資料處理
          if(($editDataArr3['military-service-status']) == 1){
            $editDataArr3['military-service'] = 0;
          }

          $result = $this->mmodel->updateAutobiographyBasic($editDataArr3, $basicId); //寫入autobiography_basic_ 表

          if($result){
            $msg = $basicId;
          }else{
            $msg = 'fail';
          }
        }
      }else{
        $msg = 'illegal';
      }
      echo $msg;
    }

    public function passwordValid(){
      if($_SERVER['REQUEST_METHOD'] == 'POST'){
        if(!empty($_POST['account'])){
          $memberData = $this->mmodel->getMemberDataBy("m_username", $_POST['account']);
          if(!empty($memberData)){
            if(!empty($_POST['password'])){
              if(base64_encode($_POST['password']) == $memberData[0]['m_passwd']){
                echo "sucess";
              }else{
                echo '密碼錯誤，請重新輸入!';
              }
            }else{
              echo '請輸入您的密碼!';
            }
          }else{
            echo '查無此 "'.$_POST['account'].' "帳號!';
          }
        }else{
          echo '請輸入您的帳號!';
        }
      }else{
        echo '非法傳遞參數!';
      }
    }

    public function showAssistAreaTitle(){
      $headingNum = $_POST['headingNum'];
      $result = array();
      switch($headingNum){
        case "1":
          $result = $this->mmodel->getAutobiographyViewById(4);
          break;
        case "2":
          $result = $this->mmodel->getAutobiographyViewById(1);
          break;
        case "3":
          $result = $this->mmodel->getAutobiographyViewById(2);
          break;  
      }
      echo "\"".$result[0]["assist-title"]."\" 說明 : ";
    }

    public function showAssistAreaText(){
      $headingNum = $_POST['headingNum'];
      $result = array();
      switch($headingNum){
        case "1":
          $result = $this->mmodel->getAutobiographyViewById(4);
          break;
        case "2":
          $result = $this->mmodel->getAutobiographyViewById(1);
          break;
        case "3":
          $result = $this->mmodel->getAutobiographyViewById(2);
          break;  
      }
      echo $result[0]["assist-content"];
    }

    public function logoutAutobiographyEdit(){
      $this->session->unset_userdata("loginAutobiography");
      $loginAutobiography = $this->session->userdata("loginAutobiography");
      $loginMember = $this->session->userdata('loginMember');

      if(empty($loginAutobiography)){
        if(!empty($loginMember)){
          echo "sucess1";
        }else{
          echo "sucess2";
        }
      }else{
        echo "fail";
      }
    }

    private function unsetUselessData($inputData, $unsetKeyArray){
      foreach($inputData as $key => $data){
        foreach($unsetKeyArray as $unsetKey){
          if($key == $unsetKey){
            unset($inputData[$key]);
          }
        }
      }
      return $inputData;
    }
    private function unsetUselessData2($inputData, $unsetKeyArray){
      foreach($inputData as $key => $data){
        foreach($unsetKeyArray as $unsetKey){
          if(strpos($key, $unsetKey) > -1){
            unset($inputData[$key]);
          }
        }
      }
      return $inputData;
    }

    private function combineJsonStr($jsonStr){
      return '['.substr($jsonStr, 0, strlen($jsonStr)-1).']';
    }

    private function inputBasicData($inputData, $arrKey, $val){
      $tmpArr = array();

      //免役的理由
      if($arrKey == "exempt-service-reason-2"){
        $inputData["exempt-service-reason"] = $val;
      }
      //居住區域資料處理
      if($arrKey == "living-city-position"){
        $tmpArr = $this->mmodel->getCityInfoBy('id', $val);
        $inputData["living-position"] = $tmpArr[0]["name"];
      }
      if($arrKey == "living-district-position"){
        $tmpArr = $this->mmodel->getDistrictBy('id', $val);
        $inputData["living-position"] .= $tmpArr[0]["name"];
      }
      //居住詳細地址資料處理
      if($arrKey == "address-1" || $arrKey == "address-2" || $arrKey == "address-3"){
        $inputData["address"] .= $val;
      }

      return $inputData;
    }

    private function inputEducationData($inputData, $arrKey, $val){
      //學校名稱資料處理
      if(strpos($arrKey, "school-name") !== false){
        $index = substr($arrKey, strlen($arrKey)-1);
        $inputData['education'][$index]["school-name"] = $val;
      }
      //學校學習開始時間資料處理
      if(strpos($arrKey, "learning-term-start") !== false){
        $index = substr($arrKey, strlen($arrKey)-1);
        $inputData['education'][$index]["learning-term-start"] = $val;
      }
      //學校學習結束時間資料處理
      if(strpos($arrKey, "learning-term-end") !== false){
        $index = substr($arrKey, strlen($arrKey)-1);
        $inputData['education'][$index]["learning-term-end"] = $val;
      }
      //學校就學科系資料處理
      if(strpos($arrKey, "dept-name") !== false){
        $index = substr($arrKey, strlen($arrKey)-1);
        $inputData['education'][$index]["dept-name"] = $val;
      }
      //學校結業狀態資料處理
      if(strpos($arrKey, "graduate-status") !== false){
        $index = substr($arrKey, strlen($arrKey)-1);
        $inputData['education'][$index]["graduate-status"] = $val;
      }

      return $inputData;
    }

    private function inputWorkingExpData($inputData, $arrKey, $val){
      //工作職稱資料處理
      if(strpos($arrKey, "job-name") !== false){
        $index = substr($arrKey, strlen($arrKey)-1);
        $inputData['working-exp'][$index]["job-name"] = $val;
      }
      //工作開始時間資料處理
      if(strpos($arrKey, "working-term-start") !== false){
        $index = substr($arrKey, strlen($arrKey)-1);
        $inputData['working-exp'][$index]["working-term-start"] = $val;
      }
      //工作結束時間資料處理
      if(strpos($arrKey, "working-term-end") !== false){
        $index = substr($arrKey, strlen($arrKey)-1);
        $inputData['working-exp'][$index]["working-term-end"] = $val;
      }
      //公司名稱資料處理
      if(strpos($arrKey, "company-name") !== false){
        $index = substr($arrKey, strlen($arrKey)-1);
        $inputData['working-exp'][$index]["company-name"] = $val;
      }
      //職務資料處理
      if(strpos($arrKey, "job-title") !== false){
        $index = substr($arrKey, strlen($arrKey)-1);
        $inputData['working-exp'][$index]["job-title"] = $val;
      }
      //工作內容資料處理
      if(strpos($arrKey, "working-content") !== false){
        $index = substr($arrKey, strlen($arrKey)-1);
        $inputData['working-exp'][$index]["working-content"] = $val;
      }

      return $inputData;
    }

  }
?>