<?php

	class Home extends CI_Controller{
		
		public function __construct()
		{
			parent::__construct();

			//model
			$this->load->model("front/main_model","mmodel");

			//libraries
			$this->load->library("common/initial");
			$this->load->library('tools/basicTool', '','basicTool');

			//helper
			$this->load->helper('cookie');

		}

		public function index($page='1', $loginChk=''){

			//網頁View配置初始化
			$data=$this->initial->init();

			//是否登入過檢查
			$loginMember = $this->session->userdata('loginMember'); 
			$memberLevel = $this->session->userdata('memberLevel');
			$userId = $this->session->userdata('userId');

			//頁面檢查功能分類
			$data['pageName']= "home";

			//檢查是否經過登入，若有登入則重新導向
			if(isset($loginMember) && $loginMember != "" && $loginChk == 'sucess'){

				if($memberLevel=="admin"){
					header("Location: /home/haveLogin/admin/".$userId."/".$page);
				}else if($memberLevel=="member"){
					header("Location: /home/haveLogin/member/".$userId."/".$page);
				}

			}

			//登入如果錯誤
			if(isset($loginChk)){
				if($loginChk=='acc_err'){
					$data['errMsg']="acc_error";
				}
				if($loginChk=='pw_err'){
					$data['errMsg']="pw_error";
				}

				if($loginChk=='timeout'){
					$data['errMsg']="timeout";
				}
			}

			//檢查是否登入時記住帳密
			if(isset($_COOKIE["loginName"])){
				$data['cookie_name']=$_COOKIE["loginName"];
			}
			if(isset($_COOKIE["loginPW"])){
				$data['cookie_pw']=$_COOKIE["loginPW"];
			}

			//登入後右側欄位的View變化
			if(!empty($memberLevel)){
				$data['memberLevel']=$memberLevel;
				if(!empty($userId)){
					$data['userId']=$userId;
				}
			}

			//右側Menu項目
			$menu_arr = array('menuBtn_login'=>"登入會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_forgotPw'=>"忘記密碼", 'menuBtn_applyMember'=>"申請會員", 'menuBtn_autobiography'=>"版主經歷與介紹");
			//右側Menu項目icon檔名
			$icon_arr = array('menuBtn_login'=>"menuIcon_login.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_forgotPw'=>"menuIcon_forgot.png", 'menuBtn_applyMember'=>"menuIcon_apply.png", 'menuBtn_autobiography'=>"menuIcon_apply.png");
			//右側Menu印出內文
			$data['menuContext'] = $this->initial->menuBarPrintedContext($menu_arr, $icon_arr, $data['images_root']);

			//登入資訊
			$data['memberName'] = "訪客";
			$data['loginInfo'] = "您尚未登入";

      //如已經登入，則紀錄寫入Session，視窗不再出現
      $data['loginAutobiography'] = $this->session->userdata("loginAutobiography");

			$content=$this->initial->getHomeContext($page);
			$data['title']=$content['title'];
			$data['sub_title']=$content['sub_title'];
			$data['content']=$content['content'];

			$this->load->view('/front/home_header',$data);
			$this->load->view('/front/home_body',$data);
			$this->load->view('/front/home_footer',$data);
		}

		//登入檢查(參照orignal的index.php)
		public function login(){

			if(isset($_POST["loginName"])){
				$userData = $this->mmodel->getAllUserInfoByUserName($_POST["loginName"]);

				if(empty($userData)){

					header("Location: /home/index/1/acc_err");//帳號輸入錯誤轉址
					delete_cookie("loginName");
					delete_cookie("loginPW");
					delete_cookie("loginRemChk");

				}else{

					foreach ($userData as $userArr) {
						$user_id=$userArr["m_id"];
						$user_name=$userArr["m_username"];
						$user_pw=$userArr["m_passwd"];
						$user_level=$userArr["m_level"];
						$user_login=$userArr["m_login"];
					}

					if(isset($_POST["loginPW"])){

						//Base64加密後的輸入密碼
						$base64_encode_pw=base64_encode($_POST["loginPW"]);

						//登入資料正確時的處理
						if(preg_match(("/".$base64_encode_pw."/"), $user_pw)){

							//如密碼加密及比對無誤後則開始修改登入時間及次數
							ini_set('date.timezone','Asia/Taipei');
							// $now_time=date("Y-m-d H:i:s",time()+60*60*6);
							$now_time=date("Y-m-d H:i:s",time());

							if(isset($user_login)){
								$this->mmodel->updateLoginTimeByUid($now_time, $user_login, $user_id);
							}

							//將資料寫入SESSION內
							$this->session->set_userdata('loginMember', $user_name); 
							$this->session->set_userdata('memberLevel', $user_level);
							$this->session->set_userdata('loginTime', $user_login);
							$this->session->set_userdata('userId', $user_id);

							// 將帳戶決定是否寫入COOKIE內(生命週期:2 Weeks)
							if(isset($_POST["loginRem"])){
								set_cookie("loginName", $_POST["loginName"], 86400*14);
								set_cookie("loginPW", $_POST["loginPW"], 86400*14);
								set_cookie("loginRemChk", "true", 86400*14);
							}else
							{
								delete_cookie("loginName");
								delete_cookie("loginPW");
								delete_cookie("loginRemChk");
							}
							
							//轉向到index繼續處理
							$memberLevel = $this->session->userdata('memberLevel');
							if($memberLevel == "admin" || $memberLevel == "member"){
								header("Location: /home/index/1/sucess");
							}

						}else{

							header("Location: /home/index/1/pw_err");//密碼輸入錯誤轉址
							delete_cookie("loginName");
							delete_cookie("loginPW");
							delete_cookie("loginRemChk");

						}

					}

				}

			}

		}

		public function haveLogin($memberLevel, $userId, $page='1'){

			//登入session檢查(未操作30分鐘後登出)
			$this->basicTool->autoLogout(1800);

			//網頁View配置初始化
			$data=$this->initial->init();

			//留言板管理頁面是否登入
			$data['boardAdminLogin'] = $this->basicTool->boardAdminLoginChk();
			
			//檢查是否登入時記住帳密
			if(isset($_COOKIE["loginName"])){
				$data['cookie_name']=$_COOKIE["loginName"];
			}
			if(isset($_COOKIE["loginPW"])){
				$data['cookie_pw']=$_COOKIE["loginPW"];
			}

			//登入後右側欄位的View變化
			if(!empty($memberLevel)){
				$data['memberLevel']=$memberLevel;
				if(!empty($userId)){
					$data['userId']=$userId;
				}
			}

			$content=$this->initial->getHomeContext($page);
			$data['title']=$content['title'];
			$data['sub_title']=$content['sub_title'];
			$data['content']=$content['content'];

			//頁面檢查功能分類
			$data['pageName']= "haveLogin";

			//登入成功處理
			$data['loginChk']="sucess";

			//登入資訊
			$memberData = $this->mmodel->getAllUserInfoByUserId($userId);
			$data['memberName'] = $memberData[0]['m_name'];
			$data['loginInfo'] = "您登入了".$memberData[0]['m_login']."次";

			if($memberData[0]['m_level'] == 'admin'){
				$center = "前往管理中心";
			}else{
				$center = "前往會員中心";
			}

      //左側主文區下方提示(登入成功後)
      $data['main_content_notcie'] = "<p><em class=\"context\">※你是第一次瀏覽此網站的嗎?建議你先點選此<a href=\"/helper_questions/index/1/2\">說明連結</a>比較容易上手喔!</em></p>";

			//右側Menu項目
			$menu_arr = array('menuBtn_login'=>"登出會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_center'=>$center, 'menuBtn_autobiography'=>"版主經歷與介紹");
			//右側Menu項目icon檔名
			$icon_arr = array('menuBtn_login'=>"menuIcon_logout.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_center'=>"menuIcon_toCenter.png", 'menuBtn_autobiography'=>"menuIcon_apply.png");
			//右側Menu印出內文
			$data['menuContext'] = $this->initial->menuBarPrintedContext($menu_arr, $icon_arr, $data['images_root']);

      //如已經登入，則紀錄寫入Session，視窗不再出現
      $data['loginAutobiography'] = $this->session->userdata("loginAutobiography");

			$this->load->view('/front/home_header',$data);
			$this->load->view('/front/home_body',$data);
			$this->load->view('/front/home_footer',$data);

		}

		public function logout(){
			$this->basicTool->deleteLoginSession();
			header("Location: /home/index/1");

		}

		public function timeout(){

			$this->basicTool->deleteLoginSession();
			header("Location: /home/index/1/timeout");

		}

	}
?>