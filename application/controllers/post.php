<?php
	class Post extends CI_Controller{

		public function __construct()
		{
			parent::__construct();

			//model
			$this->load->model("front/main_model","mmodel");

			//libraries
			$this->load->library("common/initial");
			$this->load->library('tools/basicTool', '','basicTool');

			//helper
			$this->load->helper('cookie');
		}

		public function index($userId, $type)
		{
			//登入session檢查(未操作30分鐘後登出)
			$this->basicTool->autoLogout(1800);

			//網頁View配置初始化
			$data=$this->initial->init();

			$loginMember = $this->session->userdata('loginMember'); 
			$memberLevel = $this->session->userdata('memberLevel');
			$userId = $this->session->userdata('userId');

			//登入後的頁面必需值
			if(!empty($memberLevel)){
				$data['memberLevel']=$memberLevel;
				if(!empty($userId)){
					$data['userId']=$userId;
				}
			}

			//頁面檢查功能分類
			$data['pageName'] = "post";

			//登入成功處理
			if(isset($loginMember)){
				$data['loginChk']="sucess";
			}

			//留言種類
			$data['type'] = $type;

			//右側Menu項目
			$menu_arr = array('menuBtn_login'=>"登出會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_boardAdmin'=>"登入留言板系統", 'menuBtn_autobiography'=>"版主經歷與介紹");
			//右側Menu項目icon檔名
			$icon_arr = array('menuBtn_login'=>"menuIcon_logout.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_boardAdmin'=>"menuIcon_boardLogIn.png", 'menuBtn_autobiography'=>"menuIcon_apply.png");
			//右側Menu印出內文
			$data['menuContext'] = $this->initial->menuBarPrintedContext($menu_arr, $icon_arr, $data['images_root']);
			
			//登入資訊
			$memberData = $this->mmodel->getAllUserInfoByUserId($userId);
			$data['memberName'] = $memberData[0]['m_name'];
			$data['loginInfo'] = "您登入了".$memberData[0]['m_login']."次";

      //如已經登入，則紀錄寫入Session，視窗不再出現
      $data['loginAutobiography'] = $this->session->userdata("loginAutobiography");

			$this->load->view('/front/home_header',$data);
			$this->load->view('/front/post_body',$data);
			$this->load->view('/front/home_footer',$data);
		}

		public function leaveSomeMsg(){

			$type = $_POST['type'];

			$board_web ="";
			if(substr_count($_POST['board_web'], "http://") == 0){
				$board_web = "http://".$_POST['board_web'];
			}else{
				$board_web = $_POST['board_web'];
			}

			if($type == 'board'){
				$comment_type = 'N';
			}else{
				$comment_type = 'H';
			}

			$input_data = array(
				"board_name"=>$_POST['board_name'], "board_sex"=>$_POST['board_sex'],
				"board_subject"=>$_POST['board_subject'], "board_time"=>date('Y-m-d H:i:s'),
				"board_email"=>$_POST['board_email'], "board_web"=>$board_web,
				"board_content"=>$_POST['board_content'], "comment_type"=>$comment_type);
			$result = $this->mmodel->addComment($input_data);

			$returnUrl='';

			if($type == 'board'){

				$returnUrl = '/message_board/index/'.$this->session->userdata('userId').'/1/0/notLogin/0';

			}elseif ($type == 'helper') {

				$returnUrl = '/helper/index/'.$this->session->userdata('userId');
				if($result){
					$returnUrl .= "/1";
				}else{
					$returnUrl .= "/0";
				}

			}elseif ($type == 'contact') {

				$returnUrl = '/contact/index/1/0';

			}

			$this->basicTool->script_message(false, "", $returnUrl);
		}

	}
?>