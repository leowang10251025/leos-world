<?php
	/**
	 * 僅供練習使用不上傳至Git
	 */
	class Car extends Transportation
	{
		var $wheelQty;
		var $carDoorQty;
		var $soldPrice;

		function __construct($name, $bodyColor, $horsePower, $wheelQty, $carDoorQty, $soldPrice)
		{
			parent::__construct($name, $bodyColor, $horsePower);
			$this->wheelQty = $wheelQty;
			$this->carDoorQty = $carDoorQty;
			$this->soldPrice = $soldPrice;
		}

		public function setWheelQty($wheelQty='')
		{
			$this->wheelQty = $wheelQty;
		}

		public function getWheelQty()
		{
			return $this->wheelQty;
		}

		public function setCarDoorQty($carDoorQty='')
		{
			$this->carDoorQty = $carDoorQty;
		}

		public function getCarDoorQty()
		{
			return $this->carDoorQty;
		}

		public function setSoldPrice($soldPrice='')
		{
			$this->soldPrice = $soldPrice;
		}

		public function getSoldPrice()
		{
			return $this->soldPrice;
		}


	}
?>