<?php
	class Pet_home extends CI_Controller{

		public function __construct(){

			parent::__construct();
			//model
			$this->load->model("front/main_model","mmodel");

			//libraries
			$this->load->library("common/initial");

			//helper
			$this->load->helper('cookie');

		}

		public function index(){

			//網頁View配置初始化
			$data=$this->initial->init();

			//頁面檢查功能分類
			$data['pageName']= "pet_home";

			$this->load->view('/front/pet_home_header',$data);
			$this->load->view('/front/pet_home_body',$data);

		}
	
	}
?>