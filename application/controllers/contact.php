<?php
	class Contact extends CI_Controller{

		public function __construct()
		{
			parent::__construct();

			//model
			$this->load->model("front/main_model","mmodel");

			//libraries
			$this->load->library("common/initial");
			$this->load->library('tools/basicTool', '','basicTool');

			//helper
			$this->load->helper('cookie');
		}

		public function index($numPage, $sendStatus){

			//登入session檢查(未操作30分鐘後登出)
			$this->basicTool->autoLogout(1800);

			//網頁View配置初始化
			$data=$this->initial->init();

			$loginMember = $this->session->userdata('loginMember'); 
			$memberLevel = $this->session->userdata('memberLevel');
			$userId = $this->session->userdata('userId');

			//登入後的頁面必需值
			if(!empty($memberLevel)){
				$data['memberLevel']=$memberLevel;
				if(!empty($userId)){
					$data['userId']=$userId;
				}
			}

			if(!isset($numPage)) {
				$numPage=1;
			}

			$allAdminMemberData = $this->mmodel->getAllUserInfoByMemberLevel('admin');

			foreach ($allAdminMemberData as $key => $adminMemberData) {
				if($key == ($numPage-1)){
					$data['adminMember'] = $adminMemberData;
				}
			}

			$data['totalPages'] = count($allAdminMemberData);

			$data['numPage'] = $numPage; //程式目前所控制頁數
			$data['userId']=$userId;

			//頁面檢查功能分類
			$data['pageName'] = "contact";

			//寄信成功與否判斷
			$data['sendStatus']= $sendStatus;

			//寄信狀態判斷
			if(isset($sendStatus)){
				if($sendStatus=='sendSuccess' || $sendStatus=='sendFail'){
					echo "<script>";
					echo "window.close();";
					echo "self.opener.location.reload();";
					echo "</script>";					
				}
			}

			//登入成功處理
			if(isset($loginMember)){
				$data['loginChk']="sucess";
			}

			//登入資訊
			$memberData = $this->mmodel->getAllUserInfoByUserId($userId);
			$data['memberName'] = $memberData[0]['m_name'];
			$data['loginInfo'] = "您登入了".$memberData[0]['m_login']."次";

			if($memberData[0]['m_level'] == 'admin'){
				$center = "前往管理中心";
			}else{
				$center = "前往會員中心";
			}

			//右側Menu項目
			$menu_arr = array('menuBtn_login'=>"登出會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_center'=>$center, 'menuBtn_autobiography'=>"版主經歷與介紹");
			//右側Menu項目icon檔名
			$icon_arr = array('menuBtn_login'=>"menuIcon_logout.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_center'=>"menuIcon_toCenter.png",  'menuBtn_autobiography'=>"menuIcon_apply.png");
			//右側Menu印出內文
			$data['menuContext'] = $this->initial->menuBarPrintedContext($menu_arr, $icon_arr, $data['images_root']);

      //如已經登入，則紀錄寫入Session，視窗不再出現
      $data['loginAutobiography'] = $this->session->userdata("loginAutobiography");

			$this->load->view('/front/home_header',$data);
			$this->load->view('/front/contact_body',$data);
			$this->load->view('/front/home_footer',$data);
		}

		public function inputMailContent($m_id)
		{
			//登入session檢查(未操作30分鐘後登出)
			$this->basicTool->autoLogout(1800);
			
			//網頁View配置初始化
			$data=$this->initial->init();

			//頁面檢查功能分類
			$data['pageName'] = "contact_mail";

			$data['board_heading'] = "連絡管理員-信件內容";
			$data['inputArr'] = array('連絡者暱稱' =>"mailFromName", '連絡者信箱' =>"mailFrom", '填寫內容'=>"input_content");
			$data['formId'] = "formAdminContact";
			$data['formAction'] = "/contact/contactAdminByMail/".$m_id."/1";
			$data['formBackgroundSrc'] = $data['images_root']."sticker_fix.png";
			$data['formSubmitSrc'] = $data['images_root']."sticker_button_updateOK.png";
			$data['formBackwardSrc'] = $data['images_root']."sticker_button_back.png";

			$this->load->view('/front/open_window_header',$data);
			$this->load->view('/front/contact_window_body',$data);
		}

		public function contactAdminByMail($adminId, $numPage){

			$mailFrom = $_POST['mailFrom'];
			$mailFromName = $_POST['mailFromName'];
			$input_content = '<b>'. $_POST['input_content'].'</b>';

			$adminData = $this->mmodel->getAllUserInfoByUserId($adminId);
			$mailTo = $adminData[0]["m_email"];

			$mailSubject="你好，我是瀏覽者 ".$mailFromName."，想與你連絡...";
			$mailContent="<div style='margin:10px;'>To 管理者 <b>".$adminData[0]["m_username"].":</b><br/>
							你好，我想請問以下問題...<br/><br/>
							<div style='margin-left:20px;'><q>".
								$input_content.
							"</q></div><br/><br/>
							<p style='float:right;position:absolute;right:10px;'>
								<small style='color:#bfbfbf;'>連絡方式:(你的大名:請留下你的暱稱或名字，以方便與你連絡)<br/>(你的連絡信箱:格式如xxx.oo@gmail.com)。</small><br/><br/>
								<i>From: $mailFromName</i>
							</p>".
						"</div>";

			// echo '$mailFrom:'.$mailFrom.'<br/>';
			// echo '$mailFromName:'.$mailFromName.'<br/>';
			// echo '$mailTo:'.$mailTo.'<br/>';
			// echo '$mailSubject:'.$mailSubject.'<br/>';
			// echo '$mailContent:'.$mailContent.'<br/>';
			// exit;

			// $this->basicTool->sendEmail($mailFrom, $mailFromName, $mailTo, $mailSubject, $mailContent);
			$sendResult = $this->basicTool->sendEmail($mailFrom, $mailFromName, $mailTo, $mailSubject, $mailContent);

			if($sendResult == '1'){
				header("Location: /contact/index/".$numPage."/sendSuccess");
			}else{
				header("Location: /contact/index/".$numPage."/sendFail");
			}

		}

	}
?>