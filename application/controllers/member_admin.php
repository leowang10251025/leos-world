<?php
	class Member_admin extends CI_Controller{

		public function __construct()
		{
			parent::__construct();

			//model
			$this->load->model("front/main_model","mmodel");

			//libraries
			$this->load->library("common/initial");
			$this->load->library('tools/basicTool', '','basicTool');

			//helper
			$this->load->helper('cookie');
		}

		public function index($userId, $page=1, $infoStatus='')
		{
			//登入session檢查(未操作30分鐘後登出)
			$this->basicTool->autoLogout(1800);
			
			//網頁View配置初始化
			$data=$this->initial->init();

			$loginMember = $this->session->userdata('loginMember'); 
			$memberLevel = $this->session->userdata('memberLevel');
			$userId = $this->session->userdata('userId');

			if($memberLevel=="member"){
				header("Location: /member_center/".$userId);
			}


			//會員編輯、刪除的訊息回饋
			$data['infoStatus'] = $infoStatus;

			//登入會員資料處理
			$result_data = array();

			if(empty($userId)){
				$result_data = $this->mmodel->getAllUserInfoByUserName($loginMember);

			}else{
				$result_data = $this->mmodel->getAllUserInfoByUserId($userId);
			}

			$data['adminName'] = $result_data[0]['m_name'];
			if($result_data[0]['m_sex'] == 'M'){
				$data['adminSex'] = '先生';
			}else{
				$data['adminSex'] = '小姐';
			}
			$data['constellation'] = $this->initial->adjustConstellations($result_data[0]['m_birthday']);
			$data['email']=$result_data[0]['m_email'];
			$data['url']=$result_data[0]['m_url'];
			$data['loginTimes'] = $result_data[0]['m_login'];
			$data['loginTime'] = substr($result_data[0]['m_logintime'], 0, -6);

			//資料表頁數處理
			$numPage=1;//目前頁數

			if(isset($page)){
				$numPage=$page;
			}

			$numRowPage=5; //每頁筆數
			$startRowPage=($numPage-1) * $numRowPage;

			$allOfResult = $this->mmodel->getLimitUserInfoByPageNum($startRowPage, $numRowPage, "member", false); // 全部非一般會員資料

			$limitOfResult = $this->mmodel->getLimitUserInfoByPageNum($startRowPage, $numRowPage, "member", true); //實際每頁呈現資料

			$totalLimitRow = count($allOfResult); //全部相關資料筆數

			$totalPage=ceil($totalLimitRow/$numRowPage); //全部頁數

			$data['result_data'] = $limitOfResult;
			$data['m_id'] = $userId;
			$data['numPage'] = $numPage;
			$data['totalPage'] = $totalPage;
			$data['totalLimitRow'] = $totalLimitRow;

			//登入後的頁面必需值
			if(!empty($memberLevel)){
				$data['memberLevel']=$memberLevel;
				if(!empty($userId)){
					$data['userId']=$userId;
				}
			}

			//頁面檢查功能分類
			$data['pageName'] = "member_admin";

			//登入成功處理
			if(isset($loginMember)){
				$data['loginChk']="sucess";
			}

			//登入資訊
			$memberData = $this->mmodel->getAllUserInfoByUserId($userId);
			$data['memberName'] = $memberData[0]['m_name'];
			$data['loginInfo'] = "您登入了".$memberData[0]['m_login']."次";

			//右側Menu項目
			$menu_arr = array('menuBtn_login'=>"登出會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_adminInfo'=>'管理員資訊', 'menuBtn_adminInfoModify'=>'管理員資訊修改', 'menuBtn_autobiography'=>"版主經歷與介紹");
			//右側Menu項目icon檔名
			$icon_arr = array('menuBtn_login'=>"menuIcon_logout.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_adminInfo'=>"menuIcon_memberInfo.png", 'menuBtn_adminInfoModify'=>"menuIcon_apply.png", 'menuBtn_autobiography'=>"menuIcon_apply.png");
			//右側Menu印出內文
			$data['menuContext'] = $this->initial->menuBarPrintedContext($menu_arr, $icon_arr, $data['images_root']);

      //如已經登入，則紀錄寫入Session，視窗不再出現
      $data['loginAutobiography'] = $this->session->userdata("loginAutobiography");

			$this->load->view('/front/home_header',$data);
			$this->load->view('/front/member_admin_body',$data);
			$this->load->view('/front/home_footer',$data);
		}

		public function deleteMember($userId){

			$loginUerId = $this->session->userdata('userId');

			if(!empty($userId)){

				$result = $this->mmodel->deleteUserInfoByUserId($userId);

				if($result){
					header("Location: /member_admin/index/".$loginUerId."/1/isDelete");
				}else{
					header("Location: /member_admin/index/".$loginUerId."/1/notDelete");
				}

			}else{
				header("Location: /member_admin/index/".$loginUerId."/1/notDelete");
			}

		}

		public function logout(){

			//清除SESSION內資料
			$this->session->unset_userdata('loginMember'); 
			$this->session->unset_userdata('memberLevel');
			$this->session->unset_userdata('loginTime');
			$this->session->unset_userdata('userId');

			header("Location: /home/index/1");
		}

	}
?>