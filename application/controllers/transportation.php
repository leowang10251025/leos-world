<?php
	/**
	 * 僅供練習使用不上傳至Git
	 */
	class Transportation extends CI_controller
	{
		var $name;
		var $bodyColor;
		var $horsePower;
		var $fuelConsumption;

		function __construct($name, $bodyColor, $horsePower)
		{
			parent::__construct();
			$this->name = $name;
			$this->bodyColor = $bodyColor;
			$this->horsePower = $horsePower;
		}

		public function setName($name='')
		{
			$this->name = $name;
		}

		public function getName()
		{
			return $this->name;
		}

		protected function setBodyColor($bodyColor='')
		{
			$this->bodyColor = $bodyColor;
		}

		protected function getBodyColor()
		{
			return $this->bodyColor;
		}

		protected function setHorsePower($horsePower='')
		{
			$this->horsePower = $horsePower;
		}

		protected function getHorsePower()
		{
			return $this->horsePower;
		}

		public function setFuelConsumption($fuelConsumption='')
		{
			$this->fuelConsumption = $fuelConsumption;
		}

		public function getFuelConsumption()
		{
			return $this->fuelConsumption;
		}


	}
?>