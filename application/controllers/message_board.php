<?php
	class Message_board extends CI_Controller
	{
		
		public function __construct()
		{
			parent::__construct();

			//model
			$this->load->model("front/main_model","mmodel");

			//libraries
			$this->load->library("common/initial");
			$this->load->library('tools/basicTool', '','basicTool');

			//helper
			$this->load->helper('cookie');

		}

		public function index($userId, $page, $sendStatus='',$loginStatus= 'notLogin', $dataStatus)
		{
			//登入session檢查(未操作30分鐘後登出)
			$this->basicTool->autoLogout(1800);

			//網頁View配置初始化
			$data=$this->initial->init();

			$loginMember = $this->session->userdata('loginMember'); 
			$memberLevel = $this->session->userdata('memberLevel');
			$userId = $this->session->userdata('userId');

			//登入後的頁面必需值
			if(!empty($memberLevel)){
				$data['memberLevel']=$memberLevel;
				if(!empty($userId)){
					$data['userId']=$userId;
				}
			}

			//頁面檢查功能分類
			$data['pageName'] = "board";

			//登入成功處理
			if(isset($loginMember)){
				$data['loginChk']="sucess";
			}

			//留言板處理
			//程式目前所控制頁數
			if(isset($page)){
				$numPage=$page;
			}
			//每頁資料(留言)筆數
			$numRowPage=4;
			$startRowPage=($numPage-1) * $numRowPage;

			$commentType = '';
			$isAllData = true;

			if($loginStatus == 'loginFail' || $loginStatus == 'notLogin'){
				$isAllData = false;
				$commentType = 'N';
			}else if ($loginStatus == 'loginSucess') {
				$isAllData = true;
				$commentType = '';
			}

			$commentArr = $this->mmodel->getAllCommentByType($isAllData, $commentType, true, $startRowPage, $numRowPage);

			$data['commentArr']= $commentArr;
			$allComment = $this->mmodel->getAllCommentByType($isAllData, $commentType, false, '', '');

			//總資料數
			$totalRecords = count($allComment);
			$data['totalRecords'] = $totalRecords;

			//總頁數
			$data['totalPages'] = ceil($totalRecords/$numRowPage);
			$data['numPage'] = $numPage;

			$data['loginStatus'] = $loginStatus;

			//寄信成功與否判斷
			if(isset($sendStatus)){
				$data['sendStatus']= $sendStatus;
			}

			//管理頁面成功與否判斷
			if(isset($loginStatus)){
				$data['loginStatus']= $loginStatus;
			}

			//留言狀態判斷
			if(isset($dataStatus)){
				$data['dataStatus']= $dataStatus;
				if($dataStatus=='isUpdate' || $dataStatus=='notUpdate' || $dataStatus=='isDel' || $dataStatus=='notDel' || $dataStatus=='dataAlive'){
					echo "<script>";
					echo "window.close();";
					echo "self.opener.location.reload();";
					echo "</script>";					
				}
			}

			if($loginStatus == "notLogin" || $loginStatus == "logoutSucess"){
				$menuBtn_boardAdmin_item = "登入留言板系統";
				$menuBtn_boardAdmin_loginIconName = "menuIcon_boardLogIn.png";
			}else if($loginStatus == "loginSucess"){
				$menuBtn_boardAdmin_item = "登出留言板系統";
				$menuBtn_boardAdmin_loginIconName = "menuIcon_boardLogOut.png";
			}

			//右側Menu項目
			$menu_arr = array('menuBtn_login'=>"登出會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_boardAdmin'=>$menuBtn_boardAdmin_item, 'menuBtn_autobiography'=>"版主經歷與介紹");
			//右側Menu項目icon檔名
			$icon_arr = array('menuBtn_login'=>"menuIcon_logout.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_boardAdmin'=>$menuBtn_boardAdmin_loginIconName, 'menuBtn_autobiography'=>"menuIcon_apply.png");
			//右側Menu印出內文
			$data['menuContext'] = $this->initial->menuBarPrintedContext($menu_arr, $icon_arr, $data['images_root']);
			
			//登入資訊
			$memberData = $this->mmodel->getAllUserInfoByUserId($userId);
			$data['memberName'] = $memberData[0]['m_name'];
			$data['loginInfo'] = "您登入了".$memberData[0]['m_login']."次";

      //如已經登入，履歷權限登入視窗不再出現
      $loginAutobiography = $this->session->userdata("loginAutobiography");
      if(isset($loginAutobiography) && $loginAutobiography == "sucess"){
        $data['loginAutobiography'] = $loginAutobiography;
      }

			$this->load->view('/front/board_header',$data);
			$this->load->view('/front/board_body',$data);
			$this->load->view('/front/board_footer',$data);
		}

		public function inputMailContent($boardId)
		{			
			//登入session檢查(未操作30分鐘後登出)
			$this->basicTool->autoLogout(1800);

			//網頁View配置初始化
			$data=$this->initial->init();

			//頁面檢查功能分類
			$data['pageName'] = "message_mail";

			$resultArr = $this->mmodel->getOneCommentById($boardId);
			// var_export($resultArr);exit;

			$data['board_heading'] = "連絡留言者-信件內容";
			$data['inputArr'] = array('留言者暱稱' =>"mailFromName", '留言者信箱' =>"mailFrom", '填寫內容'=>"input_content");
			$data['formId'] = "formMessageMail";
			$data['formAction'] = "/message_board/contactAdminByMail/1/".$resultArr[0]['board_name'];
			$data['formBackgroundSrc'] = $data['images_root']."sticker_fix.png";
			$data['formSubmitSrc'] = $data['images_root']."sticker_button_updateOK.png";
			$data['formBackwardSrc'] = $data['images_root']."sticker_button_back.png";

			$data['mailFromName'] = $resultArr[0]['board_name'];
			$data['mailFrom'] = $resultArr[0]['board_mail'];

			$this->load->view('/front/open_window_header',$data);
			$this->load->view('/front/message_mail_window_body',$data);
		}

		public function contactAdminByMail($adminId, $boardName){

			$mailFrom = $_POST['mailFrom'];
			$mailFromName = $_POST['mailFromName'];
			$input_content = '<b>'. $_POST['input_content'].'</b>';

			$adminData = $this->mmodel->getOneBoradAdminById($adminId);
			$mailTo = $adminData[0]["m_email"];

			$mailSubject="這是管理者 ".$mailFromName."的來信";
			$mailContent="<div style='margin:10px;'><i>To ".$boardName."</i>:<br/><br/>
							你好，這裡是 ".$adminData[0]['m_name']."，因為有些問題如下，所以必需跟您通知:<br/><br/>
							<div style='margin-left:20px;'><br/>
							<q>".
								$input_content.
							"</q></div><br/><br/>
							<p style='float:right;position:absolute;right:10px;'>
								<small style='color:#bfbfbf;'>這是管理版的問題聯絡信件，不代表您的權力有任何變動，<br/>請安心繼續使用留言板!</small><br/><br/>
								<i>From: ".$adminData[0]['m_name']."</i>
							</p>".
						"</div>";

			// echo '$mailFrom:'.$mailFrom.'<br/>';
			// echo '$mailFromName:'.$mailFromName.'<br/>';
			// echo '$mailTo:'.$mailTo.'<br/>';
			// echo '$mailSubject:'.$mailSubject.'<br/>';
			// echo '$mailContent:'.$mailContent.'<br/>';
			// exit;

			// $this->basicTool->sendEmail($mailFrom, $mailFromName, $mailTo, $mailSubject, $mailContent);
			$sendResult = $this->basicTool->sendEmail($mailFrom, $mailFromName, $mailTo, $mailSubject, $mailContent);

			if($sendResult == '1'){
				header("Location: /message_board/index/".$this->session->userdata('userId')."/1/sendSuccess/0/dataAlive");
			}else{
				header("Location: /message_board/index/".$this->session->userdata('userId')."/1/sendFail/0/dataAlive");
			}

		}

		public function checkAdminAcc(){

			$result = $this->mmodel-> getOneBoradAdmin($_POST['admin_acc']);

			echo $result[0];

		}

		public function checkAdminPw(){

			$result = $this->mmodel-> getOneBoradAdmin($_POST['admin_acc']);

			if(base64_decode($result[0]['m_passwd']) != $_POST['admin_pw']){
				echo "notEqual";
			}

		}

		public function adminLogin(){

			$userId = $this->session->userdata('userId');

			if(isset($_POST["username"]) && isset($_POST["passwd"])){

				$boardAdmin = $this->mmodel->getOneBoradAdmin($_POST["username"]);

				if(!empty($boardAdmin)){
					if(base64_encode($_POST["passwd"]) == $boardAdmin[0]['m_passwd']){
						$this->session->set_userdata('boardAdminName', $boardAdmin[0]['m_name']);
						$this->session->set_userdata('boardAdminUserName', $boardAdmin[0]['m_username']);

						$this->basicTool->script_message(false, "", '/message_board/index/'.$userId.'/1/0/loginSucess/0');
					}else{
						$this->basicTool->script_message(false, "", '/message_board/index/'.$userId.'/1/0/loginFail/0');
					}
				}

			}
		}

		public function adminLogout()
		{
			$userId = $this->session->userdata('userId');

			//清除SESSION內資料
			$this->session->unset_userdata('boardAdminName'); 
			$this->session->unset_userdata('boardAdminUserName');

			$this->basicTool->script_message(false, "", '/message_board/index/'.$userId.'/1/0/logoutSucess/0');
		}

	}
?>