<?php
	class Admindel extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();

			//model
			$this->load->model("front/main_model","mmodel");

			//libraries
			$this->load->library("common/initial");
			$this->load->library('tools/basicTool', '','basicTool');

			//helper
			$this->load->helper('cookie');
		}

		public function index($userId, $board_id)
		{	
			//登入session檢查(未操作30分鐘後登出)
			$this->basicTool->autoLogout(1800);

			//網頁View配置初始化
			$data=$this->initial->init();

			$loginMember = $this->session->userdata('loginMember'); 
			$memberLevel = $this->session->userdata('memberLevel');
			$userId = $this->session->userdata('userId');

			//登入後的頁面必需值
			if(!empty($memberLevel)){
				$data['memberLevel']=$memberLevel;
				if(!empty($userId)){
					$data['userId']=$userId;
				}
			}

			//頁面檢查功能分類
			$data['pageName'] = "admindel";

			//登入成功處理
			if(isset($loginMember)){
				$data['loginChk']="sucess";
			}

			$data['windowTitle'] = "訪客留言版-刪除留言";

			$commentData = $this->mmodel->getOneCommentById($board_id);
			$data['commentData'] = $commentData[0];

			$this->load->view('/front/open_window_header',$data);
			$this->load->view('/front/admindel_body',$data);
		}

		public function deleteComment($board_id)
		{
			$userId = $this->session->userdata('userId');
			$result = $this->mmodel->deleteOneCommentByBoardId($board_id);

			if($result){
				$this->basicTool->script_message(false, "", '/message_board/index/'.$userId.'/1/0/0/isDel');
			}else{
				$this->basicTool->script_message(false, "", '/message_board/index/'.$userId.'/1/0/0/notDel');
			}

		}
	}
?>