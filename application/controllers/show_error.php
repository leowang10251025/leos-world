<?php
	class Show_error extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();

			//model
			$this->load->model("front/main_model","mmodel");

			//libraries
			$this->load->library("common/initial");
			$this->load->library('tools/basicTool', '','basicTool');

			//helper
			$this->load->helper('cookie');
		}

		public function index($sendStatus='', $error='')
		{
			//登入session檢查(未操作30分鐘後登出)
			$this->basicTool->autoLogout(1800);
			
			//網頁View配置初始化
			$data=$this->initial->init();

			$loginMember = $this->session->userdata('loginMember'); 
			$memberLevel = $this->session->userdata('memberLevel');
			$userId = $this->session->userdata('userId');

			//登入後右側欄位的View變化
			if(!empty($memberLevel)){
				$data['memberLevel']=$memberLevel;
				if(!empty($userId)){
					$data['userId']=$userId;
				}
			}

			//登入資訊與Menu項目
			if(empty($userId)){
				$data['memberName'] = "訪客";
				$data['loginInfo'] = "您尚未登入";
				//右側Menu項目
				$menu_arr = array('menuBtn_login'=>"登入會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_forgotPw'=>"忘記密碼", 'menuBtn_applyMember'=>"申請會員", 'menuBtn_autobiography'=>"版主經歷與介紹");
				//右側Menu項目icon檔名
				$icon_arr = array('menuBtn_login'=>"menuIcon_login.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_forgotPw'=>"menuIcon_forgot.png", 'menuBtn_applyMember'=>"menuIcon_apply.png", 'menuBtn_autobiography'=>"menuIcon_apply.png");
			}else{
				$memberData = $this->mmodel->getAllUserInfoByUserId($userId);
				$data['memberName'] = $memberData[0]['m_name'];
				$data['loginInfo'] = "您登入了".$memberData[0]['m_login']."次";

				if($memberData[0]['m_level'] == 'admin'){
					$center = "前往管理中心";
				}else{
					$center = "前往會員中心";
				}

				//右側Menu項目
				$menu_arr = array('menuBtn_login'=>"登出會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_center'=>$center, 'menuBtn_autobiography'=>"版主經歷與介紹");
				//右側Menu項目icon檔名
				$icon_arr = array('menuBtn_login'=>"menuIcon_logout.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_center'=>"menuIcon_toCenter.png", 'menuBtn_autobiography'=>"menuIcon_apply.png");
			}

			//右側Menu印出內文
			$data['menuContext'] = $this->initial->menuBarPrintedContext($menu_arr, $icon_arr, $data['images_root']);

      //如已經登入，則紀錄寫入Session，視窗不再出現
      $data['loginAutobiography'] = $this->session->userdata("loginAutobiography");

			// 原版Error參考
			// if (isset($_REQUEST['error_message'])) {
			// 	$error_message = preg_replace("/\\\\/", '', $_REQUEST['error_message']);
			// } else {
			// 	$error_message = "某些事件造成執行上錯誤, 正也是你為什麼在會停止在這頁面。";
			// }

			// if (isset($_REQUEST['system_error_message'])) {
			// 	$system_error_message = preg_replace("/\\\\/", '', $_REQUEST['system_error_message']);
			// } else {
			// 	$system_error_message = "無系統層級的錯誤回報。";
			// }
			$data['error_message']="Nothing Will Happen!"; //待處理

			//頁面檢查功能分類
			$data['pageName'] = "show_error";
			//登入成功處理
			if(!empty($loginMember)){
				$data['loginChk']="sucess";
			}

			//寄信狀態判斷
			if(!empty($sendStatus)){
				$data['sendStatus']= $sendStatus;
				if($sendStatus=='sendSuccess' || $sendStatus=='sendFail'){
					// echo 'Test 888';exit;
					echo "<script>";
					echo "window.close();";
					echo "self.opener.location.reload();";
					echo "</script>";					
				}

			}

			$this->load->view('/front/home_header',$data);
			$this->load->view('/front/error_body',$data);
			$this->load->view('/front/home_footer',$data);
		}

		public function inputMailContent($m_id)
		{
			//網頁View配置初始化
			$data=$this->initial->init();

			//頁面檢查功能分類
			$data['pageName'] = "error_mail";

			$data['board_heading'] = "系統問題聯絡-信件內容";
			$data['inputArr'] = array('回饋者暱稱' =>"mailFromName", '回饋者信箱' =>"mailFrom", '回饋問題內容'=>"input_content");
			$data['formId'] = "formErrorReply";
			$data['formAction'] = "/show_error/errorContactByMail/".$m_id;
			$data['formBackgroundSrc'] = $data['images_root']."msg_content_bg.png";
			$data['formSubmitSrc'] = $data['images_root']."sticker_button_updateOK.png";
			$data['formBackwardSrc'] = $data['images_root']."sticker_button_back.png";

			$this->load->view('/front/open_window_header',$data);
			$this->load->view('/front/error_window_body',$data);
		}

		public function errorContactByMail($adminId){

			$mailFrom = $_POST['mailFrom'];
			$mailFromName = $_POST['mailFromName'];
			$input_content = '<b>'. $_POST['input_content'].'</b>';

			$adminData = $this->mmodel->getAllUserInfoByUserId($adminId);
			$mailTo = $adminData[0]["m_email"];

			$mailSubject="你好，我是瀏覽者 ".$mailFromName."，想與你連絡...";
			$mailContent="<div style='margin:10px;'>To 管理者 <b>".$adminData[0]["m_username"].":</b><br/>
							你好，目前系統因為出現以下問題...<br/><br/>
							<div style='margin-left:20px;'><q>".
								$input_content.
							"</q>
							</div>
							<div style='margin-left:20px;margin-top:20px;'>
							麻煩協助解決此問題，方便系統使用，謝謝!
							</div>
							<br/><br/>
							<p style='float:right;position:absolute;right:10px;'>
								<small style='color:#bfbfbf;'>※收到問題回覆信件後，會盡快來處理並回覆處理狀況，<br/>感謝您的來信。</small><br/><br/>
								<i>From: $mailFromName</i>
							</p>".
						"</div>";

			// echo '$mailFrom:'.$mailFrom.'<br/>';
			// echo '$mailFromName:'.$mailFromName.'<br/>';
			// echo '$mailTo:'.$mailTo.'<br/>';
			// echo '$mailSubject:'.$mailSubject.'<br/>';
			// echo '$mailContent:'.$mailContent.'<br/>';
			// exit;

			// $this->basicTool->sendEmail($mailFrom, $mailFromName, $mailTo, $mailSubject, $mailContent);
			$sendResult = $this->basicTool->sendEmail($mailFrom, $mailFromName, $mailTo, $mailSubject, $mailContent);

			if($sendResult == '1'){
				header("Location: /show_error/index/sendSuccess");
			}else{
				header("Location: /show_error/index/sendFail");
			}

		}
	}
?>