<?php
	class Admin_passmail extends CI_Controller{

		public function __construct()
		{
			parent::__construct();
			//model
			$this->load->model("front/main_model","mmodel");

			//libraries
			$this->load->library("common/initial");
			$this->load->library('tools/basicTool', '','basicTool');

			//helper
			$this->load->helper('cookie');
		}

		public function index($page='1', $errMsg=''){

			//網頁View配置初始化
			$data=$this->initial->init();

			//頁面檢查功能分類
			$data['pageName']= "admin_passmail";

			//如輸入資料錯誤時，訊息回饋
			$data['errMsg']= $errMsg;

			//右側Menu項目
			$menu_arr = array('menuBtn_login'=>"登入會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_sendPw'=>"寄送密碼", 'menuBtn_applyMember'=>"申請會員", 'menuBtn_autobiography'=>"版主經歷與介紹");
			//右側Menu項目icon檔名
			$icon_arr = array('menuBtn_login'=>"menuIcon_login.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_sendPw'=>"menuIcon_contact.png", 'menuBtn_applyMember'=>"menuIcon_apply.png", 'menuBtn_autobiography'=>"menuIcon_apply.png");
			//右側Menu印出內文
			$data['menuContext'] = $this->initial->menuBarPrintedContext($menu_arr, $icon_arr, $data['images_root']);

			//登入資訊
			$data['memberName'] = "訪客";
			$data['loginInfo'] = "您尚未登入";

      //如已經登入，則紀錄寫入Session，視窗不再出現
      $data['loginAutobiography'] = $this->session->userdata("loginAutobiography");

			$content=$this->initial->getHomeContext($page);
			$data['title']=$content['title'];
			$data['sub_title']=$content['sub_title'];
			$data['content']=$content['content'];

			$this->load->view('/front/home_header',$data);
			$this->load->view('/front/forgot_mail_body',$data);
			$this->load->view('/front/home_footer',$data);
		}

		public function chkUserName(){

			$result = $this->mmodel->getAllUserInfoByUserName($_POST['name']);

			if(empty($result[0])){
				echo "notMatch";
			}

		}

		public function checkUserMail(){

			$result = $this->mmodel->getAllUserInfoByUserMail($_POST['mail']);

			if(empty($result[0])){
				echo "notMatch";
			}

		}

		public function chkUserData(){
			$userData = array();

			if(!empty($_POST['userName'])){
				$userData = $this->mmodel->getAllUserInfoByUserName($_POST['userName']);

				if(empty($userData[0])){
					header("Location: /admin_passmail/index/1/nameError");
				}else{
					$this->startSending($userData[0]['m_email'], $userData[0]['m_username']);
				}

			}

			if(!empty($_POST['eMail'])){
				$userData = $this->mmodel->getAllUserInfoByUserMail($_POST['eMail']);

				if(empty($userData[0])){
					header("Location: /admin_passmail/index/1/mailError");
				}else{
					$this->startSending($userData[0]['m_email'], $userData[0]['m_username']);
				}

			}

		}

		public function startSending($mailAddr, $sendUsername){

			#產生新一組密碼並更換掉舊密碼
			$newPW=$this->RandomPW(10);

			#補寄新密碼到信箱
			$mailFrom="leowang1025rd@gmail.com";
			$mailFromName="Leo's World的網頁系統管理員";
			$mailTo=$mailAddr;
			$mailSubject="*~來至Leo's World的網頁補寄密碼信~*";
			$mailHeader="From: $mailFrom\r\n";
			$mailContent="<div style='margin:10px;'>你好，你的帳號為: '$sendUsername'，<br/>臨時密碼為: ".$newPW. "。<br/></div>";

			// echo '$mailFrom:'.$mailFrom.'<br/>';
			// echo '$mailTo:'.$mailTo.'<br/>';
			// echo '$mailSubject:'.$mailSubject.'<br/>';
			// echo '$mailHeader:'.$mailHeader.'<br/>';
			// echo '$mailContent:'.$mailContent.'<br/>';
			// echo 'base64_encode_pw:'.base64_encode($newPW);exit;

			//變更儲存密碼
			$this->mmodel->updateLoginPassWordByUserName(base64_encode($newPW), $sendUsername);
			// $this->basicTool->sendEmail($mailFrom, $mailFromName, $mailTo, $mailSubject, $mailContent);
			$sendResult = $this->basicTool->sendEmail($mailFrom, $mailFromName, $mailTo, $mailSubject, $mailContent);
			if($sendResult == '1'){
				header("Location: /admin_passmail/index/1/sendSuccess");
			}else if($sendResult == '0'){
				header("Location: /admin_passmail/index/1/sendFail");
			}

		}

		function RandomPW($max_limit){

			$item=0;
			$rand=0;
			while($item<6){
				$rand.=rand(0,$max_limit);
				$item++;
			}
			return $rand;
		}

	}
?>