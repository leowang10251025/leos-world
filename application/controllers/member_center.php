<?php
	class Member_center extends CI_Controller{

		public function __construct()
		{
			parent::__construct();

			//model
			$this->load->model("front/main_model","mmodel");

			//libraries
			$this->load->library("common/initial");
			$this->load->library('tools/basicTool', '','basicTool');

			//helper
			$this->load->helper('cookie');
		}

		public function index($userId, $infoStatus='')
		{
			//登入session檢查(未操作30分鐘後登出)
			$this->basicTool->autoLogout(1800);

			//網頁View配置初始化
			$data=$this->initial->init();

			$loginMember = $this->session->userdata('loginMember'); 
			$memberLevel = $this->session->userdata('memberLevel');

			$result_data = $this->mmodel->getAllUserInfoByMemberLevelAndUID("member", $userId);

			if($result_data[0]['m_sex'] == 'M'){
				$data['memberSex'] = '先生';
			}else{
				$data['memberSex'] = '小姐';
			}
			$data['constellation'] = $this->initial->adjustConstellations($result_data[0]['m_birthday']);
			$data['email']=$result_data[0]['m_email'];
			$data['url']=$result_data[0]['m_url'];

			$data['memberName'] = $result_data[0]['m_name'];
			$data['loginTimes'] = $result_data[0]['m_login'];
			$data['loginTime'] = substr($result_data[0]['m_logintime'], 0, -6);

			$data['m_id'] = $userId;

			//會員編輯、刪除的訊息回饋
			$data['infoStatus'] = $infoStatus;

			//登入後的頁面必需值
			if(!empty($memberLevel)){
				$data['memberLevel']=$memberLevel;
				if(!empty($userId)){
					$data['userId']=$userId;
				}
			}

			//頁面檢查功能分類
			$data['pageName'] = "member_center";

			//登入成功處理
			if(isset($loginMember)){
				$data['loginChk']="sucess";
			}

			$content=$this->initial->getHomeContext(4);
			$data['title']=$content['title'];
			$data['sub_title']=$content['sub_title'];
			$data['content']=$content['content'];

			//登入資訊
			$memberData = $this->mmodel->getAllUserInfoByUserId($userId);
			$data['memberName'] = $memberData[0]['m_name'];
			$data['loginInfo'] = "您登入了".$memberData[0]['m_login']."次";

			//右側Menu項目
			$menu_arr = array('menuBtn_login'=>"登出會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_memberInfo'=>'會員資訊', 'menuBtn_memberInfoModify'=>'會員資訊修改', 'menuBtn_autobiography'=>"版主經歷與介紹");
			//右側Menu項目icon檔名
			$icon_arr = array('menuBtn_login'=>"menuIcon_logout.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_memberInfo'=>"menuIcon_memberInfo.png", 'menuBtn_memberInfoModify'=>"menuIcon_apply.png", 'menuBtn_autobiography'=>"menuIcon_apply.png");
			//右側Menu印出內文
			$data['menuContext'] = $this->initial->menuBarPrintedContext($menu_arr, $icon_arr, $data['images_root']);

      //如已經登入，則紀錄寫入Session，視窗不再出現
      $data['loginAutobiography'] = $this->session->userdata("loginAutobiography");

			$this->load->view('/front/home_header',$data);
			$this->load->view('/front/member_center_body',$data);
			$this->load->view('/front/home_footer',$data);
			
		}

		public function logout(){

			//清除SESSION內資料
			$this->session->unset_userdata('loginMember'); 
			$this->session->unset_userdata('memberLevel');
			$this->session->unset_userdata('loginTime');
			$this->session->unset_userdata('userId');

			header("Location: /home/index/1");
		}

	}
?>