<?php
	class Adminfix extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();

			//model
			$this->load->model("front/main_model","mmodel");

			//libraries
			$this->load->library("common/initial");
			$this->load->library('tools/basicTool', '','basicTool');

			//helper
			$this->load->helper('cookie');
		}

		public function index($userId, $board_id, $isUpdate)
		{	
			//登入session檢查(未操作30分鐘後登出)
			$this->basicTool->autoLogout(1800);

			//網頁View配置初始化
			$data=$this->initial->init();

			$loginMember = $this->session->userdata('loginMember'); 
			$memberLevel = $this->session->userdata('memberLevel');
			$userId = $this->session->userdata('userId');

			//登入後的頁面必需值
			if(!empty($memberLevel)){
				$data['memberLevel']=$memberLevel;
				if(!empty($userId)){
					$data['userId']=$userId;
				}
			}

			//頁面檢查功能分類
			$data['pageName'] = "adminfix";

			//登入成功處理
			if(isset($loginMember)){
				$data['loginChk']="sucess";
			}

			$data['windowTitle'] = "訪客留言版-編輯留言";

			$commentData = $this->mmodel->getOneCommentById($board_id);
			$data['commentData'] = $commentData[0];

			$this->load->view('/front/open_window_header',$data);
			$this->load->view('/front/adminfix_body',$data);
			// $this->load->view('/front/board_footer',$data);
		}

		public function updateCommentData($board_id)
		{			
			$userId = $this->session->userdata('userId');
			$updateArr = array(
				'board_subject' => $_POST['board_subject'], 'board_name'=> $_POST['board_name'],
				'board_sex' => $_POST['board_sex'], 'board_mail' => $_POST['board_mail'],
				'board_web' => $_POST['board_web'], 'board_content' => $_POST['board_content']
			);
			$updateResult = $this->mmodel->updateCommentData($updateArr, $board_id);

			if($updateResult){
				$this->basicTool->script_message(false, "", '/message_board/index/'.$userId.'/1/0/0/isUpdate');
			}else{
				$this->basicTool->script_message(false, "", '/message_board/index/'.$userId.'/1/0/0/notUpdate');
			}
		}
	}
?>