<?php
	class Member_join extends CI_Controller{

		public function __construct()
		{
			parent::__construct();

			//model
			$this->load->model("front/main_model","mmodel");

			//libraries
			$this->load->library("common/initial");
			$this->load->library('tools/basicTool', '','basicTool');

			//helper
			$this->load->helper('cookie');
		}

		public function index(){

			//網頁View配置初始化
			$data=$this->initial->init();

			//頁面檢查功能分類
			$data['pageName']= "member_join";

			//右側Menu項目
			$menu_arr = array('menuBtn_login'=>"登入會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_forgotPw'=>"忘記密碼", 'menuBtn_applyNotice'=>"填寫注意事項", 'menuBtn_autobiography'=>"版主經歷與介紹");
			//右側Menu項目icon檔名
			$icon_arr = array('menuBtn_login'=>"menuIcon_login.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_forgotPw'=>"menuIcon_forgot.png", 'menuBtn_applyNotice'=>"menuIcon_notice.png", 'menuBtn_autobiography'=>"menuIcon_apply.png");
			//右側Menu印出內文
			$data['menuContext'] = $this->initial->menuBarPrintedContext($menu_arr, $icon_arr, $data['images_root']);

			//登入資訊
			$data['memberName'] = "訪客";
			$data['loginInfo'] = "您尚未登入";

      //如已經登入，則紀錄寫入Session，視窗不再出現
      $data['loginAutobiography'] = $this->session->userdata("loginAutobiography");
			
			$this->load->view('/front/home_header',$data);
			$this->load->view('/front/member_join',$data);
			$this->load->view('/front/home_footer',$data);
		}

		public function joinMember(){

			$input_name=trim($_POST["input_name"]);
			$input_username=trim($_POST["input_username"]);
			$input_password=base64_encode($_POST["input_password"]);
			$input_admin_pw=base64_encode($_POST["input_adminPW"]);
			$input_sex=$_POST["input_sex"];
			$input_birthday=trim($_POST["input_birthday"]);
			$input_web=trim($_POST["input_web"]);
			$input_phone=trim($_POST["input_phone"]);
			$input_addr=trim($_POST["input_addr"]);
			$input_email=trim($_POST["input_email"]);
			$adminPW="7488e331b8b64e5794da3fa4eb10ad5d"; //管理者創建密碼

			if(isset($_POST["action"]) && ($_POST["action"]=="join")){

			
				if(isset($_POST["input_adminPW"]) && ($_POST["input_adminPW"]!=""))
				{	

					//加解密需處理
					if($adminPW == md5($_POST["input_adminPW"])){
						$_SESSION["memberLevel"]="admin";
						$input_level="admin";
					}

				}else {

					$_SESSION["memberLevel"]="member";
					$input_level="member";

				}

				$inputData = array("m_name"=>$input_name, "m_username"=>$input_username,
								 "m_passwd"=>$input_password, "m_sex"=>$input_sex,
								 "m_email"=>$input_email, "m_birthday"=>$input_birthday,
								 "m_url"=>$input_web, "m_phone"=>$input_phone,
								 "m_address"=>$input_addr, "m_jointime"=>date("Y-m-d H:i:s"),
								 "m_login"=>1, "m_logintime"=>date("Y-m-d H:i:s"),
								 "m_level"=>$input_level);

				$this->mmodel->addMemberData($inputData);

				$addData=$this->mmodel->getLastestUserInfo();

				if($input_level == "admin"){
					$this->basicTool->script_message(false,'','/member_admin/index/'.$addData[0]['m_id'].'/1');
				}else if($input_level == "member"){
					$this->basicTool->script_message(false,'','/member_center/index/'.$addData[0]['m_id']);
				}

			}

		}

		public function chkUserName(){

			$result = $this->mmodel->getAllUserInfoByUserName($_POST['name']);

			echo $result[0]['m_username'];

		}

		public function chkUserMail(){

			$result = $this->mmodel->getAllUserInfoByUserMail($_POST['mail']);

			echo $result[0]['m_email'];

		}

	}
?>