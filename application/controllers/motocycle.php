<?php
	/**
	 * 僅供練習使用不上傳至Git
	 */
	require 'transportation.php';

	class Motocycle extends Transportation
	{
		var $wheelQty;
		var $soldPrice;
		var $storageBoxQty;

		function __construct($name, $bodyColor, $horsePower, $wheelQty, $soldPrice)
		{
			parent::__construct($name, $bodyColor, $horsePower);
			$this->wheelQty = $wheelQty;
			$this->soldPrice = $soldPrice;
		}

		public function setWheelQty($wheelQty='')
		{
			$this->wheelQty = $wheelQty;
		}

		public function getWheelQty()
		{
			return $this->wheelQty;
		}

		public function setSoldPrice($soldPrice='')
		{
			$this->soldPrice = $soldPrice;
		}

		public function getSoldPrice()
		{
			return $this->soldPrice;
		}

		public function hasStorageBox($hasBox=false, $storageBoxQty)
		{
			if($hasBox){
				Motocycle::setStorageBoxQty($storageBoxQty);
			}else{
				Motocycle::setStorageBoxQty(0);
			}
		}

		private static function setStorageBoxQty($storageBoxQty)
		{
			$this->storageBoxQty = $storageBoxQty;
		}


	}
?>