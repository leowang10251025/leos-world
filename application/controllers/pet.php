<?php

	class Pet extends CI_Controller{

		public function __construct(){
			parent::__construct();

			//libraries
			$this->load->library("common/initial");

			//model
			$this->load->model("front/main_model","mmodel");
			$this->load->library('tools/basicTool', '','basicTool');

			//helper
			$this->load->helper('cookie');
		}

		public function index()
		{
			//登入session檢查(未操作30分鐘後登出)
			$this->basicTool->autoLogout(1800);

			//網頁View配置初始化
			$data=$this->initial->init();

			$loginMember = $this->session->userdata('loginMember'); 
			$memberLevel = $this->session->userdata('memberLevel');
			$userId = $this->session->userdata('userId');

			//登入後的頁面必需值
			if(!empty($memberLevel)){
				$data['memberLevel']=$memberLevel;
				if(!empty($userId)){
					$data['userId']=$userId;
				}
			} 

			//頁面檢查功能分類
			$data['pageName']= "pet";
			//登入成功處理
			if(!empty($loginMember)){
				$data['loginChk']="sucess";
			}

			//登入資訊與Menu項目
			if(empty($userId)){
				$data['memberName'] = "訪客";
				$data['loginInfo'] = "您尚未登入";
				//右側Menu項目
				$menu_arr = array('menuBtn_login'=>"登入會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_forgotPw'=>"忘記密碼", 'menuBtn_applyMember'=>"申請會員", 'menuBtn_autobiography'=>"版主經歷與介紹");
				//右側Menu項目icon檔名
				$icon_arr = array('menuBtn_login'=>"menuIcon_login.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_forgotPw'=>"menuIcon_forgot.png", 'menuBtn_applyMember'=>"menuIcon_apply.png", 'menuBtn_autobiography'=>"menuIcon_apply.png");
			}else{
				$memberData = $this->mmodel->getAllUserInfoByUserId($userId);
				$data['memberName'] = $memberData[0]['m_name'];
				$data['loginInfo'] = "您登入了".$memberData[0]['m_login']."次";

				if($memberData[0]['m_level'] == 'admin'){
					$center = "前往管理中心";
				}else{
					$center = "前往會員中心";
				}

				//右側Menu項目
				$menu_arr = array('menuBtn_login'=>"登出會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_center'=>$center, 'menuBtn_autobiography'=>"版主經歷與介紹");
				//右側Menu項目icon檔名
				$icon_arr = array('menuBtn_login'=>"menuIcon_logout.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_center'=>"menuIcon_toCenter.png", 'menuBtn_autobiography'=>"menuIcon_apply.png");
			}

			//右側Menu印出內文
			$data['menuContext'] = $this->initial->menuBarPrintedContext($menu_arr, $icon_arr, $data['images_root']);

      //如已經登入，則紀錄寫入Session，視窗不再出現
      $data['loginAutobiography'] = $this->session->userdata("loginAutobiography");

			$this->load->view('/front/home_header',$data);
			$this->load->view('/front/pet_body',$data);
			$this->load->view('/front/home_footer',$data);
		}

	}
?>