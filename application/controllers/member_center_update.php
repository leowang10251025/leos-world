<?php
	class Member_center_update extends CI_Controller{

		public function __construct()
		{
			parent::__construct();

			//model
			$this->load->model("front/main_model","mmodel");

			//libraries
			$this->load->library("common/initial");
			$this->load->library('tools/basicTool', '','basicTool');

			//helper
			$this->load->helper('cookie');
		}

		public function index($userId)
		{
			//登入session檢查(未操作30分鐘後登出)
			$this->basicTool->autoLogout(1800);

			//網頁View配置初始化
			$data=$this->initial->init();

			$loginMember = $this->session->userdata('loginMember'); 
			$memberLevel = $this->session->userdata('memberLevel');

			if($memberLevel=="admin"){
				header("Location: /member_center/index/".$userId);
			}

			//登入會員資料處理
			$result_data = array();

			if(empty($userId)){
				$result_data = $this->mmodel->getAllUserInfoByUserName($loginMember);

			}else{
				$result_data = $this->mmodel->getAllUserInfoByUserId($userId);
			}

			if($result_data[0]['m_sex'] == 'M'){
				$data['memberSex'] = '先生';
			}else{
				$data['memberSex'] = '小姐';
			}
			$data['constellation'] = $this->initial->adjustConstellations($result_data[0]['m_birthday']);
			$data['email']=$result_data[0]['m_email'];
			$data['url']=$result_data[0]['m_url'];

			$data['memberName'] = $result_data[0]['m_name'];
			$data['loginTimes'] = $result_data[0]['m_login'];
			$data['loginTime'] = substr($result_data[0]['m_logintime'], 0, -6);
			$data['result_row'] = $result_data[0];
			$data['m_id'] = $userId;

			//頁面檢查功能分類
			$data['pageName'] = "member_center_update";

			//登入後的頁面必需值
			if(!empty($memberLevel)){
				$data['memberLevel']=$memberLevel;
				if(!empty($userId)){
					$data['userId']=$userId;
				}
			}

			//登入成功處理
			if(isset($loginMember)){
				$data['loginChk']="sucess";
			}

			//登入資訊
			$memberData = $this->mmodel->getAllUserInfoByUserId($userId);
			$data['memberName'] = $memberData[0]['m_name'];
			$data['loginInfo'] = "您登入了".$memberData[0]['m_login']."次";

			//右側Menu項目
			$menu_arr = array('menuBtn_login'=>"登出會員系統", 'helper'=>"需要協助", 'contact'=>"聯絡我們", 'menuBtn_memberInfo'=>'會員資訊', 'menuBtn_autobiography'=>"版主經歷與介紹");
			//右側Menu項目icon檔名
			$icon_arr = array('menuBtn_login'=>"menuIcon_logout.png", 'helper'=>"menuIcon_helper.png", 'contact'=>"menuIcon_contact.png", 'menuBtn_memberInfo'=>"menuIcon_memberInfo.png", 'menuBtn_autobiography'=>"menuIcon_apply.png");
			//右側Menu印出內文
			$data['menuContext'] = $this->initial->menuBarPrintedContext($menu_arr, $icon_arr, $data['images_root']);

      //如已經登入，則紀錄寫入Session，視窗不再出現
      $data['loginAutobiography'] = $this->session->userdata("loginAutobiography");

			$this->load->view('/front/home_header',$data);
			$this->load->view('/front/member_center_update_body',$data);
			$this->load->view('/front/home_footer',$data);

		}

		public function updateCenterInfo($userId){

			$loginMember = $this->session->userdata('loginMember');

			// #第一次進入時的查詢
			if(isset($userId)){
				$query_Member=$this->mmodel->getAllUserInfoByUserId($userId);
			}

			#返回時的查詢
			if(!empty($loginMember)){
				$query_Admin=$this->mmodel->getAllUserInfoByUserName($loginMember);
				$return_id=$query_Admin[0]['m_id'];

			}

			$redirector="";
			$isChangPw=false;

			$input_password=$_POST["input_password"];
			$input_confirmPW=$_POST["input_confirmPW"];

			$input_name=trim($_POST["input_name"]);
			$input_sex=$_POST["input_sex"];
			$input_birthday=trim($_POST["input_birthday"]);
			$input_url=trim($_POST["input_url"]);
			$input_phone=trim($_POST["input_phone"]);
			$input_addr=trim($_POST["input_addr"]);
			$input_email=trim($_POST["input_email"]);

			$input_data=array();

			if ($input_password=="" && $input_confirmPW=="") {

				$input_data['m_name'] = $input_name;
				$input_data['m_sex'] = $input_sex;
				$input_data['m_birthday'] = $input_birthday;
				$input_data['m_url'] = $input_url;
				$input_data['m_phone'] = $input_phone;
				$input_data['m_address'] = $input_addr;
				$input_data['m_email'] = $input_email;

				$result = $this->mmodel->updateMemberInfo($input_data, $userId);

				$isChangPw=false;
			}

			if (($input_password!="") && ($input_password==$input_confirmPW)) {

				$input_data['m_name'] = $input_name;
				$input_data['m_sex'] = $input_sex;
				$input_data['m_birthday'] = $input_birthday;
				$input_data['m_url'] = $input_url;
				$input_data['m_phone'] = $input_phone;
				$input_data['m_address'] = $input_addr;
				$input_data['m_email'] = $input_email;

				$input_data['m_passwd'] = base64_encode($input_password);

				$result = $this->mmodel->updateMemberInfo($input_data, $userId);

				$isChangPw=true;
			}

			// #重新導向回到主畫面
			if($isChangPw){
				if($result){
					$redirector="/home/logout";
				}else{
					$redirector="/member_center/index/".$userId."/notUpdate";
				}
			}else{
				if($result){
					$redirector="/member_center/index/".$userId."/isUpdate";
				}else{
					$redirector="/member_center/index/".$userId."/notUpdate";
				}
			}

			header("Location: ".$redirector);
		}

		public function checkOrignalPW(){

			$result = $this->mmodel-> getAllUserInfoByUserId($_POST['m_id']);

			echo base64_decode($result[0]['m_passwd']);

		}


	}
?>