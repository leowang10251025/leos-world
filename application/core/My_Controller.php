<?php
	
	ini_set("display_errors", "on");
	error_reporting(E_ALL & ~E_NOTICE);

	class My_Controller extends CI_Controller{

		//-----------------------------------------------------------------------------------
		// 編號	 ：1
		// 函數名：script_message($str, $url='')
		// 作 用 ：跳出提示訊息(沒先跳出alert對話框)
		// 參 數 ：$str 訊息
		// $url 是否跳轉頁面
		// 返回值：無
		// 備 注 ：無
		//-----------------------------------------------------------------------------------
		public function script_message($isAlert=false, $str, $url='', $type='')
		{
			echo '<script>';

			if($isAlert){
				echo 'alert("'.$str.'");';
			}

			if($url != '' && $type == ''){
				echo 'window.location.href="'.$url.'";';
			}
			else if($url != '' && $type == 'top'){
				echo 'location.href="'.$url.'";';
			}

			echo '</script>';				
		}

		

		public function sendEmail($sendFrom='',$sendFromName='', $sendTo='', $title='',$message='', $bcc='')
		{
			//----匯入PHPMailer library(寄信程式)----
			$this->load->library('PHPMailer/PHPMailer');
			$mail= new Phpmailer();          //建立新物件
			$mail->IsSMTP();                 //設定使用SMTP方式寄信
			$mail->SMTPAuth = true;          //設定SMTP需要驗證
			$mail->SMTPSecure = "ssl";       // Gmail的SMTP主機需要使用SSL連線
			$mail->Port = 465;		         //Gamil的SMTP主機的SMTP埠位為465埠。
			$mail->Host = "smtp.gmail.com";  //"smtp.gmail.com";  //Gamil的SMTP主機
			$mail->CharSet = "utf-8";        //設定郵件編碼，預設UTF-8
			$mail->Username = 'leowang1025rd@gmail.com';
			$mail->Password = 'Whatsnew10251o25';
			// $mail->SMTPAuth = true;
			// $mail->SMTPSecure = 'tls';
			// $mail->Port = 587;

			$mail->From = $sendFrom; 									//設定寄件者信箱
			$mail->FromName = $sendFromName; 							//設定寄件者姓名
			//"=?UTF-8?B?".base64_encode($user_name)."?=<".$account.">";
			$mail->AddAddress($sendTo); 								//設定收件者郵件及名稱

			$mail->Subject = "=?UTF-8?B?".base64_encode($title)."?="; 	//設定郵件標題
			$mail->Body = $message; //設定郵件內容
			$mail->IsHTML(true); //設定郵件內容為HTML

			if($bcc)
			{
				foreach($bcc as $key=>$val)
				{
					$mail->AddBCC($val);
				}
			}

			if(!$mail->Send()) {
				// return '0';
				//$info="送信失敗: " . $mail->ErrorInfo;
				//echo $mail->ErrorInfo;
				return '0';
			}
			else {
				return '1';//$info="送信成功!";
			}
		}

	} 
?>