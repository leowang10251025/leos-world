<?php

class Main_model extends CI_Model{

	public function __construct(){
		parent:: __construct();
		$this->load->database();
	}

	public function getContent($where='1'){

		$sql = 'SELECT * FROM `main_content` WHERE `mid` = '.$where;

		return $this->db->query($sql)->result_array();

	}

	public function getOneBoradAdmin($adminUserName){

		$sql = "SELECT * FROM `board_admin` WHERE `m_username` = '".$adminUserName."'";

		return $this->db->query($sql)->result_array();
	}

	public function getOneBoradAdminById($adminId){

		$sql = "SELECT * FROM `board_admin` WHERE `m_id` = '".$adminId."'";

		return $this->db->query($sql)->result_array();
	}	

	public function getAllUserInfoByUserId($userId){

		$sql = 'SELECT * FROM `memberdata` WHERE `m_id` = "'.$userId.'"';

		return $this->db->query($sql)->result_array();
	}

	public function getAllUserInfoByMemberLevel($memberLevel){

		$sql = 'SELECT * FROM `memberdata` WHERE `m_level` = "'.$memberLevel.'"';

		return $this->db->query($sql)->result_array();
	}

	public function getAllUserInfoByMemberLevelAndUID($memberLevel, $userId){

		$sql = 'SELECT * FROM `memberdata` WHERE `m_level` = "'.$memberLevel.'" AND `m_id` = '.$userId;

		return $this->db->query($sql)->result_array();
	}

	public function getAllUserInfoByUserName($userName){

		$sql = 'SELECT * FROM `memberdata` WHERE `m_username` = "'.$userName.'"';

		return $this->db->query($sql)->result_array();
	}

	public function getAllUserInfoByUserMail($mail){

		$sql = 'SELECT * FROM `memberdata` WHERE `m_email` = "'.$mail.'"';

		return $this->db->query($sql)->result_array();
	}

	public function getLimitUserInfoByPageNum($startRowPage, $numRowPage, $memberLevel, $isLimit){

		$sql = "SELECT * FROM `memberdata` WHERE `m_level` <> '".$memberLevel."'  ORDER BY `m_jointime` DESC ";
		
		if($isLimit == true){
			$sql .= 'LIMIT '.$startRowPage .','.$numRowPage;
		}

		return $this->db->query($sql)->result_array();
	}

	public function getOneCommentById($board_id){

		$sql = 'SELECT * FROM `board` WHERE `board_id` = '.$board_id;

		return $this->db->query($sql)->result_array();
	}

	public function getAllCommentByType($isAll, $type='', $isLimit, $startNumRec, $endNumRec){

		$sql = 'SELECT * FROM `board`';

		if($isAll == false){
			$sql .= ' WHERE `comment_type` = "'.$type.'"';
		}

		$sql .= ' ORDER BY `board_time` DESC';

		if($isLimit == true){
			$sql .= ' LIMIT '.$startNumRec.", ".$endNumRec;
		}

		return $this->db->query($sql)->result_array();
	}

  public function getCityInfoBy($whereField = '', $whereValue = ''){

		$sql = 'SELECT * FROM `city`';

    if($whereField !='' && $whereValue != ''){
      if(is_string($whereValue)){
        $sql .= ' WHERE `'.$whereField.'` = "'.$whereValue.'"';
      }else{
        $sql .= ' WHERE `'.$whereField.'` = '.$whereValue;
      }
    }

		return $this->db->query($sql)->result_array();
	}

  public function getDistrictBy($whereField = '', $whereValue = ''){
    
    $sql = 'SELECT * FROM `district`';

    if($whereField !='' && $whereValue != ''){
      if(is_string($whereValue)){
        $sql .= ' WHERE `'.$whereField.'` = "'.$whereValue.'"';
      }else{
        $sql .= ' WHERE `'.$whereField.'` = '.$whereValue;
      }
    }

    return $this->db->query($sql)->result_array();
  }

  public function getDistrictInfo($cityId){
    
    $sql = "SELECT `id`, `name` FROM `district` WHERE `city-id` = ".$cityId;

    return $this->db->query($sql)->result_array();
  }

  public function getAutobiographyIntermediateTableData($basicId){
    
    $sql = "SELECT * FROM `autobiography_basic_edu_working` WHERE `autobiography-basic-id` = ".$basicId;

    return $this->db->query($sql)->result_array();
  }

	public function updateLoginTimeByUid($now_time, $user_login, $user_id){

		$sql = "UPDATE `memberdata` SET `m_logintime` = '".$now_time."', `m_login` = ".($user_login+1).
				" WHERE `m_id` = ".$user_id;

		$this->db->query($sql);
	}

	public function updateLoginPassWordByUserName($newPassword, $sendUsername){

		$sql = "UPDATE memberdata SET m_passwd = '".$newPassword."' WHERE m_username = '". $sendUsername ."'";

		$this->db->query($sql);
	}

	public function updateMemberInfo($input_data, $user_id){

		$sql = "UPDATE memberdata SET ";

		foreach ($input_data as $key => $value) {
			$sql .= $key." = '".$value."', ";
		}
		
		$sql = substr($sql,0,-2);
		$sql .= " WHERE m_id = ". $user_id;

		$result=$this->db->query($sql);
		return $result;

	}

	public function updateCommentData($input_arr, $board_id){

		$sql = "UPDATE `board` SET ";

		foreach ($input_arr as $key => $value) {
			$sql .= $key." = '".$value."', ";
		}
		
		$sql = substr($sql,0,-2);
		$sql .= " WHERE `board_id` = ". $board_id;

		$result=$this->db->query($sql);
		return $result;

	}

  public function updateAutobiographyBasic($input_arr, $id){
		$sql = "UPDATE `autobiography_basic_` SET ";

		foreach ($input_arr as $key => $data) {
      if($key == "gender" || $key == "age" || $key == "military-service-status" || $key == "military-service" || $key == "job-status"){
        $sql .= "`".$key."` = '".$data."', ";
      }else if($key == "contact-method" || $key == "driving-licence" ||$key == "driving-tool"){
        $sql .= "`".$key."` = '".$data."', ";
      }else{
        $sql .= "`".$key.'` = "'.$data.'", ';
      }
		}
		
		$sql = substr($sql,0,-2);
		$sql .= " WHERE `id` = ". $id;

		$result = $this->db->query($sql);
		return $result;
	}

  public function updateAutobiographyEducation($input_arr, $id){
		$sql = "UPDATE `autobiography_education` SET ";

		foreach ($input_arr as $key => $value) {
      $sql .= "`".$key."` = '".$value."', ";
		}
		
		$sql = substr($sql,0,-2);
		$sql .= " WHERE `id` = ". $id;

		$result=$this->db->query($sql);
		return $result;
	}

  public function updateAutobiographyWorkingExp($input_arr, $id){
		$sql = "UPDATE `autobiography_working_exp` SET ";

		foreach ($input_arr as $key => $value) {
      $sql .= "`".$key."` = '".$value."', ";
		}
		
		$sql = substr($sql,0,-2);
		$sql .= " WHERE `id` = ". $id;

		$result=$this->db->query($sql);
		return $result;
	}

	public function addMemberData($input_data){

		$query="INSERT INTO `memberdata` (`m_name`, `m_username`, `m_passwd`, `m_sex`, `m_email`,`m_birthday`, `m_url`, `m_phone`,
		`m_address`, `m_jointime`, `m_login`, `m_logintime`, `m_level`) VALUES ( ";

		foreach ($input_data as $key => $value) {

			if($key != "m_login"){
				$query .="'".$value."',";

			}else if($key == "m_login"){
				$query .="".$value.",";
			}
		}
		$query = substr($query,0,-1);
		$query .=")";

		$returnId = $this->db->query($query);
		return $returnId;
	}

	public function addComment($input_data){

		$query="INSERT INTO `board`  (`board_name`,
		`board_sex`, `board_subject`, `board_time`, `board_mail`, `board_web`, 
		`board_content`, `comment_type`) VALUES (";

		foreach ($input_data as $key => $value) {
			$query .="'".$value."',";
		}

		$query = substr($query,0,-1);
		$query .=")";

		$returnId = $this->db->query($query);

		return $returnId;
	}

  public function addAutobiographyBasicData($input_data){

		$query = "INSERT INTO `autobiography_basic_` (";
    foreach ($input_data as $key => $value) {
      $query .="`".$key."`, ";
		}

    $query = substr($query, 0, -2);
    $query .=") VALUES ( ";

		foreach ($input_data as $key => $data) {
      if($key == "gender" || $key == "age" || $key == "military-service-status" || $key == "military-service" || $key == "job-status"){
        $query .= $data.', ';
      }else if($key == "contact-method" || $key == "driving-licence" ||$key == "driving-tool"){
        $query .="'".$data."', ";
      }else{
        $query .='"'.$data.'", ';
      }
		}

    $query = substr($query, 0, -2);
    $query .= ")";

    $db = $this->db;
    $db->query($query);

    return $db->insert_id();
	}

  public function addAutobiographyIntermediateTableData($basicId, $foreignKey1, $foreignKey2){

    $tmpArr1 = array("autobiography-basic-id", "autobiography-education-id", "autobiography-working-exp-id");
    $tmpArr2 = array($basicId, $foreignKey1, $foreignKey2);

    $query = "INSERT INTO `autobiography_basic_edu_working` (";
    foreach ($tmpArr1 as $field) {
      $query .="`".$field."`, ";
		}
    $query = substr($query, 0, -2);

    $query .=") VALUES ( ";
    foreach ($tmpArr2 as $val) {
        $query .= $val.', ';
		}
    $query = substr($query, 0, -2);
    $query .= ")";
    $result = $this->db->query($query);

    if(isset($basicId) && $result == true){
      return true;
    }else{
      return false;
    }
  }

  public function addAutobiographyEducationData($input_data){
    $query = "INSERT INTO `autobiography_education` (";
    foreach ($input_data as $key => $value) {
      $query .="`".$key."`, ";
		}
    $query = substr($query,0,-2);
    $query .=") VALUES ( ";

		foreach ($input_data as $key => $data) {
      if($key == "graduate-status"){
        $query .= $data.", ";
      }else{
        $query .= "'".$data."', ";
      }
		}

		$query = substr($query,0,-2);
		$query .=")";

    $db = $this->db;
    $db->query($query);

		return $db->insert_id();
  }

  public function addAutobiographyWorkingExpData($input_data){
    $query = "INSERT INTO `autobiography_working_exp` (";
    foreach ($input_data as $key => $value) {
      $query .="`".$key."`, ";
		}
    $query = substr($query,0,-2);
    $query .=") VALUES ( ";

		foreach ($input_data as $key => $data) {
      $query .= "'".$data."', ";
		}

		$query = substr($query,0,-2);
		$query .=")";

		$db = $this->db;
    $db->query($query);

		return $db->insert_id();
  }

  public function getMemberDataBy($whereColumn, $whereValue){
    $sql = "SELECT * FROM `memberdata`";
    if(isset($whereColumn) && isset($whereValue)){
      if(is_string($whereValue)){
        $sql .= " WHERE `".$whereColumn."` = '".$whereValue."'";
      }else{
        $sql .= " WHERE `".$whereColumn."` = '".$whereValue."'";
      }
    }
    return $this->db->query($sql)->result_array();
  }

	public function getLastestUserInfo(){

		$sql = "SELECT * FROM `memberdata` ORDER BY `m_id` DESC LIMIT 1 ";

		return $this->db->query($sql)->result_array();
	}

  public function getAllExpInfo($getDataColumnArr, $basicId, $joinTable, $relateKey){

		$sql = "SELECT DISTINCT";
    foreach($getDataColumnArr as $column){
      $sql .= "`".$column."`, ";
    }
    $sql = substr($sql,0,-2);
    $sql .= " FROM `autobiography_basic_edu_working` AS `aw`";
    $sql .= " LEFT JOIN `".$joinTable."` AS `a`";
    $sql .= " ON `aw`.`".$relateKey."` = `a`.`id`";
    $sql .= "WHERE `autobiography-basic-id` = ".$basicId;

		return $this->db->query($sql)->result_array();
	}

  public function getAutobiographyData($whereId = 0, $orderType = 'ASC', $limitNum = 0){

		$sql = "SELECT * FROM `autobiography_basic_`";
    if($whereId != 0){
      $sql .= "WHERE `id` = ".$whereId;
    }
    $sql .= " ORDER BY `id` ".$orderType;
    if($limitNum != 0){
      $sql .= " LIMIT ".$limitNum;
    }
    
		return $this->db->query($sql)->result_array();
	}

  public function getAutobiographyEducationData($whereColumn, $whereValue){

		$sql = "SELECT * FROM `autobiography_education`";
    if($whereColumn != "" && $whereValue != ""){
      if(is_string($whereValue)){
        $sql .= "WHERE `".$whereColumn.'` = "'.$whereValue.'"';
      }else{
        $sql .= "WHERE `".$whereColumn.'` = '.$whereValue;
      }
    }
    
		return $this->db->query($sql)->result_array();
	}

  public function getAutobiographyWorkingExpData($whereColumn, $whereValue){

		$sql = "SELECT * FROM `autobiography_working_exp`";
    if($whereColumn != "" && $whereValue != ""){
      if(is_string($whereValue)){
        $sql .= "WHERE `".$whereColumn.'` = "'.$whereValue.'"';
      }else{
        $sql .= "WHERE `".$whereColumn.'` = '.$whereValue;
      }
    }
    
		return $this->db->query($sql)->result_array();
	}

	public function deleteUserInfoByUserId($userId){

		$sql = "DELETE FROM `memberdata` WHERE `m_id` = ".$userId;

		return $this->db->query($sql);

	}

	public function deleteOneCommentByBoardId($boardId){

		$sql = "DELETE FROM `board` WHERE `board_id` = ".$boardId;

		return $this->db->query($sql);

	}

  public function getAutobiographyViewById($viewId){
    $sql = "SELECT `content`, `assist-title`, `assist-content` FROM autobiography_view WHERE id = ".$viewId;

    return $this->db->query($sql)->result_array();
  }

	
}

?>