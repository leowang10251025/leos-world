<?php
class Initial
{

  public function __construct()
  {
    $this->_ci = &get_instance();
    $this->_ci->load->library('session');
    $this->_ci->load->library('tools/basicTool', '', 'basicTool');
  }

  public function init()
  {

    $data = array();
    $data['host_root'] = "http://" . $_SERVER['HTTP_HOST'];
    $data['css_root'] = "/css";
    $data['js_root'] = "/js";
    $data['images_root'] = $data['host_root'] . "/images/";
    $data['date_now'] = date("Y-m-d");
    $data['boardAdminLogin'] = $this->_ci->basicTool->boardAdminLoginChk();

    return $data;
  }

  public function getHomeContext($page)
  {

    $data = array();

    $CI = &get_instance();
    $CI->load->model("front/main_model", "mmodel");

    $content = $CI->mmodel->getContent($page);
    foreach ($content as $arr) {
      foreach ($arr as $key => $value) {
        if ($key == "title") {
          $data['title'] = $value;
        }
        if ($key == "sub_title") {
          $data['sub_title'] = $value;
        }
        if ($key == "content") {
          $data['content'] = $value;
        }
      }
    }

    return $data;
  }


  public function menuBarPrintedContext($menu_arr, $icon_arr, $images_root = '')
  {
    $printedContext = "";

    foreach ($menu_arr as $button_id => $button_text) {

      foreach ($icon_arr as $icon_id => $icon_name) {
        if ($button_id == $icon_id) {
          $file_name = $icon_name;
        }
      }
      $printedContext .= "<tr width='100%' height='70' id='menuBtn' style='background-image:url(" . $images_root . "/pop_menu_buttom_bg.png);'>";
      $printedContext .= "<td align='center'><img id='button_icon' src='" . $images_root . $file_name . "' width='32' height='32' border='0' align='absmiddle' alt='" . $button_text . "'></td>";
      $printedContext .= "<td><input class='button_text' id='" . $button_id . "' type='button' value='" . $button_text . "'/></td>";
      $printedContext .= "</tr>";
    }

    return $printedContext;
  }

  public function adjustConstellations($birthday)
  {
    $constellations = '';

    if (strtotime($birthday) >= strtotime(date('Y-01-20')) && strtotime($birthday) <= strtotime(date('Y-02-18'))) {
      $constellations = '水瓶座 (Aquarius)';
    } elseif (strtotime($birthday) >= strtotime(date('Y-02-19')) && strtotime($birthday) <= strtotime(date('Y-03-20'))) {
      $constellations = '雙魚座 (Pisces)';
    } elseif (strtotime($birthday) >= strtotime(date('Y-03-21')) && strtotime($birthday) <= strtotime(date('Y-04-20'))) {
      $constellations = '牧羊座 (Aries)';
    } elseif (strtotime($birthday) >= strtotime(date('Y-04-21')) && strtotime($birthday) <= strtotime(date('Y-05-20'))) {
      $constellations = '金牛座 (Taurus)';
    } elseif (strtotime($birthday) >= strtotime(date('Y-05-21')) && strtotime($birthday) <= strtotime(date('Y-06-21'))) {
      $constellations = '雙子座 (Gemini)';
    } elseif (strtotime($birthday) >= strtotime(date('Y-06-22')) && strtotime($birthday) <= strtotime(date('Y-07-22'))) {
      $constellations = '巨蟹座 (Cancer)';
    } elseif (strtotime($birthday) >= strtotime(date('Y-07-23')) && strtotime($birthday) <= strtotime(date('Y-08-22'))) {
      $constellations = '獅子座 (Leo)';
    } elseif (strtotime($birthday) >= strtotime(date('Y-08-23')) && strtotime($birthday) <= strtotime(date('Y-09-22'))) {
      $constellations = '處女座 (Virgo)';
    } elseif (strtotime($birthday) >= strtotime(date('Y-09-23')) && strtotime($birthday) <= strtotime(date('Y-10-23'))) {
      $constellations = '天平座 (Libra)';
    } elseif (strtotime($birthday) >= strtotime(date('Y-10-24')) && strtotime($birthday) <= strtotime(date('Y-11-22'))) {
      $constellations = '天蠍座 (Scorpio)';
    } elseif (strtotime($birthday) >= strtotime(date('Y-11-23')) && strtotime($birthday) <= strtotime(date('Y-12-21'))) {
      $constellations = '射手座 (Sagittarius)';
    } elseif (strtotime($birthday) >= strtotime(date('Y-12-22')) && strtotime($birthday) < strtotime(date('Y-01-20'))) {
      $constellations = '摩羯座 (Capricorn)';
    } else {
      $constellations = '不提供喔!';
    }

    return $constellations;
  }
}
