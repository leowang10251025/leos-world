<?php
	/**
	 * 類別名稱 	: 基本工具類別
	 * 作者		: Leo
	 * 撰寫日期	: 2020/09/10
	 * 用途		: 
	 *
	 */
	class BasicTool
	{
		
		public function __construct()
		{
			$this->_ci =& get_instance();
			$this->_ci->load->library('session');
			session_start();
		}


		//-----------------------------------------------------------------------------------
		// 編號	 ：1
		// 函數名：script_message($str, $url='')
		// 作 用 ：跳出提示訊息(沒先跳出alert對話框)
		// 參 數 ：$str 訊息
		// $url 是否跳轉頁面
		// 返回值：無
		// 備 注 ：無
		//-----------------------------------------------------------------------------------
		public function script_message($isAlert=false, $str, $url='', $type='')
		{
			echo '<script>';

			if($isAlert){
				echo 'alert("'.$str.'");';
			}

			if($url != '' && $type == ''){
				echo 'window.location.href="'.$url.'";';
			}
			else if($url != '' && $type == 'top'){
				echo 'location.href="'.$url.'";';
			}

			echo '</script>';				
		}

		

		public function sendEmail($sendFrom='',$sendFromName='', $sendTo='', $title='',$message='', $bcc='')
		{
			//----匯入PHPMailer library(寄信程式)----
			$this->_ci->load->library('PHPMailer/PHPMailer');
			$mail= new Phpmailer();           //建立新物件
			$mail->IsSMTP();                  //設定使用SMTP方式寄信
			$mail->SMTPAuth = true;           //設定SMTP需要驗證
			$mail->SMTPSecure = "ssl";        // Gmail的SMTP主機需要使用SSL連線
			$mail->Port = 465;		            //Gamil的SMTP主機的SMTP埠位為465埠。
			$mail->Host = "smtp.gmail.com";   //"smtp.gmail.com";  //Gamil的SMTP主機
			$mail->CharSet = "utf-8";         //設定郵件編碼，預設UTF-8
			$mail->Username = 'leowang1025rd@gmail.com';
			$mail->Password = 'Whatsnew10251o25';
			// $mail->SMTPAuth = true;
			// $mail->SMTPSecure = 'tls';
			// $mail->Port = 587;

			$mail->From = $sendFrom; 									//設定寄件者信箱
			$mail->FromName = $sendFromName; 							//設定寄件者姓名
			//"=?UTF-8?B?".base64_encode($user_name)."?=<".$account.">";
			$mail->AddAddress($sendTo); 								//設定收件者郵件及名稱

			$mail->Subject = "=?UTF-8?B?".base64_encode($title)."?="; 	//設定郵件標題
			$mail->Body = $message; //設定郵件內容
			$mail->IsHTML(true); //設定郵件內容為HTML

			if($bcc)
			{
				foreach($bcc as $key=>$val)
				{
					$mail->AddBCC($val);
				}
			}

			if(!$mail->Send()) {
				// return '0';
				//$info="送信失敗: " . $mail->ErrorInfo;
				//echo $mail->ErrorInfo;
				return '0';
			}
			else {
				return '1';//$info="送信成功!";
			}
		}

		public function boardAdminLoginChk()
		{
			$boardAdminName = $this->_ci->session->userdata('boardAdminName');
			$boardAdminLogin= 'false';
			if(isset($boardAdminName) && !empty($boardAdminName)){
				if($boardAdminName !=""){
					$boardAdminLogin = 'sucess';
				}
			}

			return $boardAdminLogin;
		}

		public function autoLogout($time, $reloadUrl='')
		{
      error_reporting(E_ALL^E_NOTICE^E_WARNING);
			echo '<script>';
			echo 'timeout();';

			$uid = $this->_ci->session->userdata('userId');
			//返回未登入首頁
			if(empty($uid)){
				echo 'location.href="/home/timeout";';
			}
			echo 'function timeout(){setTimeout(function(){window.location.reload("'.$reloadUrl.'");}, '.$time.' * 1000);}';			
			echo '</script>';
		}

		public function deleteLoginSession()
		{
			//清除SESSION內資料(會員資料)
			$this->_ci->session->unset_userdata('loginMember'); 
			$this->_ci->session->unset_userdata('memberLevel');
			$this->_ci->session->unset_userdata('loginTime');
			$this->_ci->session->unset_userdata('userId');
			//清除SESSION內資料(留言板管理者資料)
			$this->_ci->session->unset_userdata('boardAdminName'); 
			$this->_ci->session->unset_userdata('boardAdminUserName');
		}

	}
?>