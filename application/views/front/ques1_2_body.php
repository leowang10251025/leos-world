			<style type="text/css">
			<!--
				#exp_content{
					cursor: help;
				}
				
				#light_button:hover{
					cursor: pointer;
				}
				
				#point1_2_1:hover,#point1_2_2:hover,#point1_2_3:hover{
					cursor: crosshair;
				}
				
				.point{
					width:20%;
					font-family: "微軟正黑體";
					font-size: 14pt;
					color: #FF3300;
					text-align: center;
					font-style:italic;
					font-weight: bolder;
				}
				
				.point_content{
					font-family: "微軟正黑體";
					font-size: 11pt;
					color: #242376;
					font-weight: normal;
				}
				
				#point1_2_1{
					margin-top: 260px;
					margin-left: 5px;
				}
				
				#point1_2_2{
					margin-top: 260px;
					margin-left: 3px;
				}
				
				#point1_2_3{
					margin-top: 225px;
					margin-left: 1px;
				}
				
				#point1_2_1_content{
					margin-top: 5px;
					margin-left: 15px;
				}
				
				#point1_2_2_content{
					margin-top: 5px;
					margin-left: 15px;
					margin-right: 5px;
				}
				
				#point1_2_3_content{
					margin-top: 5px;
					margin-left: 8px;
					margin-right: 13px;
				}
				
				.photo{
					cursor:auto;
					padding-top:45px;
					background-attachment:scroll;
					background-position:center,center;
					background-repeat:no-repeat;
				}
				
				.player_button1 {
						margin-top: 8px;
						margin-bottom: 8px;
						margin-left: 5px;
						margin-right: 5px;
				}
					
				.player_skin1 {
					margin-top: 4px;
					margin-left: 115px;
					margin-bottom: 8px;
				}
			-->
			</style>
			
			<!--中間區段2-->
			<tr align="left">
				<td align="center" width="100%" height="600">
					<table id="board_main" width="100%"  height="100%" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
						<tr valign="middle">
							<td valign="top" align="center" width="75%" height="100%" style="background-color: #ffffff;">
								<table id="maincontent4" width="1000" height="100%" border="0" cellpadding="0" cellspacing="0" align="left"  valign="top" >
									<tr  height="100%" width="100%">
										<td>
											<div id="mainRegion4">
												<form name="formAns" method="post" action="">
												<table width="93%" height="100%" border="0" align="center" cellpadding="0" cellspacing="1"> 													
													<tr valign="top" align="center">
														<td colspan="2" class="heading">【瀏覽網頁主要功能建議方式】</td>
													</tr>
													<tr valign="top">
														<td background="<?=$images_root;?>/ques1-2.png" width="900" height="450" class="photo"  align="center"  valign="middle">
															<table width="93%" height="100%" border="0" cellpadding="0" cellspacing="0" align="center"  valign="top" >
																<tr width="93%" height="100%" >
																	<td width="32%"  align="left" valign="top">
																		<p class="point" id="point1_2_1">建議一:</p>
																		<div class="point_content" id="point1_2_1_content">
																			<em>
																				總共有四個標籤內容，分別是不同有關版面主題的說明，
																				可以從這邊了解這裡再做些什麼、搞些什麼挖糕瞜!
																			</em>
																		</div>	
																	</td>
																	<td width="32%" align="left" valign="top">
																		<p class="point" id="point1_2_2">建議三:</p>
																		<div class="point_content" id="point1_2_2_content">
																			<em>
																				如果你已經瀏覽完各頁面的內容，皆可以按下"Leo's World"字樣返回首頁瞜~
																			</em>
																		</div>	
																	</td>
																	<td width="32%" align="left" valign="top">
																		<p class="point" id="point1_2_3">建議二:</p>
																		<div class="point_content" id="point1_2_3_content">
																			<em>
																				如果您使用某些功能後，有發現任何問題或任何異常，請按"需要協助"選項，在裡面的內容應該可以幫助你找到答案喔!或者還有其他未預期的狀況，也可以在下方的選項"連絡我們"告知管理員問題與狀況喔!
																			</em>
																		</div>	
																	</td>
																</tr>	
															</table>	
														</td>
													</tr>
													<tr valign="middle" align="right">
														<td >
															<p align="left">
																<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;margin-top: 60px;" />
																<table width="100%" height="100%">
																	<tr align="center">
																		<td align="left" valign="middle">
																			<p align="left" valign="middle">
																				<strong>
																					※當你第一次進入首頁時或進入"需要協助"時，我們會建議你從<a id="exp_content" href="#point1_2_1">標籤頁</a>點選，
																					<br/>&nbsp;&nbsp;去查看你想要看的內容，使用過程中難免遇到一些狀況無法解決，你可以選擇
																					<br/>&nbsp;&nbsp;每個頁面右上角的
																				</strong>
																				<strong>
																					<a id="exp_content" href="#point1_2_2">協助與聯絡連結</a>，跟我們聯絡，我們會盡可能提供你最好的
																					<br/>&nbsp;&nbsp;解決方式。當你停留在各頁面想回首頁時，可以點選<a id="exp_content" href="#point1_2_3">回首頁字樣連結</a>即可返回。
																				</strong>
																			</p>	
																		</td>
																		<td align="right" valign="middle">
																			<!--a href="javascript:window.history.back()"><img id="light_button" src="<?=$images_root;?>/Light-1.png"/></a-->
																			<input type="image" id="light_button1" src="<?=$images_root;?>/Light-1.png" value="solve" onClick="return checkForm(this.id);"/>
																			<input type="image" id="light_button2" src="<?=$images_root;?>/Light-2.png" value="issue" onClick="return checkForm(this.id);"/>
																			<input type="image" id="reply_button1" src="<?=$images_root;?>/ques_reply.png" value="reply" onClick="return checkForm(this.id);"/>
																		</td>
																	</tr>
																</table>
															</p>	
														</td>
													</tr>
												</table>
											</form>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<!--右下方圖案-->
							<td valign="bottom" align="right" width="10%" style="background-color: #ffffff;">
								<div id="corner_img_div" style="display: none;">
									<img id="corner_img" src="<?=$images_root;?>/talking.png" style="margin-right: 30px;margin-bottom: 15px;">
								</div>
							</td>
							<!--右下方圖案-->
							<!--Menu跳出小視窗1-->
							<div id="pop_menu_login" class="pop_menu" style="display: none;">
								<!-- 中間區塊2-2的跳出小視窗1  -->
								<form name="formReg" id="formReg" method="post" >
									<p class="heading" align="center">&#9674; &#9830; 登出會員系統 &#9830; &#9674;</p>
									<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
									<p align="center" class="smalltext7">&#8855;  如有修改會員、管理員密碼 &#8855;<br/>&#8855; 請重新登入!謝謝! &#8855; </p>
									<p align="center" style="opacity: 50%">
										<a href="/home/logout" >
											<img class="button_margin_1" name="index_logout" id="index_logout" src="<?=$images_root;?>/buttom_logout_1.png">
										</a>
									</p>
								<form/>
							</div>
							<!--Menu跳出小視窗1-->
              <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->
              <?php if(!isset($loginAutobiography) || $loginAutobiography != "sucess"){?>
              <div class="pop_menu" id="autobiography-pw-dialog" style="display: none;">
                <p class="context_regbox">
                  <label class="form-check-label" for="account-input" aria-describedby="inputGroupFileAddon02">自傳閱覽帳號: </label>
                  <input class="form-control account-input" id="account-input" type="text">
                </p>
                <p class="context_regbox">
                  <label class="form-check-label" for="password-input" aria-describedby="inputGroupFileAddon02">自傳閱覽密碼: </label>
                  <input class="form-control password-input" id="password-input" type="password">
                </p>
                <p class="align-center" style="opacity: 50%;">
                  <input  class="button_margin_1 password-submit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
                </p>
              </div>
              <?php } ?>
              <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->
							<!--中間區塊2-2-->
							<td id="regbox" align="center" valign="top" width="15%">
								<table id="pop_menu_content" width="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="top">
									<?=$menuContext;?>
								</table>
								<div id="menuLastArea" style="background-image:url(<?=$images_root;?>/pop_menu_buttom_bg.png);"></div>
							</td>
							<!--中間區塊2-2-->
						</tr>
				</table>	
				</td>
			</tr>
			<!--中間區塊2-->