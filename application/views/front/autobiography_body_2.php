<div class="row my-4">
  <div class="col-10 shadow p-3 mb-5 bg-white rounded">
    <div class="card">
      <div class="card-header" id="headingTwo">
          <div class="row align-items-center">
            <div class="col-10">
              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#education">
                <h4><b>學歷</b></h4>  
              </button>
            </div>
            <div class="col-2">
              <div class="row text-center align-items-center" id="add-education">
                <div class="col-6 text-right">
                  <i class="fa fa-plus-square-o fa-lg" aria-hidden="true"></i>
                </div>
                <div class="col-6 text-left">
                  <h5>新增</h5>
                </div>
              </div>
            </div>
          </div>
      </div>

      <div id="education" class="collapse">
        <div class="card-body border-bottom" id='edu-info-1'>
          <div class="row mb-4 text-left align-items-center">
            <div class="col-11">
            </div>
            <!-- <div class="col-1 text-right remove-education" id="remove-education-1">
              <i class="fa fa-times fa-lg" aria-hidden="true"></i>
            </div> -->
          </div>  
          <div class="row mt-4 mb-4 text-left align-items-center">
            <div class="col-3">
              <input class="form-control" id="school-name-1" name="school-name-1" type="text" placeholder="請輸入學校名稱"/>
            </div>
            <div class="col-9 pull-right">
              <div class="row">
                <div class="col-3">
                </div>  
                <div class="col-4">
                  <input class="form-control learning-term" id="learning-term-start-1" name="learning-term-start-1" type="date"/>
                </div>
                <div class="col-1">
                  ~ 
                </div>
                <div class="col-4">
                  <input class="form-control learning-term" id="learning-term-end-1" name="learning-term-end-1" type="date"/>
                </div>
              </div>
            </div>
          </div>  
          <div class="row mt-4 mb-4 text-center text-secondary align-items-center">
            <div class="col-2">
              <input class="form-control" name="dept-name-1" id="dept-name-1" type="text" placeholder="請填入科系名"/>
            </div>
            <div class="col-1 border-left"> 
              <h6 class="font-weight-bold">狀態: </h6>
            </div>
            <div class="col-1"> 
              <input class="form-check-input" name="graduate-status-1" type="radio" value="1">畢業 
              </input>
            </div>
            <div class="col-1">  
              <input class="form-check-input" name="graduate-status-1" type="radio" value="2">肄業 
              </input>
            </div>
            <div class="col-2">  
              <input class="form-check-input" name="graduate-status-1" type="radio" value="3">轉學 / 其他 
              </input>
            </div>
            <div class="col-5">  
              <h6 class="text-secondary font-italic pull-right"> 學習期間(起~迄)</h6>
            </div>
          </div>
          <div class="row mt-4 mb-4 text-center text-secondary align-items-center">
            <div class="col-9">
            </div>
            <div class="col-3 text-right">
              <span class="education-time-range text-danger" id="education-time-range-1" name="education-time-range-1" type="date" title="學歷時間"></span>  
            </div>
          </div>         
        </div>
        <?php
          if(!empty($educationView)){
            foreach($educationView as $view){ //編輯時預設呈現
              echo $view;
            } 
          }
        ?> 
      </div>
    </div>
  </div>
  <div class="col-2">
  </div>
</div>