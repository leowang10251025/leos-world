<html>
	<head>
		<meta http-equiv="Content" Content-Type="text/html; charset=utf-8">
		<link rel="stylesheet" href="<?=$css_root;?>/front/style_1.css" type="text/css">
		<link rel="stylesheet" href="<?=$js_root;?>/lib/bootstrap-sweetalert-master/dist/sweetalert.css" >
		<title>Leo's 的個人資料</title>
		<script src="<?=$js_root;?>/lib/jquery-3.3.1.min.js"></script>
		<script src="<?=$js_root;?>/lib/bootstrap-sweetalert-master/dist/sweetalert.min.js"></script>
		<?php if(isset($pageName)){
				if($pageName == "pet_home"){ ?>
					<script src="<?=$js_root;?>/front/pet_home.js"></script>
		<?php 	}?>
		<?php }  ?>
	</head>