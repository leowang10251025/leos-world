			<!--中間區段2-->
			<tr align="left">
				<td align="center" width="100%" height="600">
					<table id="board_main" width="100%"  height="100%" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
						<tr valign="middle">
							<td valign="top" align="center" width="75%" height="100%" style="background-color: #E0E0EB;">
								<table id="maincontent5" width="80%" height="100%" border="0" cellpadding="0" cellspacing="0" align="left"  valign="top">
									<tr height="100%" width="100%" valign="top">
										<td valign="middle" align="left">
											<div id="mainRegion5">
												<table width="75%" height="400" cellpadding="0" cellspacing="0" border="0" align="center" id="contact_area">
													<tr valign="middle">
														<td id="board_heading"  colspan="2" class="heading" align="center">
														<h4><b><i>&#8501; 如何聯絡管理員? &#8501;</i></b></h4> 
														</td>
													</tr>
													<tr  valign="middle">
														<td class="board_list"  align="left">
															<p  class="context_regbox" style="padding-left: 5%;">
																<strong>【管理員"<?php  echo $adminMember["m_username"]; ?>"的資訊】</strong>
																<ul  class="context_regbox" style="padding-left: 10%;">
																	<li type="disc"><strong>暱稱 : </strong><?php  echo $adminMember["m_name"]; ?></li>
																	<li type="disc"><strong>性別 : </strong> <?php  if($adminMember["m_sex"]=="M"){echo "男"; }else{echo "女"; } ?></li>
																	<li type="disc"><strong>聯絡郵件 :  </strong>
																		<a onclick="windowOpen('/contact/inputMailContent/<?=$adminMember['m_id']?>', '留言修改', 800, 550)";>
																			<strong>
																				<?=$adminMember["m_email"];?>
																			</strong>
																		</a>	
																	<li type="disc"><strong>個人網站 : </strong>
																		<a href="<?php  echo $adminMember["m_url"]; ?>">
																			<strong>
																				<?php  echo $adminMember["m_url"]; ?>
																			</strong>
																		</a>
																	</li>
																	<li type="disc"><a href="/post/index/<?=$userId;?>/contact"><strong>有任何指教或聯絡請按此處!  </strong></a></li>
																</ul>
																<div align="center">	
																	<hr width="90%" style="background-color: #2F4F4F;height: 1px;" />
																</div>
															</p>
														</td><!--內文顯示-->
													</tr><!--表單內文填寫區塊-->
													<tr valign="middle">	
														<td align="center"  id="board_footer" colspan="3">	
															<a id="next-step" href="/contact/index/<?php if($numPage>=$totalPages){echo $totalPages;}else if($numPage<$totalPages){echo $numPage+1;} ?>">
																<img src="<?=$images_root;?>/sticker_button_next.png">
															</a>	
															<a id="previous-step" href="/contact/index/<?php if($numPage<=1){$numPage=1;  echo $numPage;}else{echo $numPage-1;}?>">
																<img src="<?=$images_root;?>/sticker_button_prev.png">
															</a>
															<a href="javascript:window.history.back()">
																<img src="<?=$images_root;?>/sticker_button_back.png">
															</a>
															<input type="hidden" id="numPage" class="numPage" value="<?=$numPage;?>"/>
															<input type="hidden" id="totalPages" class="totalPages" value="<?=$totalPages;?>"/>
															<?php if(!empty($sendStatus)){?>
																<input type="hidden" id="sendStatus" class="sendStatus" value="<?=$sendStatus;?>"/>
															<?php }?>	
														</td>
													</tr><!--表單內文填寫區塊-->
												</table>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<!--右下方圖案-->
							<td valign="bottom" align="right" width="10%" style="background-color: #E0E0EB;">
								<div id="corner_img_div" style="display: none;">
									<img id="corner_img" src="<?=$images_root;?>/talking.png" style="margin-right: 30px;margin-bottom: 15px;">
								</div>
							</td>
							<!--右下方圖案-->
							<!--Menu跳出小視窗1-->
							<div id="pop_menu_login" class="pop_menu" style="display: none;">
								<!-- 中間區塊2-2的跳出小視窗1  -->
								<form name="formReg" id="formReg" method="post" >
									<p class="heading" align="center">&#9674; &#9830; 登出會員系統 &#9830; &#9674;</p>
									<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
									<p align="center" class="smalltext7">&#8855;  如有修改會員、管理員密碼 &#8855;<br/>&#8855; 請重新登入!謝謝! &#8855; </p>
									<p align="center" style="opacity: 50%">
										<a href="/home/logout" >
											<img class="button_margin_1" name="index_logout" id="index_logout" src="<?=$images_root;?>/buttom_logout_1.png">
										</a>
									</p>
								<form/>
							</div>
							<!--Menu跳出小視窗1-->
              <!--Menu跳出小視窗3(經歷與自傳:輸入密碼)-->
              <?php if(!isset($loginAutobiography) || $loginAutobiography != "sucess"){?>
              <div class="pop_menu" id="autobiography-pw-dialog" style="display: none;">
                <p class="context_regbox">
                  <label class="form-check-label" for="account-input" aria-describedby="inputGroupFileAddon02">自傳閱覽帳號: </label>
                  <input class="form-control account-input" id="account-input" type="text">
                </p>
                <p class="context_regbox">
                  <label class="form-check-label" for="password-input" aria-describedby="inputGroupFileAddon02">自傳閱覽密碼: </label>
                  <input class="form-control password-input" id="password-input" type="password">
                </p>
                <p class="align-center" style="opacity: 50%;">
                  <input  class="button_margin_1 password-submit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
                </p>
              </div>
              <?php }?>  
              <!--Menu跳出小視窗3(經歷與自傳:輸入密碼)-->
							<!--中間區塊2-2-->
							<td id="regbox" align="center" valign="top" width="15%">
								<table id="pop_menu_content" width="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="top">
									<?=$menuContext;?>
								</table>
								<div id="menuLastArea" style="background-image:url(<?=$images_root;?>/pop_menu_buttom_bg.png);"></div>
							</td>
							<!--中間區塊2-2-->
						</tr>
				</table>	
				</td>
			</tr>
			<!--中間區塊2-->