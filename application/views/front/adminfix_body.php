<table  width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" align="left"  valign="top" class="msg_content_bg_2">
	<tr  height="100%" width="100%" valign="top">
		<td>
			<div class="open_msg_container">
				<form name="formAdminFix" id="formAdminFix" method="post" action="/adminfix/updateCommentData/<?=$commentData["board_id"];?>">
				<table width="95%" height="250" cellpadding="4" cellspacing="4" border="0" align="center">	
					<tr valign="top" align="center">
						<td id="board_heading" colspan="2" class="heading" style="padding-bottom: 30px;">
							<h4><b><i>【更新訪客留言版資料】</i></b></h4>
						</td>
					</tr>
					<tr class="context_regbox" valign="top" align="left">
						<td width="95%" style="padding-top: 20px;">
							<p>
								<span style="color: #ff0000;margin-top: 20px;">* </span>
								<label class="form-check-label" for="board_subject" aria-describedby="inputGroupFileAddon02">標題 :</label>
								<input type="text" class="form-control" id="board_subject" name="board_subject" style="width: 85%" value="<?php echo $commentData["board_subject"];?>" placeholder='請輸入留言標題'>
							</p>
							<p><span style="color: #ff0000;">* </span>
								<label class="form-check-label" for="board_name" aria-describedby="inputGroupFileAddon02">姓名 :</label>
								<input type="text" class="form-control" id="board_name" name="board_name" style="width: 85%" value="<?php echo $commentData["board_name"];?>" placeholder='請輸入留言者姓名'>
							</p>
							<p><span style="color: #ff0000;">* </span>
								<label class="form-check-label" for="board_sex" aria-describedby="inputGroupFileAddon02">性別 :</label>

								<input name="board_sex" type="radio" id="board_sex" value="M" <?php if($commentData["board_sex"]=="M"){echo "checked";}?>>男
								<input name="board_sex" type="radio" id="board_sex" value="F" <?php if($commentData["board_sex"]=="F"){echo "checked";}?>>女
							</p>
							<p><span style="color: #ff0000;">* </span>
								<label class="form-check-label" for="board_mail" aria-describedby="inputGroupFileAddon02">郵件 :</label>
								<input type="text" style="width: 85%" class="form-control" id="board_mail" name="board_mail" value="<?php echo $commentData["board_mail"];?>" placeholder='請輸入留言者信箱'>
							</p>
							<p><span>&nbsp;&nbsp; </span>
								<label class="form-check-label" for="board_web" aria-describedby="inputGroupFileAddon02">網站 :</label> 
								<input type="text" style="width: 85%" class="form-control" id="board_web" name="board_web" value="<?php echo $commentData["board_web"];?>" placeholder='請輸入留言者個人網頁'>
							</p>
							<p>
								<span>&nbsp;&nbsp; </span>
								<label class="form-check-label" for="board_content" aria-describedby="inputGroupFileAddon02">留言內容 :</label> 
								<textarea name="board_content" id="board_content"><?php echo $commentData["board_content"];?></textarea>
							</p>
						</td>
					</tr>	
					<tr valign="top" align="center">
						<td>
							<p align="center" valign="top"  id="window_footer">
								<input name="board_id" type="hidden" id="board_id" value="<?php echo $commentData["board_id"];?>"/>
								<input name="action" type="hidden" id="action" value="update"/>
								<input class="button_margin_3" style="background-image:url(<?=$images_root;?>/buttom_update_1.png);width: 140px; height: 40px;border:none;margin-right: 10px;" type="button" id="updateForm" onclick="checkForm();" value=""/>
								<input class="button_margin_3" style="background-image:url(<?=$images_root;?>/buttom_backward_1.png);width: 140px; height: 40px;border:none;margin-right: 20px;" type="button" id="closeForm" onclick="javascript:window.close();self.opener.location.reload();" value=""/>							
							</p>
						</td>
					</tr>
				</table>
			</form>
			</div>
		</td>
	</tr>
</table>

<script>

	//Ckeditor

	var this_ckeditor;

	function createEditor() {
	    if (ckeditor != null) {
	        if (this_ckeditor) {
	            this_ckeditor.destroy();
	        }
	    }
	}

	CKEDITOR.env.isCompatible = true;
	this_ckeditor = CKEDITOR.replace('board_content', {
		width: '98%',
		height: 220,
		resize_enabled: false,
		enterMode: 2,
		forcePasteAsPlainText: true,
		toolbar: [
			['Source', '-'],
			['Maximize', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
			['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
			['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
			['TextColor', 'BGColor', '-', 'NumberedList', 'BulletedList', ],
			'/', ['Outdent', 'Indent', 'Iineheight'],
			['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
			['Link', 'Unlink', 'Anchor'],
			['Image', 'Flash', 'Table', 'HorizontalRule', 'SpecialChar', 'PageBreak'],
			['Format', 'Font', 'FontSize']
			]
	});
</script>