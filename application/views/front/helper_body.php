			<!--中間區段2-->
			<tr align="left">
				<td align="center" width="100%" height="600">
					<table id="board_main" width="100%"  height="100%" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
						<tr valign="middle">
							<td valign="top" align="center" width="75%" height="100%" style="background-color: #E0E0EB;">
								<table id="maincontent6" width="80%" height="100%" border="0" cellpadding="0" cellspacing="0" align="left"  valign="top">
									<tr  height="100%" width="100%" valign="top">
										<td>
											<div id="mainRegion6">
											<form  id="help_board"  name="formHelp" action="" method="post" onSubmit="return checkForm();">
												<table width="65%" height="490" cellpadding="0" cellspacing="0" border="0" align="center" valign="middle" id="help_area">
													<tr  valign="middle">
														<td id="board_heading" class="heading" colspan="2" align="center" style="padding-left: 20px;">
															<h4><b><i>&#8501; 請問你要什麼協助呢? &#8501;</i></b></h4> 
														</td>
													</tr>
													<tr valign="middle">
														<td class="board_list"  align="left">
															<p class="title4" valign="top" style="padding-left: 20px;">
																※請先依問題類型選擇:
															</p>
															<div align="center">
																<hr width="90%" style="background-color: #2F4F4F;height: 1px;border: none;" />
															</div>
															<p class="title4" align="left" style="padding-left: 20px;">
																【常見介面使用問題】
																<ol>
																	<a href="/helper_questions/index/1/1"><li><u>請問會員登入方式與加入步驟 ?</u></li></a>
																	<a href="/helper_questions/index/1/2"><li><u>如何瀏覽網頁主要功能呢?</u></li></a>
																	<a href="/helper_questions/index/1/3"><li><u>如何操作各網頁下方的功能? 。</u></li></a>
																	<a href="/helper_questions/index/1/4/1"><li><u>留言版使用的方式為...?</u></li></a>
																</ol>	
																<div align="center">
																	<hr width="90%" style="background-color: #2F4F4F;height: 1px;border: none;" />
																</div>
															</p>	
															<p class="title4" align="left" style="padding-left: 20px;">
																【常見系統操作問題】
																<ol>
																	<a href="/helper_questions/index/2/1"><li><u>已連線伺服器，但可能出現的異常狀況。</u></li></a>
																	<a href="/helper_questions/index/2/2"><li><u>Google地圖常出現錯誤說明 。</u></li></a>
																	<a href="/helper_questions/index/2/3"><li><u>無法連線至伺服器的情況。</u></li></a>
																	<a href="/helper_questions/index/2/4"><li><u>瀏覽器意外當機停擺的狀況。</u></li></a>
																</ol>
																<div align="center">
																	<hr width="90%" style="background-color: #2F4F4F;height: 1px;border: none;" />
																</div>
															</p>
														</td><!--內文顯示-->
													</tr><!--表單內文填寫區塊-->
						
													<tr valign="top">	
														<td align="center"  id="board_footer" colspan="2">
															<input id="action" name="action" type="hidden" value="solve"/>
															<?php if(isset($isAddComment)){?>
																<input id="isAddComment" name="isAddComment" type="hidden" value="<?=$isAddComment;?>"/>
															<?php }?>
															<a href="/home/index/1/sucess"><img src="<?=$images_root;?>/sticker_button_solveOK.png"/></a>
															<a href="javascript:window.history.back()"><img src="<?=$images_root;?>/sticker_button_back.png"></a>	
														</td>
													</tr><!--表單內文填寫區塊-->
											</table>
											</form>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<!--右下方圖案-->
							<td valign="bottom" align="right" width="10%" style="background-color: #E0E0EB;">
								<div id="corner_img_div" style="display: none;">
									<img id="corner_img" src="<?=$images_root;?>/talking.png" style="margin-right: 30px;margin-bottom: 15px;">
								</div>
							</td>
							<!--右下方圖案-->
							<!--Menu跳出小視窗1-->
							<div id="pop_menu_login" class="pop_menu" style="display: none;">
								<!-- 中間區塊2-2的跳出小視窗1  -->
								<form name="formReg" id="formReg" method="post" >
									<p class="heading" align="center">&#9674; &#9830; 登出會員系統 &#9830; &#9674;</p>
									<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
									<p align="center" class="smalltext7">&#8855;  如有修改會員、管理員密碼 &#8855;<br/>&#8855; 請重新登入!謝謝! &#8855; </p>
									<p align="center" style="opacity: 50%">
										<a href="/home/logout" >
											<img class="button_margin_1" name="index_logout" id="index_logout" src="<?=$images_root;?>/buttom_logout_1.png">
										</a>
									</p>
								<form/>
							</div>
							<!--Menu跳出小視窗1-->
              <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->
              <?php if(!isset($loginAutobiography) || $loginAutobiography != "sucess"){?>
              <div class="pop_menu" id="autobiography-pw-dialog" style="display: none;">
                <p class="context_regbox">
                  <label class="form-check-label" for="account-input" aria-describedby="inputGroupFileAddon02">自傳閱覽帳號: </label>
                  <input class="form-control account-input" id="account-input" type="text">
                </p>
                <p class="context_regbox">
                  <label class="form-check-label" for="password-input" aria-describedby="inputGroupFileAddon02">自傳閱覽密碼: </label>
                  <input class="form-control password-input" id="password-input" type="password">
                </p>
                <p class="align-center" style="opacity: 50%;">
                  <input  class="button_margin_1 password-submit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
                </p>
              </div>
              <?php } ?>
              <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->
							<!--中間區塊2-2-->
							<td id="regbox" align="center" valign="top" width="15%">
								<table id="pop_menu_content" width="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="top">
									<?=$menuContext;?>
								</table>
								<div id="menuLastArea" style="background-image:url(<?=$images_root;?>/pop_menu_buttom_bg.png);"></div>
							</td>
							<!--中間區塊2-2-->
						</tr>
				</table>	
				</td>
			</tr>
			<!--中間區塊2-->