			<style type="text/css">
			<!--
				#exp_content{
					cursor: help;
				}
				
				#light_button:hover{
					cursor: pointer;
				}
				
				#point1_1_1:hover,#point1_1_2:hover,#point1_1_3:hover{
					cursor: crosshair;
				}
				
				.point{
					width:10%;
					font-family: "微軟正黑體";
					font-size: 14pt;
					color: #FF3300;
					text-align: center;
					font-style:italic;
					font-weight: bolder;
				}
				
				.point_content{
					font-family: "微軟正黑體";
					font-size: 11pt;
					color: #242376;
					font-weight: normal;
				}
				
				#point1_1_1{
					margin-top: 30px;
					margin-left: 320px;
				}
				
				#point1_1_2{
					margin-top: 45px;
					margin-left: 320px;
				}
				
				#point1_1_3{
					margin-top: 40px;
					margin-left: 510px;
				}
				
				#point1_1_1_content{
					margin-top: 5px;
					margin-left: 360px;
				}
				
				#point1_1_2_content{
					margin-top: 5px;
					margin-left: 360px;
				}
				
				#point1_1_3_content{
					margin-top: 5px;
					margin-left: 545px;
				}
				
				.photo{
					cursor:auto;
					padding-top:45px;
					background-attachment:scroll;
					background-position:center,center;
					background-repeat:no-repeat;
				}
				
				.player_button1 {
						margin-top: 8px;
						margin-bottom: 8px;
						margin-left: 5px;
						margin-right: 5px;
				}
					
				.player_skin1 {
					margin-top: 4px;
					margin-left: 115px;
					margin-bottom: 8px;
				}
			-->
			</style>

			<!--中間區段2-->
			<tr align="left">
				<td align="center" width="100%" height="700">
					<table id="board_main" width="100%"  height="100%" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
						<tr valign="middle">
							<td valign="top" align="center" width="75%" height="100%" style="background-color: #ffffff;">
								<table id="maincontent4" width="1000" height="100%" border="0" cellpadding="0" cellspacing="0" align="left"  valign="top" >
									<tr   height="100%" width="100%">
										<td>
											<div id="mainRegion4">
												<form name="formAns" method="post" action="">
												<table width="93%" height="100%" border="0" align="center" cellpadding="0" cellspacing="1">
													<tr valign="top" align="center">
														<td colspan="2" class="heading">【會員登入方式說明】</td>
													</tr>
													<tr valign="top">
														<td background="<?=$images_root;?>/ques1-1.png" width="900" height="450" class="photo">
															<p class="point" id="point1_1_1">步驟二:</p>
																<em class="point_content" id="point1_1_1_content">如果已註冊會員的捧友們，請依申請到的帳號跟密碼輸入，再進行下一步驟喔!</em>
															<p class="point" id="point1_1_2">步驟三:</p>
																<em class="point_content" id="point1_1_2_content">最後恭喜你可以進入會員專區瞜!開始使用會員吧~</em>
															<p class="point" id="point1_1_3"><b>步驟一:</b></p>
																<em class="point_content" id="point1_1_3_content">會員開始的起點喔!必須先註冊才能使用功能。</em>
														</td>
													</tr>
													<tr valign="middle" align="right">
														<td >
															<p align="left">
																<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;margin-top: 60px;" />
																<table width="100%" height="100%">
																	<tr align="center">
																		<td align="left" valign="middle">
																			<p align="left" valign="middle">
																				<strong>※歡迎使用會用功能，當你第一次想試試看使用會員功能時，請先嘗試去<a id="exp_content" href="#point1_1_3">加入會員</a>吧，</strong><br/>
																				&nbsp;&nbsp;<strong>當你成功加入會員後，你自然就可以自由使用<a id="exp_content" href="#point1_1_1">帳號與密碼</a>登入等功能啦!</strong>
																			</p>
																			<p align="left" valign="middle">
																				<strong>※如果你忘了密碼也別擔心喔!再動動手指頭按下<a id="exp_content" href="#point1_1_2">忘記密碼</a>連結就可以取回了。</strong>
																			</p>	
																		</td>
																		<td align="right" valign="middle">
																			<input type="image" id="light_button1" src="<?=$images_root;?>/Light-1.png" value="solve" onClick="return checkForm(this.id);"/>
																			<input type="image" id="light_button2" src="<?=$images_root;?>/Light-2.png" value="issue" onClick="return checkForm(this.id);"/>
																			<input type="image" id="reply_button1" src="<?=$images_root;?>/ques_reply.png" value="reply" onClick="return checkForm(this.id);"/>
																		</td>
																	</tr>
																</table>
															</p>	
														</td>
													</tr>
												</table>
											</form>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<!--右下方圖案-->
							<td valign="bottom" align="right" width="10%" style="background-color: #ffffff;">
								<div id="corner_img_div" style="display: none;">
									<img id="corner_img" src="<?=$images_root;?>/talking.png" style="margin-right: 30px;margin-bottom: 15px;">
								</div>
							</td>
							<!--右下方圖案-->
							<!--Menu跳出小視窗1-->
							<div id="pop_menu_login" class="pop_menu" style="display: none;">
								<!-- 中間區塊2-2的跳出小視窗1  -->
								<form name="formReg" id="formReg" method="post" >
									<p class="heading" align="center">&#9674; &#9830; 登出會員系統 &#9830; &#9674;</p>
									<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
									<p align="center" class="smalltext7">&#8855;  如有修改會員、管理員密碼 &#8855;<br/>&#8855; 請重新登入!謝謝! &#8855; </p>
									<p align="center" style="opacity: 50%">
										<a href="/home/logout" >
											<img class="button_margin_1" name="index_logout" id="index_logout" src="<?=$images_root;?>/buttom_logout_1.png">
										</a>
									</p>
								<form/>
							</div>
							<!--Menu跳出小視窗1-->
              <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->
              <?php if(!isset($loginAutobiography) || $loginAutobiography != "sucess"){?>
              <div class="pop_menu" id="autobiography-pw-dialog" style="display: none;">
                <p class="context_regbox">
                  <label class="form-check-label" for="account-input" aria-describedby="inputGroupFileAddon02">自傳閱覽帳號: </label>
                  <input class="form-control account-input" id="account-input" type="text">
                </p>
                <p class="context_regbox">
                  <label class="form-check-label" for="password-input" aria-describedby="inputGroupFileAddon02">自傳閱覽密碼: </label>
                  <input class="form-control password-input" id="password-input" type="password">
                </p>
                <p class="align-center" style="opacity: 50%;">
                  <input  class="button_margin_1 password-submit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
                </p>
              </div>
              <?php } ?>
              <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->              
							<!--中間區塊2-2-->
							<td id="regbox" align="center" valign="top" width="15%">
								<table id="pop_menu_content" width="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="top">
									<?=$menuContext;?>
								</table>
								<div id="menuLastArea" style="background-image:url(<?=$images_root;?>/pop_menu_buttom_bg.png);"></div>
							</td>
							<!--中間區塊2-2-->
						</tr>
				</table>	
				</td>
			</tr>
			<!--中間區塊2-->