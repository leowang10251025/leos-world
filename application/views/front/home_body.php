		<!--中間區段2-->
		<!--中間區塊2-1-->
		<tr align="left">
		  <td align="center" width="100%" height="600">
		    <table class="home_main" width="100%" height="100%" align="center" valign="middle" border="0" cellspacing="0" cellpadding="0">
		      <tr>
		        <td id="mainbox" valign="top" align="center" width="75%">
		          <table id="maincontent1" width="90%" height="544" border="0" align="center">
		            <tr height="100%" width="100%" border="0" valign="top">
		              <td valign="top" align="left">
		                <p valign="middle" align="left">
		                  <marquee id="title_marquee1" class="marquee" behavior="alternate" width="90%" scrolldelay="150" direction="right">
		                    <?= $title; ?>
		                  </marquee>
		                </p>
		                <p class="heading0">
		                  <?= $sub_title; ?>
		                </p>
		                <?= $content; ?>
                    <?= $main_content_notcie;?>
		              </td>
		              <!--左側主文內容-->
		            </tr>
		          </table>
		        </td>
		        <!--右下方圖案-->
		        <td valign="bottom" align="right" width="10%" style="background-color: #ffffff;">
		          <div id="corner_img_div" style="display: none;">
		            <img id="corner_img" src="<?= $images_root; ?>/talking.png" style="margin-right: 30px;margin-bottom: 15px;">
		          </div>
		        </td>
		        <!--右下方圖案-->
		        <!--中間區塊2-1-->
		        <!--Menu跳出小視窗-->
		        <div id="pop_menu_login" class="pop_menu" style="display: none;">
		          <!-- 中間區塊2-2的跳出小視窗  -->
		          <form name="formReg" id="formReg" method="post" action="/home/login" onSubmit="return checkSubForm();">
		            <?php if (empty($memberLevel)) { ?>
		              <p class="heading" align="center" style="padding-right: 5px;">
		                &#9830; &#9674; 登入會員系統 &#9674; &#9830;
		              </p>
		              <hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
		              <p class="context_regbox">
		                <label class="form-check-label" for="loginName" aria-describedby="inputGroupFileAddon02">帳號: </label>
		                <input name="loginName" id="loginName" type="text" class="form-control" value="<?php if (isset($cookie_name)) {
                                                                                                      echo $cookie_name;
                                                                                                    } else {
                                                                                                      echo '';
                                                                                                    } ?>">
		              </p>
		              <p class="context_regbox">
		                <label class="form-check-label" for="loginPW" aria-describedby="inputGroupFileAddon02">密碼: </label>
		                <input name="loginPW" id="loginPW" type="password" class="form-control" value="<?php if (isset($cookie_pw)) {
                                                                                                      echo $cookie_pw;
                                                                                                    } else {
                                                                                                      echo '';
                                                                                                    } ?>">
		              </p>
		              <p class="context_regbox">
		                <input name="loginRem" id="loginRem" type="checkbox">
		                <input name="loginRemChk" id="loginRemChk" type="hidden" class="form-check-input" value="<?php if (isset($loginRemChk)) {
                                                                                                                echo $loginRemChk;
                                                                                                              } else {
                                                                                                                echo 'false';
                                                                                                              } ?>">
		                <label class="form-check-label" for="loginRemChk" aria-describedby="inputGroupFileAddon02"> 記住我的帳號與密碼 </label>
		              </p>
		              <p align="center" style="opacity: 50%;">
		                <input class="button_margin_1" name="loginSubmit" id="loginSubmit" src="<?= $images_root; ?>/buttom_login_1.png" type="image">
		              </p>
		              <?php if (isset($errMsg)) {
                    echo '<input type="hidden" name="errMsg" id="errMsg" value="' . $errMsg . '">';
                  } ?>
		            <?php } else { ?>
		              <form name="formReg" id="formReg" method="post">
		                <p class="heading" align="center">&#9674; &#9830; 登出會員系統 &#9830; &#9674;</p>
		                <hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
		                <p align="center" class="smalltext7">&#8855; 如有修改會員、管理員密碼 &#8855;<br />&#8855; 請重新登入!謝謝! &#8855; </p>
		                <p align="center" style="opacity: 50%">
		                  <a href="/home/logout">
		                    <img class="button_margin_1" name="index_logout" id="index_logout" src="<?= $images_root; ?>/buttom_logout_1.png">
		                  </a>
		                </p>
		                <form />
		              <?php } ?>
		              </form>
		        </div>
		        <!--Menu跳出小視窗-->
            <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->
            <?php if(!isset($loginAutobiography) || $loginAutobiography != "sucess"){?>
            <div class="pop_menu" id="autobiography-pw-dialog" style="display: none;">
              <p class="context_regbox">
                <label class="form-check-label" for="account-input" aria-describedby="inputGroupFileAddon02">自傳閱覽帳號: </label>
                <input class="form-control account-input" id="account-input" type="text">
              </p>
              <p class="context_regbox">
                <label class="form-check-label" for="password-input" aria-describedby="inputGroupFileAddon02">自傳閱覽密碼: </label>
                <input class="form-control password-input" id="password-input" type="password">
              </p>
              <p class="align-center" style="opacity: 50%;">
                <input  class="button_margin_1 password-submit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
              </p>
            </div>
            <?php } ?>
            <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->
		        <!--中間區塊2-2-->
		        <td id="regbox" align="center" valign="top" width="15%">
		          <table id="pop_menu_content" width="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="top">
		            <?= $menuContext; ?>
		          </table>
		          <div id="menuLastArea" style="background-image:url(<?= $images_root; ?>/pop_menu_buttom_bg.png);"></div>
		        </td>
		        <!--中間區塊2-2-->
		      </tr>
		    </table>
		  </td>
		</tr>
		<!--中間區塊2-->