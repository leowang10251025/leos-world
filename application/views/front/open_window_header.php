<?php
	header("Content-Type: text/html; charset=utf-8");
?>

<!DOCTYPE>
<html lang="zh-tw">
	<head>
		<meta http-equiv="Content" Content-Type="text/html; charset=utf-8">
		<link rel="stylesheet" href="<?=$css_root;?>/front/style_1.css" type="text/css">
		<link rel="stylesheet" href="<?=$js_root;?>/lib/bootstrap-sweetalert-master/dist/sweetalert.css" >
		<title><?=$windowTitle;?></title>
		<script src="<?=$js_root;?>/lib/jquery-3.3.1.min.js"></script> 
		<script src="<?=$js_root;?>/lib/bootstrap-sweetalert-master/dist/sweetalert.min.js"></script>
		<script src="<?=$js_root;?>/front/timer.js"></script>
		<script src="<?=$js_root;?>/front/topLeftNavi.js"></script>
		<script src="<?=$js_root;?>/common/basic_tools.js"></script>
		<!-- ckeditor -->
		<script type="text/javascript" src="<?=$js_root;?>/lib/ckeditor/ckeditor.js"></script>
		<!-- bootstrap -->
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

		<?php if(isset($pageName)){ ?>
		<?php 	if($pageName == "adminfix"){ ?>
				<script src="<?=$js_root;?>/front/adminfix.js"></script>
		<?php 	}else if($pageName == "admindel"){ ?>
				<script src="<?=$js_root;?>/front/admindel.js"></script>
		<?php 	}else if($pageName == "contact_mail"){ ?>
				<script src="<?=$js_root;?>/front/contact_mail.js"></script>
		<?php 	}else if($pageName == "message_mail"){ ?>
				<script src="<?=$js_root;?>/front/message_mail.js"></script>
		<?php 	}else if($pageName == "error_mail"){ ?>
				<script src="<?=$js_root;?>/front/error_mail.js"></script>
		<?php 	}?>			
		<?php }?>
	</head>
	<body bgcolor="#d8bfd8">
		<style type="text/css">
			<!--
				#updateForm, #closeForm, #deleteForm, #sendForm{
					background: transparent;
    				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE8 */ 
    				filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE6 & 7 */
				}
				#layer1{
					z-index:1;
				}
				#layer2{
					z-index:2;
				}
				.button_margin_1 {
					margin-top: 16px;
					margin-bottom: 8px;
				}

				.button_margin_2 {
					margin-top: 8px;
					margin-bottom: 16px;
				}

				#message_mail_list{
					background-size: 100% 100%;
					background-repeat: no-repeat;
				}
				
				.context_regbox{
						font-family: Georgia, "Times New Roman", Times, serif;
						font-size: 10pt;
						color: #0000cd;
						line-height: 120%;
						margin-bottom:8px;
						margin-top:8px;
					}
					
				.context_regbox_2{
					font-family: "微軟正黑體";
					font-size: 10pt;
					color: #FF0000;
					line-height: 120%;
					margin-top: 8px;
					margin-bottom: 8px;
				}
				
				.smalltext7{
					font-size: 13px;
					color: #FF0000;
					font-family: Georgia, "Times New Roman", Times, serif;
					vertical-align: middle;
				}
				
				.heading_1{
					margin-top: 25px;
					padding-left: 25px;
					font-family: "微軟正黑體";
					font-size: 13pt;
					color: #ff3300;
					line-height: 110%;
					font-weight: bold;
				}
				
				.player_button1 {
					margin-top: 8px;
					margin-bottom: 8px;
					margin-left: 5px;
					margin-right: 5px;
				}
				
				.player_skin1 {
					margin-top: 4px;
					margin-left: 115px;
					margin-bottom: 8px;
				}

				.msg_content_bg {
					background: transparent;
    				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE8 */ 
    				filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE6 & 7 */
    				background-image: url(<?=$images_root;?>/msg_content_bg.png);
    				background-color: #e6e6e6;background-repeat: no-repeat;
					background-size:100% 90%;
				}

				.msg_content_bg_2 {
					background: transparent;
    				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE8 */ 
    				filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE6 & 7 */
    				background-image: url(<?=$images_root;?>/msg_content_bg.png);
    				background-color: #e6e6e6;background-repeat: no-repeat;
					background-size:100% 97%;
				}

				.msg_content_bg_3 {
					background: transparent;
    				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE8 */ 
    				filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE6 & 7 */
    				background-image: url(<?=$images_root;?>/msg_content_bg.png);
    				background-color: #e6e6e6;background-repeat: no-repeat;
					background-size:100% 91%;
				}
			-->
		</style>	
