<div class="row my-4">
  <div class="col-2 pr-4">
    <image src="<?=$images_root;?>/myself.png" alt="This is My First Photo!" class="img-thumbnail mw-50" ></image>
    <div class="row my-2">
      <div class="col">
        <input class="form-control" id="real-name" name="real-name" type="text" placeholder="請填入真實姓名..." value="<?php echo isset($basicData["real-name"])? $basicData["real-name"]: "";?>"/>
      </div>
    </div>
  </div>
  <div class="col-8 pl-4 shadow p-3 mb-5 bg-white rounded border-left">
    <div class="row my-3 text-primary align-items-center">
      <div class="col-12">
        <strong>最後學經歷:</strong>
      </div>
    </div>
    <div class="row my-3">
      <div class="col-6">
        <label for="last-company-name" class="text-secondary">
          <strong>最後一份工作的公司名 : </strong>
        </label>
        <span id="last-company-name" class="text-secondary font-italic">
          <?php echo isset($lastCompanyName)? $lastCompanyName: "";?>
        </span>
      </div>
      <div class="col-6 border-left">
        <label for="last-job-name" class="text-secondary">
          <strong>最後一份工作的職稱名 : </strong>
        </label>
        <span id="last-job-name" class="text-secondary font-italic">
          <?php echo isset($lastJobName)? $lastJobName: "";?>
        </span>
      </div>
    </div>
    <div class="row my-3">
      <div class="col-6">
        <label for="highest-education-name" class="text-secondary">
          <strong>最高學歷學校名 : </strong>
        </label>
        <span id="highest-education-name" class="text-secondary font-italic">
          <?php echo isset($highestEducationName)? $highestEducationName: "";?>
        </span>
      </div>
      <div class="col-6 border-left">
        <label for="dept-highest-education" class="text-secondary">
          <strong>最高學歷科系名 : </strong>
        </label>
        <span id="dept-highest-education" class="text-secondary font-italic">
          <?php echo isset($deptHighestEducation)? $deptHighestEducation: "";?>
        </span>
      </div>
    </div>
    <div class="row my-3">
      <div class="col-6">
        <label class="text-primary" for="living-city-position">
          <strong>居住城市與行政區:</strong>
        </label>
        <select class="custom-select" id="living-city-position" name="living-city-position">
          <option value="0">請選擇居住的城市</option>
          <?php foreach($cityInfo as $key=>$info){
            if($info['id'] == $cityId){
              echo '<option value="'.$info['id'].'" selected>'.$info['name'].'</option>';
            }else{
              echo '<option value="'.$info['id'].'">'.$info['name'].'</option>';
            }
          }?>
        </select>
      </div>
      <div class="col-6 border-left">
        <div class="row">
          <div class="col-4">
            <span id="working-experince-term" name="working-experince-term" class="text-secondary">
              <strong>0 年 0 個月</strong>
            </span>
          </div>
          <div class="col-8 text-secondary text-left align-items-center">
            <strong>工作經驗</strong>
          </div>
        </div>
      </div>
    </div>
    <div class="row my-3">
      <div class="col-6 border-right">
        <select class="custom-select" id="living-district-position" name="living-district-position">
          <option value="0">請選擇居住的行政區</option>
          <?php if (!empty($districtArr)) {
              foreach($districtArr as $arr){
                if($arr['id'] == $districtId){
                  echo '<option value="'.$arr['id'].'" selected>'.$arr['name'].'</option>';
                }else{
                  echo '<option value="'.$arr['id'].'">'.$arr['name'].'</option>';
                }
              }
            }
          ?>
        </select>
      </div>
    </div>
    <div class="row my-3 text-primary align-items-center">
      <div class="col-12">
        <strong>希望職稱：</strong>
      </div>
    </div>
    <div class="row my-3 text-secondary align-items-center">
      <div class="col-3">
        <input class="form-control" id="hope-job-1" name="hope-job-1" type="text" placeholder="請填入職務1" value="<?php echo isset($basicData["hope-job-1"])? $basicData["hope-job-1"]: "";?>"/>
      </div>
      <div class="w-10">
        /
      </div>
      <div class="col-3">
        <input class="form-control" id="hope-job-2" name="hope-job-2" type="text" placeholder="請填入職務2" value="<?php echo isset($basicData["hope-job-2"])? $basicData["hope-job-2"]: "";?>"/>
      </div>
      <div class="w-10">
        /
      </div>
      <div class="col-3">
        <input class="form-control" id="hope-job-3" name="hope-job-3" type="text" placeholder="請填入職務3" value="<?php echo isset($basicData["hope-job-3"])? $basicData["hope-job-3"]: "";?>"/>
      </div>
    </div>
  </div>
  <div class="col-2 pl-5">
  <?php if(!empty($basicData["id"])){?>
    <button type="button" class="btn btn-outline-primary btn-add-data shadow p-3 mb-5 rounded" style="width:130px;">
      <i class="fa fa-plus" aria-hidden="true"></i> 新增<br/>一筆履歷
    </button>
  <?php }?>  
  </div>
</div>
<div class="row">
  <div class="col-10 shadow p-3 mb-5 bg-white rounded">
    <div class="card">
      <div class="card-header" id="headingOne">
        <div class="row">
          <div class="col">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#basic-info">
              <h4><b>詳細個人基本資料</b></h4>  
            </button>
          </div>
        </div>
      </div>

      <div id="basic-info" class="collapse">
        <div class="card-body">
          <div class="row mt-4 mb-4 text-left align-items-center">
            <div class="col-2">
              <label for="personal-data" class="text-primary">
                <strong>個人資料:</strong>
              </label>
            </div>
            <div class="col-10 py-4 border-bottom">
              <div class="row text-secondary">
                <div class="col-1">
                  <span>
                    <strong>性別: </strong>
                  </span>
                </div>
                <div class="col-1">
                  <input class="form-check-input" name="gender" type="radio" value="1" <?php echo (isset($basicData["gender"]) && $basicData["gender"] == 1)? "checked": "";?>/> 男 </input>
                </div>
                <div class="col-1">  
                  <input class="form-check-input" name="gender" type="radio" value="2" <?php echo (isset($basicData["gender"]) && $basicData["gender"] == 2)? "checked": "";?>/> 女 </input>
                </div>
                <div class="col-2">  
                  <input class="form-check-input" name="gender" type="radio" value="3" <?php echo (isset($basicData["gender"]) && $basicData["gender"] == 3)? "checked": "";?>/> 第三性 </input>
                </div>
              </div>
            </div>
          </div>
          <div class="row text-left align-items-center">  
            <div class="col-2">
            </div>
            <div class="col-10 py-4 border-bottom">
              <div class="row text-secondary">
                <div class="col-1">
                  <span>
                    <strong>年齡: </strong>
                  </span>
                </div>
                <div class="col-2 border-right">
                  <input class="form-control text-secondary" id="age" name="age" type="number" step="1" max="200" value="<?php echo isset($basicData["age"])? $basicData["age"]: "";?>"/>
                </div>
                <div class="col-3 border-right">
                  <select class="custom-select" id="birth-year-selector" name="birth-year-selector">
                    <option value="0">請選擇出生年</option>
                    <?php
                      foreach($birthYear as $key => $yearData){?>
                        <?php if($birthYearVal == $key) {?>
                            <option value="<?=$key?>" selected><?=$yearData?></option>
                        <?php }else{ ?>
                            <option value="<?=$key?>"><?=$yearData?></option>
                        <?php  } 
                      } 
                    ?>
                  </select>
                </div>
                <div class="col-3 border-right">
                  <select class="custom-select" id="birth-month-selector" name="birth-month-selector">
                    <option value="0">請選擇出生月</option>
                    <?php
                      foreach($birthMonth as $key => $monthData){?>
                        <?php if($birthMonthVal == $key) {?>
                            <option value="<?=$key?>" selected><?=$monthData?></option>
                        <?php }else{ ?>
                            <option value="<?=$key?>"><?=$monthData?></option>
                        <?php  } 
                      } 
                    ?>
                  </select>
                </div>
                <div class="col-3">
                  <select class="custom-select" id="birth-day-selector" name="birth-day-selector">
                    <option value="0">請選擇出生日</option>
                  </select>
                </div>   
              </div>
            </div>
          </div>
          <div class="row text-left align-items-center">  
            <div class="col-2">
            </div>
            <div class="col-10 py-4 border-bottom">
              <div class="row text-secondary">
                <div class="col-2">
                  <span>
                    <strong>兵役狀態: </strong>
                  </span>
                </div>
                <div class="col-1">
                  <input class="form-check-input" name="military-service-status" type="radio" value="1" <?php echo (isset($basicData["military-service-status"]) && $basicData["military-service-status"] == 1)? "checked": "";?>/> 待役 </input>
                </div>
                <div class="col-1">  
                  <input class="form-check-input" name="military-service-status" type="radio" value="2" <?php echo (isset($basicData["military-service-status"]) && $basicData["military-service-status"] == 2)? "checked": "";?>/> 役畢 </input>
                </div>
                <div class="col-2" id="military-service">
                  <select class="custom-select" name="military-service" id="military-service">
                    <option value="0">請選擇軍種</option>
                    <?php
                      foreach($militaryService as $key => $name){
                        if($basicData['military-service'] == $key) {?>
                          <option value="<?=$key?>" selected><?=$name?></option>
                      <?php }else{ ?>
                          <option value="<?=$key?>"><?=$name?></option>
                      <?php  } 
                      }
                    ?>
                  </select>
                </div>
                <div class="col-1">  
                  <input class="form-check-input" name="military-service-status" type="radio" value="3" <?php echo (isset($basicData["military-service-status"]) && $basicData["military-service-status"] == 3)? "checked": "";?>/> 免役</input>
                </div>
                <div class="col-1">  
                  <span id="exempt-service-reason-1" class="text-center text-secondary">原因為:</span>
                </div> 
                <div class="col">  
                  <input class="form-control" id="exempt-service-reason-2" name="exempt-service-reason-2" type="text" value="<?php echo isset($basicData["exempt-service-reason"])? $basicData["exempt-service-reason"]: "";?>"/>
                </div>                
              </div>
            </div>
          </div>  
          <div class="row mt-4 mb-4 text-left text-secondary align-items-center">
            <div class="col-2">
              <label class="text-primary form-check-label">
                <strong>工作狀態:</strong>
              </label>
            </div>
            <div class="col-2 ml-4"> 
              <input class="form-check-input" name="job-status" type="radio" value="1" <?php echo (isset($basicData["job-status"]) && $basicData["job-status"] == 1)? "checked": "";?>/> 在職中 </input>
            </div>
            <div class="col-2">  
              <input class="form-check-input" name="job-status" type="radio" value="2" <?php echo (isset($basicData["job-status"]) && $basicData["job-status"] == 2)? "checked": "";?>/> 待業中 </input>
            </div>
          </div>
          <div class="row mt-4 mb-4 text-left align-items-center">
            <div class="col-2">
              <label for="mobile-phone" class="text-primary">
                <strong>聯絡手機:</strong>
              </label>
            </div>
            <div class="col-sm-3">
              <input class="form-control" id="mobile-phone" name="mobile-phone" type="text" oninput="if(value.length > 10)value=value.slice(0,10);" value="<?php echo isset($basicData["mobile-phone"])? $basicData["mobile-phone"]: "";?>"/>
            </div>
          </div>
          <div class="row mt-4 mb-4 text-left align-items-center">  
            <div class="col-2">
              <label for="telphone" class="text-primary">
                <strong>聯絡電話:</strong>
              </label>
            </div>
            <div class="col-sm-3">
              <input class="form-control" id="telphone" name="telphone" type="text" oninput="if(value.length > 11)value=value.slice(0,11);" value="<?php echo isset($basicData["telphone"])? $basicData["telphone"]: "";?>"/>
            </div>
          </div> 
          <div class="row mt-4 mb-4 text-left align-items-center">  
            <div class="col-2">
              <label for="name" class="text-primary">
                <strong>聯絡地址:</strong>
              </label>
            </div>
            <div class="col-2 border-right">
              <input type="text" id="address-1" name="address-1" class="form-control text-center text-secondary" placeholder="請填入縣市名" value="<?php echo isset($cityPosition)? $cityPosition: "";?>">
            </div>
            <div class="col-2 border-right">
              <input type="text" id="address-2" name="address-2" class="form-control text-center text-secondary" placeholder="請填入鄉鎮市名" value="<?php echo isset($districtPosition)? $districtPosition: "";?>"/>
            </div>
            <div class="col-6">
              <input class="form-control" id="address-3" name="address-3" type="address" value="<?php echo isset($addressSuf)? $addressSuf: "";?>"/>
            </div>
          </div>
          <div class="row mt-4 mb-4 text-left align-items-center">  
            <div class="col-2">
              <label for="eng-name" class="text-primary">
                <strong>英文姓名:</strong>
              </label>
            </div>
            <div class="col-9">
              <input class="form-control" id="eng-name" name="eng-name" type="text" value="<?php echo isset($basicData["eng-name"])? $basicData["eng-name"]: "";?>"/>
            </div>
          </div>
          <div class="row mt-4 mb-4 text-left text-secondary align-items-center">
            <div class="col-2">
              <label class="text-primary">
                <strong>聯絡方式:</strong>
              </label>
            </div>
            <div class="col-2 ml-4"> 
              <input class="form-check-input" id="contact-method-1" name="contact-method-1" type="checkbox" value="1" <?php echo (isset($contactMethod1) && $contactMethod1 == 1)? "checked": "";?>/> 電話聯絡 </input>
            </div>
            <div class="col-2">  
              <input class="form-check-input" id="contact-method-2" name="contact-method-2" type="checkbox" value="2" <?php echo (isset($contactMethod2) && $contactMethod2 == 2)? "checked": "";?>/> E-mail聯絡 </input>
            </div>
            <div class="col-2">  
              <input class="form-check-input" id="contact-method-3" name="contact-method-3" type="checkbox" value="3" <?php echo (isset($contactMethod3) && $contactMethod3 == 3)? "checked": "";?>/> 其他方式聯絡 </input>
            </div>
            <div class="col-2">  
              <input class="form-control" name="other-contact-method" id="other-contact-method" type="text" placeholder="請輸入其他方式" value="<?php echo isset($basicData['other-contact-method'])? $basicData['other-contact-method']: "";?>"/>
            </div>
          </div>
          <div class="row mt-4 mb-4 text-left align-items-center">
            <div class="col-2">
              <label class="text-primary">
                <strong>聯絡時段:</strong>
              </label>
            </div>
            <div class="col-3">
              <input class="form-control content-moment" id="contact-moment-start" name="contact-moment-start" type="time" value="<?php echo isset($contactMomentStart)? $contactMomentStart: "";?>"/>
            </div>
            <div class="col-1 text-secondary">
              至 
            </div>
            <div class="col-3">
              <input class="form-control content-moment" id="contact-moment-end" name="contact-moment-end" type="time" value="<?php echo isset($contactMomentEnd)? $contactMomentEnd: "";?>"/>
            </div>
          </div>
          <div class="row mt-4 mb-4 text-left align-items-center">
            <div class="col-2">
              <label class="text-primary">
                <strong>駕駛執照:</strong>
              </label>
            </div>
            <div class="col-2 ml-4 text-secondary"> 
              <input class="form-check-input" id="driving-licence-1" name="driving-licence-1" type="checkbox" value="1" <?php echo (isset($drivingLicence1) && $drivingLicence1 == 1)? "checked": "";?>/> 普通重型機車駕照 </input>
            </div>
            <div class="col-2 text-secondary">  
              <input class="form-check-input" id="driving-licence-2" name="driving-licence-2" type="checkbox" value="2" <?php echo (isset($drivingLicence2) && $drivingLicence2 == 2)? "checked": "";?>/> 普通小型車駕照 </input>
            </div>
            <div class="col-2 text-secondary">  
              <input class="form-check-input" id="driving-licence-3" name="driving-licence-3" type="checkbox" value="3" <?php echo (isset($drivingLicence3) && $drivingLicence3 == 3)? "checked": "";?>/> 其他駕駛執照 </input>
            </div>
            <div class="col">  
              <input class="form-control" name="other-driving-licence" id="other-driving-licence" type="text" placeholder="請輸入其他類型駕照" value="<?php echo isset($basicData['other-driving-licence'])? $basicData['other-driving-licence']: "";?>"/>
            </div>
          </div>
          <div class="row mt-4 mb-4 text-left align-items-center">
            <div class="col-2">
              <label class="text-primary">
                <strong>交通工具:</strong>
              </label>
            </div>
            <div class="col-2 ml-4 text-secondary"> 
              <input class="form-check-input" id="driving-tool-1" name="driving-tool-1" type="checkbox" value="1" <?php echo (isset($drivingTool1) && $drivingTool1 == 1)? "checked": "";?>/> 普通重型機車 </input>
            </div>
            <div class="col-2 text-secondary">  
              <input class="form-check-input" id="driving-tool-2" name="driving-tool-2" type="checkbox" value="2" <?php echo (isset($drivingTool2) && $drivingTool2 == 2)? "checked": "";?>/> 普通小型車(汽車) </input>
            </div>
            <div class="col-2 text-secondary">  
              <input class="form-check-input" id="driving-tool-3" name="driving-tool-3" type="checkbox" value="3" <?php echo (isset($drivingTool3) && $drivingTool3 == 3)? "checked": "";?>/> 其他交通工具 </input>
            </div>
            <div class="col">  
              <input class="form-control" name="other-driving-tool" id="other-driving-tool" type="text" placeholder="請輸入其他交通工具" value="<?php echo isset($basicData['other-driving-tool'])? $basicData['other-driving-tool']: "";?>"/>
            </div>
          </div>
          <div class="row mt-4 mb-4 text-left align-items-center">  
            <div class="col-2">
              <label for="height" class="text-primary">
                <strong>個人身高:</strong>
              </label>
            </div>
            <div class="col-sm-3">
              <input class="form-control" id="height" name="height" type="number" oninput="if(value.length > 5)value=value.slice(0,5);" min="0.01" max="300.00" step="0.01" value="<?php echo isset($basicData["height"])? $basicData["height"]: "";?>"/>
            </div>
            <div class="col-sm-1 text-left text-secondary align-items-center">
            公分
            </div>
          </div>
          <div class="row mt-4 mb-4 text-left align-items-center">  
            <div class="col-2">
              <label for="weight" class="text-primary">
                <strong>個人體重:</strong>
              </label>
            </div>
            <div class="col-sm-3">
              <input class="form-control" id="weight" name="weight" type="number" oninput="if(value.length > 5)value=value.slice(0,5);" min="0.01" max="200.00" step="0.01" value="<?php echo isset($basicData["weight"])? $basicData["weight"]: "";?>"/>
            </div>
            <div class="col-sm-1 text-left text-secondary align-items-center">
            公斤
            </div>
          </div>            
        </div>
      </div>
    </div>
  </div>
  <div class="col-2">
    <!--輔助說明區-->
    <div id="pop_assist_dialog" style="display:none;">
      <div class="context_regbox pl-4 font-weight-bold" id="assist-area-title"></div>
      <div class="context_regbox pl-4 pt-2 ml-1" id="assist-area-text" style="width:190px"></div>
    </div>
    <!--輔助說明區-->
  </div>
</div> 