<div class="row my-4">
  <div class="col-10 shadow p-3 mb-5 bg-white rounded">
    <div class="card">
      <div class="card-header" id="headingThree">
          <div class="row align-items-center">
            <div class="col-3">
              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#work-experience">
                <h4><b>工作經歷</b></h4>  
              </button>
            </div>
            <div class="col-7 text-left">
              <h6 class="text-secondary"><span class="text-secondary" id="total-term"></span> 總工作經歷</h6>
            </div>
            <div class="col-2">
              <div class="row text-center align-items-center" id="add-work-experience">
                <div class="col-6 text-right">
                  <i class="fa fa-plus-square-o fa-lg" aria-hidden="true"></i>
                </div>
                <div class="col-6 text-left">
                  <h5>新增</h5>
                </div>
              </div>
            </div>
          </div>
      </div>

      <div id="work-experience" class="collapse">
        <div class="card-body border-bottom" id='work-experience-info-1'>
          <div class="row mb-4 text-left align-items-center">
            <div class="col-11">
            </div>
            <!-- <div class="col-1 text-right remove-work-experience" id="remove-work-experience-1">
              <i class="fa fa-times fa-lg" aria-hidden="true"></i>
            </div> -->
          </div>  
          <div class="row mt-4 mb-4 text-left align-items-center">
            <div class="col-3">
              <input class="form-control" id="job-name-1" name="job-name-1" type="text" placeholder="請輸入職稱"/>
            </div>
            <div class="col-9 pull-right">
              <div class="row">
                <div class="col-3">
                </div>  
                <div class="col-4">
                  <input class="form-control working-term text-secondary" id="working-term-start-1" name="working-term-start-1" type="date"/>
                </div>
                <div class="col-1">
                  ~ 
                </div>
                <div class="col-4">
                  <input class="form-control working-term text-secondary" id="working-term-end-1" name="working-term-end-1" type="date"/>
                </div>
              </div>
            </div>
          </div>
          <div class="row mt-4 mb-4 text-left align-items-center">
            <div class="col-5">
              <input class="form-control" id="company-name-1" name="company-name-1" type="text" placeholder="請輸入服務公司名"/>
            </div>
            <div class="col-4">
            </div>
            <div class="col-3 text-right">
              <span class="working-time-range text-danger" id="working-time-range-1" name="working-time-range-1" type="date" title="工作年資"></span>  
            </div>
          </div>  
          <div class="row mt-4 mb-4">
            <div class="col-4">
              <input class="form-control" name="job-title-1" id="job-title-1" type="text" placeholder="請填入職務名"/>
            </div>
            <div class="col-4 border-left">
              <select class="custom-select" id="working-position-city-1" name="working-position-city-1">
                <option selected value="0">請選擇工作的城市</option>
                <?php foreach($cityInfo as $key=>$info){
                  echo '<option value="'.$info['id'].'">'.$info['name'].'</option>';
                }?>
              </select>
            </div>
            <div class="col-4 border-left">
              <select class="custom-select" id="working-position-district-1" name="working-position-district-1">
                <option selected value="0">請選擇工作的行政區</option>
              </select>
            </div>
          </div>
          <div class="row mt-4 mb-4">
            <div class="col form-group">
              <label for="working-content-1" class="my-4 text-secondary font-weight-bold">職務內容描述:</label>
              <textarea class="form-control" name="working-content-1" id="working-content-1"  col= "10" row="50" placeholder="請填入職務內容描述"></textarea>
            </div>
          </div>          
        </div>
        <?php
          if(!empty($workingExpView)){
            foreach($workingExpView as $view){ //編輯時預設呈現
              echo $view;
            } 
          }
        ?> 
      </div>
    </div>
  </div>
  <div class="col-2">
  </div>
</div>