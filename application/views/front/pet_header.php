<?php
	header("Content-Type: text/html; charset=utf-8");
?>

<!DOCTYPE>
<html lang="zh-tw">
	<head>
		<meta http-equiv="Content" Content-Type="text/html; charset=utf-8">
		<link rel="stylesheet" href="<?=$css_root;?>/front/style_1.css" type="text/css">
		<link rel="stylesheet" href="<?=$js_root;?>/lib/bootstrap-sweetalert-master/dist/sweetalert.css" >
		<title>Leo's World的入口處</title>
		<script src="<?=$js_root;?>/lib/jquery-3.3.1.min.js"></script> 
		<script src="<?=$js_root;?>/lib/bootstrap-sweetalert-master/dist/sweetalert.min.js"></script>
		<script src="<?=$js_root;?>/mapFunc.js"></script>
		<script src="<?=$js_root;?>/timer.js"></script>
<!-- 		<script src="<?=$js_root;?>/player.js"></script> -->
		<script src="<?=$js_root;?>/front/index.js"></script>
		<script type="text/javascript">

			var path="";
			var max_item=4;
			var min_item=0;
			var item=Math.floor((Math.random()*(max_item-min_item))+min_item);
			var item_arr=new Array("If_I_Had_a_Chicken.mp3","Moonlight_Haze.mp3","Neck_Pillow.mp3",
				"Whistling_Down_the_Road.mp3");
			var song_name=item_arr[item];
			path="<?=$host_root;?>"+"/audio/"+song_name;

			$(function(){
				$('#audio_info').html("※現在播放的是:"+song_name);
				$('#open_broswer').attr('href',path);
			});
			
			var player=new Audio(path);
			player.loop=true;

			function media_play(){
				player.play();
			}

			function media_pause(){
				player.pause();
			}

			function setStop(){
				player.pause();
				player.currentTime=0;
			}

			$(function(){

				swal.setDefaults(
					{confirmButtonText:"我了解了" , cancelButtonText:"取消"}
				);

			});
			
		</script>
	</head>
	<body bgcolor="#d8bfd8">
	<style type="text/css">
		<!--
			.button_margin_1 {
				margin-top: 16px;
				margin-bottom: 8px;
			}

			 .button_margin_2 {
				margin-top: 8px;
				margin-bottom: 16px;
			}
			
			.context_regbox{
				font-family: Georgia, "Times New Roman", Times, serif;
				font-size: 10pt;
				color: #0000cd;
				line-height: 120%;
			}
			
			.context_regbox_2{
				font-family: "微軟正黑體";
				font-size: 10pt;
				color: #FF0000;
				line-height: 120%;
				margin-top: 8px;
				margin-bottom: 8px;
			}
			
			.smalltext7{
				font-size: 13px;
				color: #FF0000;
				font-family: Georgia, "Times New Roman", Times, serif;
				vertical-align: middle;
			}
			
			.player_button1 {
				margin-top: 8px;
				margin-bottom: 8px;
				margin-left: 5px;
				margin-right: 5px;
			}
			
			.player_skin1 {
				margin-top: 4px;
				margin-left: 115px;
				margin-bottom: 8px;
			}
		-->
	</style>
		<table width="80%" height="100%" border="0"  cellpadding="0" cellspacing="0"  align="center" class="mainline" bgcolor="#ff4500">
		<!--中間區段1-->	
			<!--中間區段1-1-->
			<tr>
				<td  align="right" valign="top" height="104" width="1085" background="<?=$images_root;?>/banner_2_1.png" class="bannerlinker | bannerline1">
					<a href="javascript:checkTopLeftTitleStateMyFavorist();">需要協助 </a>|<a href="javascript:checkTopLeftTitleStateMyFavorist();"> 聯絡我們</a>
				</td>
			</tr>
			<!--中間區段1-1-->
			<!--中間區段1-2-->
			<tr>
				<td>
					<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td  width="93">
								<img name="banner_2_2_1" src="<?=$images_root;?>/banner_2_2_1.png" width="93" height="98" border="0" alt="">
							</td>
							<td  width="95">
									<input type="button" id="banner_2_2_2" style="background-image:url(<?=$images_root;?>/banner_2_2_2.png);width: 95px; height: 98px;border:none;"></input>	
							</td>
							<td  width="94">
								<input type="button" id="banner_2_2_3" style="background-image:url(<?=$images_root;?>/banner_2_2_3.png);width: 94px; height: 98px;border:none;"></input>
	
							</td>
							<td  width="89">
								<input type="button" id="banner_2_2_4" style="background-image:url(<?=$images_root;?>/banner_2_2_4.png);width: 89px; height: 98px;border:none;"></input>
							</td>
							<td  width="113">
								<input type="button" id="banner_2_2_5" style="background-image:url(<?=$images_root;?>/banner_2_2_5.png);width: 113px; height: 98px;border:none;"></input>	
							</td>
							<td  width="600">
								<input type="button" id="banner_2_3" style="background-image:url(<?=$images_root;?>/banner_2_3.png);width: 600px; height: 98px;border:none;"></input>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!--中間區段1-2-->
		<!--中間區段1-->	