<table  width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" align="left"  valign="top" class="msg_content_bg">
	<tr  height="100%" width="100%" valign="top">
		<td>
			<div class="open_msg_container">
				<form name="<?=$formId;?>" id="<?=$formId;?>" method="post" action="<?=$formAction;?>">
				<table width="95%" height="250" cellpadding="4" cellspacing="4" border="0" align="center">	
					<tr valign="top" align="center">
						<td id="board_heading" colspan="2" class="heading" style="padding-bottom: 20px;"><h4><b><i>【聯絡管理者-信件填寫】</i></b></h4>
						</td>
					</tr>
					<tr class="context_regbox" valign="top" align="left">
						<td width="95%">
							<?php 
								foreach ($inputArr as $inputTitleName => $inputId) {
									echo "<p>";
									echo '<label class="form-check-label" for="'.$inputId.' aria-describedby="inputGroupFileAddon0">'.$inputTitleName."&nbsp;:&nbsp;</label>";
									if($inputId == "input_content"){
										echo "<textarea type='textarea' class='form-control' id='".$inputId."' name='".$inputId."' value='' cols='50' rows='10' placeholder='請留下您想尋問的問題，我們盡快與您連絡'></textarea>";
									}else if($inputId == "mailFrom"){
										echo "<input type='text' class='form-control' id='".$inputId."' name='".$inputId."' value='' placeholder='你的連絡信箱:格式如xxx.oo@gmail.com'>";
									}else if($inputId == "mailFromName"){
										echo "<input type='text' class='form-control' id='".$inputId."' name='".$inputId."' value='' placeholder='你的大名:請留下你的暱稱或名字'>";
									}
									echo "</p>";
								}
							?>
						</td>
					</tr>	
					<tr valign="top" align="center">
						<td>
							<p align="center" valign="top"  id="window_footer">
								<input name="board_id" type="hidden" id="board_id" value="<?php echo $commentData["board_id"];?>"/>
								<input name="action" type="hidden" id="action" value="update"/>
								<input class="button_margin_3" style="background-image:url(<?=$images_root;?>/buttom_send_2.png);width: 140px; height: 40px;border:none;margin-right: 10px;" type="button" id="updateForm" onclick="checkForm();" value=""/>
								<input class="button_margin_3" style="background-image:url(<?=$images_root;?>/buttom_backward_1.png);width: 140px; height: 40px;border:none;margin-right: 20px;" type="button" id="closeForm" onclick="javascript:window.close();self.opener.location.reload();" value=""/>							
							</p>
						</td>
					</tr>
				</table>
			</form>
			</div>
		</td>
	</tr>
</table>

<script>
	//Ckeditor

	var this_ckeditor;

	function createEditor() {
	    if (ckeditor != null) {
	        if (this_ckeditor) {
	            this_ckeditor.destroy();
	        }
	    }
	}

	CKEDITOR.env.isCompatible = true;
	this_ckeditor = CKEDITOR.replace('input_content', {
		width: '98%',
		height: 220,
		resize_enabled: false,
		enterMode: 2,
		forcePasteAsPlainText: true,
		toolbar: [
			['Source', '-'],
			['Maximize', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
			['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
			['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
			['TextColor', 'BGColor', '-', 'NumberedList', 'BulletedList', ],
			'/', ['Outdent', 'Indent', 'Iineheight'],
			['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
			['Link', 'Unlink', 'Anchor'],
			['Image', 'Flash', 'Table', 'HorizontalRule', 'SpecialChar', 'PageBreak'],
			['Format', 'Font', 'FontSize']
			]
	});
</script>