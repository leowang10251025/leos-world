<?php
	header("Content-Type: text/html; charset=utf-8");
?>

<!DOCTYPE>
<html lang="zh-tw">
	<head>
		<meta http-equiv="Content" Content-Type="text/html; charset=utf-8">
    <title>Leo's World的入口處</title>
    <!-- jquery(v3.5.1) -->
    <script src="<?=$js_root;?>/lib/jquery/jquery-3.5.1.min.js"></script>
		<!-- ckeditor -->
		<script type="text/javascript" src="<?=$js_root;?>/lib/ckeditor/ckeditor.js"></script>
    <!-- sweetAlert -->
    <link rel="stylesheet" href="<?=$css_root;?>/lib/sweet_alert/sweetalert.css" >
    <script src="<?=$js_root;?>/lib/bootstrap-sweetalert-master/dist/sweetalert.min.js"></script>
		<!-- bootstrap(v4.5.3) -->
    <link rel="stylesheet" href="<?=$css_root;?>/lib/bootstrap/bootstrap.min.css">
    <script src="<?=$js_root;?>/lib/bootstrap/bootstrap.min.js"></script>
		<!-- bootstrap datetimepicker -->
    <link rel="stylesheet" href="<?=$css_root;?>/front/lib/bootstrap-datepicker3.min.css" >
		<script src="<?=$js_root;?>/lib/bootstrap-datepicker.js"></script>
		<script src="<?=$js_root;?>/lib/locales/bootstrap-datepicker.zh-TW.js"></script>
		<!-- jQuery Rolate -->
		<script src="<?=$js_root;?>/lib/jQueryRotate.2.1.js"></script>
		<script src="<?=$js_root;?>/lib/jQueryRotateCompressed.2.1.js"></script>
		<!-- hoverpulse -->
		<script src="<?=$js_root;?>/lib/jquery.hoverpulse.js"></script>
    <!-- font awesome(V6.0 Beta) -->
    <script src="<?=$js_root;?>/lib/font-awesome/awesome.js" crossorigin="anonymous"></script>
    <!-- 一般/自訂的JS/CSS樣式表 -->
		<link rel="stylesheet" href="<?=$css_root;?>/front/style_1.css" type="text/css">
		<script src="<?=$js_root;?>/front/mapFunc.js"></script>
		<script src="<?=$js_root;?>/front/timer.js"></script>
		<script src="<?=$js_root;?>/front/topLeftNavi.js"></script>
		<script src="<?=$js_root;?>/front/anim_setting.js"></script>
		<script src="<?=$js_root;?>/common/basic_tools.js"></script>
    
		<?php if(isset($pageName)){
				if($pageName == "home" || $pageName == "haveLogin"){ ?>
					<script src="<?=$js_root;?>/front/home.js"></script>
					<script src="<?=$js_root;?>/front/navigation_bar.js"></script>
		<?php 	}else if($pageName == "member_join"){?>
					<script src="<?=$js_root;?>/front/member_join.js"></script>
					<script src="<?=$js_root;?>/front/navigation_bar.js"></script>
		<?php 	}else if($pageName == "pet"){ ?>
					<script src="<?=$js_root;?>/front/pet.js"></script>
					<script src="<?=$js_root;?>/front/navigation_bar.js"></script>
		<?php 	}else if($pageName == "admin_passmail"){ ?>
					<script src="<?=$js_root;?>/front/forgot_pw.js"></script>
					<script src="<?=$js_root;?>/front/navigation_bar.js"></script>
		<?php 	}else if($pageName == "member_admin"){ ?>
					<script src="<?=$js_root;?>/front/member_admin.js"></script>
					<script src="<?=$js_root;?>/front/navigation_bar.js"></script>
		<?php 	}else if($pageName == "member_admin_update"){ ?>
					<script src="<?=$js_root;?>/front/member_admin_update.js"></script>
					<script src="<?=$js_root;?>/front/navigation_bar.js"></script>
		<?php 	}else if($pageName == "member_center"){ ?>
					<script src="<?=$js_root;?>/front/member_center.js"></script>
					<script src="<?=$js_root;?>/front/navigation_bar.js"></script>		
		<?php 	}else if($pageName == "member_center_update"){ ?>
					<script src="<?=$js_root;?>/front/member_center_update.js"></script>
					<script src="<?=$js_root;?>/front/navigation_bar.js"></script>
		<?php 	}else if($pageName == "helper_questions"){ ?>
				<script src="<?=$js_root;?>/front/helper_questions.js"></script>
				<script src="<?=$js_root;?>/front/navigation_bar.js"></script>
		<?php 	}else if($pageName == "helper"){ ?>
				<script src="<?=$js_root;?>/front/navigation_bar.js"></script>
				<script src="<?=$js_root;?>/front/helper.js"></script>
		<?php 	}else if($pageName == "contact"){ ?>
				<script src="<?=$js_root;?>/front/navigation_bar.js"></script>
				<script src="<?=$js_root;?>/front/contact.js"></script>
		<?php 	}else if($pageName == "post"){ ?>
				<script src="<?=$js_root;?>/front/post.js"></script>
				<script src="<?=$js_root;?>/front/navigation_bar.js"></script>
		<?php 	}else if($pageName == "show_error"){ ?>
				<script src="<?=$js_root;?>/front/show_error.js"></script>
				<script src="<?=$js_root;?>/front/navigation_bar.js"></script>
    <?php 	}else if($pageName == "autobiography"){ ?>
        <script src="<?=$js_root;?>/front/navigation_bar.js"></script>
        <script src="/js/front/autobiography.js"></script>
		<?php 	}?>
    				
		<?php }?>
		<?php if(isset($boardAdminLogin)){?>
			<input type="hidden" name="boardAdminLogin" id="boardAdminLogin" value="<?=$boardAdminLogin;?>" />
		<?php }?>
		<script type="text/javascript">

			var path="";
			var max_item=4;
			var min_item=0;
			var item=Math.floor((Math.random()*(max_item-min_item))+min_item);
			var item_arr=new Array("If_I_Had_a_Chicken.mp3","Moonlight_Haze.mp3","Neck_Pillow.mp3",
				"Whistling_Down_the_Road.mp3");
			var song_name=item_arr[item];
			path="<?=$host_root;?>"+"/audio/"+song_name;

			$(function(){
				$('#audio_info').html("※現在播放的是:"+song_name);
				$('#open_broswer').attr('href',path);
			});
			
			var player=new Audio(path);
			player.loop=true;

			function media_play(){
				player.play();
			}

			function media_pause(){
				player.pause();
			}

			function setStop(){
				player.pause();
				player.currentTime=0;
			}

			$('[type="date"]').datepicker({
			    format: "yyyy-mm-dd",
			    autoclose: true,
			    startDate: "today",
			    clearBtn: true,
			    calendarWeeks: true,
			    todayHighlight: true,
			    language: 'zh-TW'
			});

		</script>
	</head>
	<body>
		<style type="text/css">
			
				.button_margin_1 {
					margin-top: 5px;
					margin-bottom: 2px;
				}

				 .button_margin_2 {
					margin-top: 3px;
					margin-bottom: 5px;
				}
				
				.context_regbox{
					font-family: Georgia, "Times New Roman", Times, serif;
					font-size: 10pt;
					color: #0000cd;
					line-height: 200%;
					text-align: left;
					padding-right: 5px;
				}

				.pop_menu_text{
					font-family: Georgia, "Times New Roman", Times, serif;
					font-size: 10pt;
					color: #0000cd;
					line-height: 200%;
					text-align: left;
				}

				.pop_menu_text_title{
					font-weight: bold;
				}

				#userNameDiv, #eMailDiv{
					font-family: Georgia, "Times New Roman", Times, serif;
					font-size: 10pt;
					color: #0000cd;
					line-height: 120%;
					margin-left: 15px;
				}
				
				.context_regbox_2{
					font-family: "微軟正黑體";
					font-size: 10pt;
					color: #FF0000;
					line-height: 120%;
					margin-top: 8px;
					margin-bottom: 8px;
				}
				
				.smalltext7{
					font-size: 13px;
					color: #FF0000;
					font-family: Georgia, "Times New Roman", Times, serif;
					vertical-align: middle;
				}
				
				.player_button1 {
					margin-top: 8px;
					margin-bottom: 8px;
					margin-left: 5px;
					margin-right: 5px;
				}
				
				.player_skin1 {
					margin-top: 4px;
					margin-left: 115px;
					margin-bottom: 8px;
				}

				#banner_bg{
					background-size:cover;
				}

				.button_margin_3 {
					background: transparent;
    				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE8 */ 
    				filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE6 & 7 */
				}

				#banner_2_3, #banner_2_2_2, #banner_2_2_3, #banner_2_2_4, #banner_2_2_5, #help_area, #show_error_area {
					background: transparent;
    				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE8 */ 
    				filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE6 & 7 */
					background-image: url(<?=$images_root;?>/msg_box_1.png);
					background-size:100% 100%;
				}

				#button_margin_1{
					background: transparent;
    				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE8 */ 
    				filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE6 & 7 */
    				background-image:url(<?=$images_root;?>/buttom_send_1.png);
    				width: 140px; 
    				height: 40px;
    				border:none;
				}

				#button_margin_2{
					background: transparent;
    				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE8 */ 
    				filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE6 & 7 */
    				background-image:url(<?=$images_root;?>/buttom_backward_1.png);
    				width: 140px; 
    				height: 40px;
    				border:none;
				}

				#help_area {
					background: transparent;
    				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE8 */ 
    				filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE6 & 7 */
    				background-image:url(<?=$images_root;?>/helper_area_bg.png);
					background-size:100% 100%;
				}

				#contact_area {
					background: transparent;
    				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE8 */ 
    				filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE6 & 7 */
    				background-image:url(<?=$images_root;?>/contact_area_bg.png);
					background-size:100% 100%;
				}

				#show_error_area {
					background: transparent;
    				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE8 */ 
    				filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE6 & 7 */
					background-image: url(<?=$images_root;?>/error_area_bg.png);
					background-size:80% 100%;
					background-repeat: no-repeat;
				}

				.msg_content_bg {
					background: transparent;
    				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE8 */ 
    				filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE6 & 7 */
    				background-image: url(<?=$images_root;?>/msg_content_bg.png);
    				background-color: #e6e6e6;
    				background-repeat: no-repeat;
					background-size:100% 90%;
				}

				#mainbox{
					z-index:1;
				}

				#regbox{
					background-image:url(<?=$images_root;?>/right_menu_bar.png);
					background-size: 100% 100%;
					background-repeat: no-repeat;
					z-index:2;
				}

				.pop_menu{
					z-index:4;
					background-image:url(<?=$images_root;?>/pop_menu_bg_1.png);
					background-repeat: no-repeat;
				}

        #pop_menu_login{
					position: absolute;
					top: 25%;
					right: 15%;
					width: 280px;
					height: 450px;
					padding-left: 15px;
					padding-top: 15px;
					padding-bottom: 15px;
					padding-right: 47px;
				}

        #pop_assist_dialog{
					z-index:4;
					/* background-image:url(<?=$images_root;?>/pop_menu_bg_6.png); */
					background-repeat: no-repeat;
          position: fixed;
					top: 20%;
					left: 70%;
					width: 280px;
					height: 450px;
					padding: 15px;
				}

        #pop_autobiography_login{
					z-index:4;
					background-image:url(<?=$images_root;?>/pop_menu_bg_1.png);
					background-repeat: no-repeat;
          position: fixed;
					top: 30%;
					left: 26%;
					width: 300px;
					height: 300px;
					padding: 15px;
				}

        #autobiography-pw-dialog{
					position: absolute;
					top: 70%;
					right: 15%;
					width: 280px;
					height: 220px;
					padding-left: 15px;
					padding-top: 15px;
					padding-bottom: 15px;
					padding-right: 47px;
				}

				#pop_menu_login_pet{
					z-index:4;
					background-image:url(<?=$images_root;?>/pop_menu_bg_2.png);
					background-repeat: no-repeat;
					position: absolute;
					top: 25%;
					right: 15%;
					width: 280px;
					height: 450px;
					padding-left: 15px;
					padding-top: 15px;
					padding-bottom: 15px;
					padding-right: 47px;
				}

				#pop_menu_forgotPw{
					z-index:4;
					background-image:url(<?=$images_root;?>/pop_menu_bg_4.png);
					background-repeat: no-repeat;
					position: absolute;
					top: 48%;
					right: 15%;
					width: 300px;
					height: 430px;
					padding-left: 19px;
					padding-top: 15px;
					padding-bottom: 15px;
					padding-right: 55px;
				}

				#pop_menu_applyNotice{
					z-index:4;
					background-image:url(<?=$images_root;?>/pop_menu_bg_1.png);
					background-repeat: no-repeat;
					position: absolute;
					top: 56%;
					right: 15%;
					width: 280px;
					height: 450px;
					padding-left: 15px;
					padding-top: 15px;
					padding-bottom: 15px;
					padding-right: 45px;
				}

				#pop_menu_adminInfo, #pop_menu_memberInfo{
					z-index:4;
					background-image:url(<?=$images_root;?>/pop_menu_bg_3.png);
					background-repeat: no-repeat;
					position: absolute;
					top: 49%;
					right: 15%;
					width: 320px;
					height: 450px;
					padding-left: 15px;
					padding-top: 15px;
					padding-bottom: 15px;
					padding-right: 47px;
				}

				#pop_menu_loginBoardAdmin{
					z-index:4;
					background-image:url(<?=$images_root;?>/pop_menu_bg_5.png);
					background-repeat: no-repeat;
					position: absolute;
					top: 49%;
					right: 15%;
					width: 280px;
					height: 500px;
					padding-left: 19px;
					padding-top: 15px;
					padding-bottom: 15px;
					padding-right: 55px;
				}

				#pop_menu_content{
					z-index:3;
				}

				#menuLastArea{
					width: 100%;
					height: 100%;
					background-repeat: no-repeat;
					background-size: 100% 100%;
				}

				#menuBtn{
					background-repeat: no-repeat;
					background-size: 100% 100%;
				}

				#button_icon{
					padding: 2px;
					display:flex;
				  align-items:center;
				  justify-content:center;
				}

				.button_text{
					background-image:url(<?=$images_root;?>/pop_menu_buttom_bg_2.png);
					background: transparent;
					width: 100%;
					height: 70px;
					font-family: "微軟正黑體";
					font-size: 13pt;
					color: #ffffff;
					border:none;
					text-align: left;
				}

				.corner_img_div{
					width: 200px;
					height: 200px;
				}

				.corner_img{
					position: absolute;
					transform-origin: center;
					width: 170px;
					height: 170px;
				}
		</style>	
		<table class="text-center" width="100%" height="100%" cellpadding="0" cellspacing="0">
		<!--中間區段1-->	
			<!--中間區段1-1-->
			<tr style="background-image: url(<?=$images_root;?>/banner.png)" class="banner">
				<td  class="text-right" valign="top" height="200" width="100%" class="bannerlinker" id="banner_bg">
					<div id="loginMemInfo"><?=$memberName;?>，您好! <?=$loginInfo;?></div>
					<table class="text-left" cellpadding="0" cellspacing="0" width="100%">
						<tr style="position: absolute;top:102px;left:93px;">
							<td  width="93">
								<?php if(!empty($loginChk)){?>
									<input type="hidden" id="loginChk" class="loginChk" value="<?=$loginChk;?>"/>
								<?php }?>
								<?php if(!empty($userId)){?>
									<input type="hidden" id="userId" class="userId" value="<?=$userId;?>"/>
								<?php }?>
								<?php if(!empty($memberLevel)){?>
									<input type="hidden" id="memberLevel" class="memberLevel" value="<?=$memberLevel;?>"/>
								<?php }?>
								<?php if(isset($pageName)){ ?>
									 	<input type="hidden" id="pageName" class="pageName" value="<?=$pageName;?>"/>
								<?php }?>	
                <?php if(isset($loginAutobiography)){ ?>
									 	<input type="hidden" id="loginAutobiography" class="loginAutobiography" value="<?=$loginAutobiography;?>"/>
								<?php }?>	
							</td>
							<td  width="95">
								<input type="button" id="banner_2_2_2" style="background-image:url(<?=$images_root;?>/banner_2_2_2.png);width: 95px; height: 98px;border:none;"></input>	
							</td>
							<td  width="94">
								<input type="button" id="banner_2_2_3" style="background-image:url(<?=$images_root;?>/banner_2_2_3.png);width: 94px; height: 98px;border:none;"></input>
	
							</td>
							<td  width="89">
								<input type="button" id="banner_2_2_4" style="background-image:url(<?=$images_root;?>/banner_2_2_4.png);width: 89px; height: 98px;border:none;"></input>
							</td>
							<td  width="113">
								<input type="button" id="banner_2_2_5" style="background-image:url(<?=$images_root;?>/banner_2_2_5.png);width: 113px; height: 98px;border:none;"></input>	
							</td>
							<td  width="600">
								<input type="button" id="banner_2_3" style="background-image:url(<?=$images_root;?>/banner_2_3.png);width: 600px; height: 98px;border:none;position: absolute;top:0px;left:60%;"></input>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!--中間區段1-2-->
		<!--中間區段1-->	