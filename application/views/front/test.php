<html>
	<head>
		<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		<script src="//apps.bdimg.com/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<link rel="stylesheet" href="//apps.bdimg.com/libs/jqueryui/1.10.4/css/jquery-ui.min.css">
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<!-- bootstrap -->
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

		<script type="text/javascript">
			$(function() {
				// alert("Test");
			});
		</script>
	</head>
	<body>
		<div style="margin: 10px; width: 10%;height: 10%;background-color: #00ff00;">
			<?=$body_content1;?>
		</div>
		<div style="margin: 10px; width: 10%;height: 10%;background-color: #ff0000;">
			<?=$body_content2;?>
		</div>
		<div style="margin: 10px; width: 10%;height: 10%;background-color: #0000ff;">
			<?=$body_content3;?>
		</div>
		<form name="formTest" id="formTest" action="/test/formProcess" method="POST" >
			<p>
				<label for="userName" class="form-check-label" aria-describedby="inputGroupFileAddon02">Your Name :</label>
				<input type="text" id="userName" name="userName" class="form-control" placeholder="Plese Entry Your Name">
			</p>
			<p>
				<label for="Password" class="form-check-label" aria-describedby="inputGroupFileAddon02">Your Password :</label>
				<input type="password" id="Password" name="Password" class="form-control" placeholder="Plese Entry Your Password">
			</p>
			<p>
				<label for="Level" class="form-check-label" aria-describedby="inputGroupFileAddon02">Your Education Level:</label>
				<select id="Level" name="Level" size="4" multiple="multiple">
					<option value="Rookie">Rookie</option>
					<option value="Junior">Junior</option>
					<option value="Senior">Senior</option>
					<option value="Superior">Superior</option>
				</select>
			</p>
			<p>
				<input type="reset" name="reset" value="Reset Input Data"/>
			</p>
			<p>
				<input type="submit" name="submit" value="Send Your Data!"/>
			</p>
		</form>
	</body>
	<footer></footer>
</html>