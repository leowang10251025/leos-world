    <div class="row my-4">
      <div class="col-10 float-right text-right">
      <?php if(empty($basicData["id"])){?>
        <button type="submit" class="btn btn-outline-primary btn-submit shadow p-3 mb-5 rounded mx-2 my-2">
          <i class="fa fa-pencil-square-o p-1" aria-hidden="true"></i>&nbsp;&nbsp;新增
        </button>
      <?php }else{?>
        <button type="submit" class="btn btn-outline-primary btn-submit shadow p-3 mb-5 rounded mx-2 my-2">
          <i class="fa fa-pencil-square-o p-1" aria-hidden="true"></i>&nbsp;&nbsp;修改
        </button>
      <?php }?>  
        <button type="reset" class="btn btn-outline-info shadow p-3 mb-5 rounded mx-2 my-2" id ="btn-reset">
        <i class="fa fa-refresh p-1" aria-hidden="true"></i>&nbsp;&nbsp;清空</i>
        </button>
        </form>
        <button type="button" class="btn btn-outline-secondary shadow p-3 mb-5 rounded mx-2 my-2" id ="btn-logout">
        <i class="fa fa-user-o p-1" aria-hidden="true"></i>&nbsp;&nbsp;登出</i>
        </button>
      </div>
      <div class="col-2">
        <div id="corner_img_div" style="display: none;">
          <img id="corner_img" src="<?=$images_root;?>/talking.png" style="margin-right: 30px;margin-bottom: 15px;">
        </div>
      </div>
    </div>
  </div>

<!--修改時，點擊按鈕跳出小視窗-->
<div class="pr-5" id="pop_autobiography_login" style="display: none;">
  <div class="row mx-1 mt-2 mb-4 px-1 py-2 align-center border-bottom border-secondary text-danger">
    <b>&#9830; &#9674;修改時請先輸入資料 &#9674; &#9830;</b>
  </div>
  <div class="row py-2 justify-content-center">	
    <div class="col-3 text-center">
      <label class="form-check-label"><h6 class="text-primary">帳號 </h6></label>
    </div>
    <div class="col-8">	
      <input id="login-account" type="text" class="form-control">
    </div>
  </div>
  <div class="row py-2 justify-content-center">	
    <div class="col-3 text-center">
      <label class="form-check-label"><h6 class="text-primary">密碼 </h6></label>
    </div>
    <div class="col-8">	
      <input id="login-password" type="password" class="form-control">
    </div>
  </div>
  <div class="row mr-1 my-4">
    <div class="col-4">
    </div>
    <div class="col-8" style="opacity: 50%;">
      <input  class="button_margin_1" id="login-submit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
    </div>
  </div>
</div>
<!--修改時，點擊按鈕跳出小視窗-->

<input type="hidden" name="basicId" id="basicId" value="<?php echo isset($basicData["id"])? $basicData["id"]: "";?>"/>
<input type="hidden" name="cityId" id="cityId" value="<?php echo isset($cityId)? $cityId: "";?>"/>
<input type="hidden" name="districtId" id="districtId" value="<?php echo isset($districtId)? $districtId: "";?>"/>
<input type="hidden" name="birthDayVal" id="birthDayVal" value="<?php echo isset($birthDayVal)? $birthDayVal: "";?>"/>
<?php
  if(isset($educationData)){ //學歷編輯時預設資料
    foreach($educationData as $itemNum=> $arr){
      foreach($arr as $index=> $val){
        echo '<input type="hidden" name="educationData" id="'.$index."-".$itemNum.'-data" value="'.$val.'";/>';
      }
    }
  }
  if(isset($workingExpData)){ //工作經歷編輯時預設資料
    foreach($workingExpData as $itemNum=> $arr){
      foreach($arr as $index=> $val){
        echo '<input type="hidden" name="workingExpData" id="'.$index."-".$itemNum.'-data" value="'.$val.'";/>';
      }
    }
  }
?>