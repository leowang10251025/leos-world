			<!--中間區塊2-->
			<tr align="left">
				<td align="center" width="100%" height="98%" style="background-color: #ffffff;">
					<table id="board_main" width="100%"  height="100%" align="center" valign="middle" cellspacing="0" cellpadding="0" border="0">
						<tr valign="middle">
							<td id="mainbox3" valign="top" align="center" width="75%" height="100%">
								<table id="maincontent3" width="90%" height="544" border="0" cellpadding="0" cellspacing="0" align="left"  valign="bottom">
									<tr height="100%" width="90%" valign="top">
										<td>
										<div id="mainRegion" style="padding-left: 10%;">
											<div style="width: 100%; height: 90%;">
											<?php
											foreach ($commentArr as $key => $commentData){ 
												if($loginStatus == "notLogin" || $loginStatus == "logoutSucess"){
													$msgContent = "sticker_content";
												}else{
													if($commentData["comment_type"] == 'N'){
														$msgContent = "msg_bg_normal";
													}elseif ($commentData["comment_type"] == 'H') {
														$msgContent = "msg_bg_helper";
													}
												}
												?>
												<table class="<?=$msgContent;?>" id="<?=$msgContent;?>" align="center" valign="top" border="0" width="87%" cellpadding="3" cellspacing="0">
													<tr valign="top" height="120">
														<td class="board_content" width="60" align="center">
															<?php if($commentData["board_sex"]=="M"){?>
															<img src="<?=$images_root;?>/male_1.png" width="83" height="120" alt="我是男生"/>
															<?php }else{?>
															<img src="<?=$images_root;?>/female_1.png" width="83" height="120" alt="我是女生"/>
															<?php }?>
															<span class="postname" width="100%">
																<?php echo $commentData["board_name"]; ?>
															</span><br/>
															<span id="smalltext_2" style="	padding-left: 15px;">[<?php echo $commentData["board_id"]; ?>]</span>
														</td>		
														<td class="board_content">
															<div id="content" style="width: 580px; height: 190px;padding-left: 6%; padding-top: 10px;">
																<span class="heading" style="padding-left: 20px;padding-bottom: 30px;">
																	<?php echo $commentData["board_subject"]; ?>
																</span>
																<p style="height: 100px;width: 410px;padding-left: 40px;margin-top: 10px;">
																	<wbr>
																	<?php echo trim(nl2br($commentData["board_content"])); ?>
																	</wbr>		
																</p>
															</div>
														</td>	
														<td class="board_button_area">
															<span id="actiondiv" vlign="bottom" style="width: 80px;">
																<a onclick="windowOpen('/adminfix/index/<?php echo "$userId".'/'.$commentData["board_id"]; ?>', '留言修改', 800,550)">
																	<img id="board_button" src="<?=$images_root;?>/message_modify.png" width="32" height="32" border="0" align="absmiddle" alt="留言修改">
																</a><br/>
																<a onclick="windowOpen('/admindel/index/<?php echo "$userId".'/'.$commentData["board_id"]; ?>', '留言刪除', 800, 550)">
																	<img  id="board_button" src="<?=$images_root;?>/message_delete.png" width="32" height="32" border="0" align="absmiddle" alt="留言刪除">
																</a><br/>
																<?php if(!empty($commentData["board_mail"]) && $loginStatus== 'loginSucess'){ ?>
																<a onclick="windowOpen('/message_board/inputMailContent/<?php echo $commentData["board_id"];?>', '聯絡方式', 800, 550);">
																	<img id="board_button" src="<?=$images_root;?>/menuIcon_contact.png" width="32" height="32" border="0" align="absmiddle" alt="電子郵件">
																</a><br/>
																<?php }?>
																<?php if(!empty($commentData["board_web"]) && $loginStatus== 'loginSucess'){?>
																	<a href="<?php echo $commentData['board_web'];?>">
																		<img id="board_button" src="<?=$images_root;?>/menuIcon_homePage.png" width="32" height="32" border="0" align="absmiddle" alt="個人網站">
																	</a><br/>
																<?php }?>
																	<br/>
																	<span style="width: 100px;">
																		<?php echo "留言時間:<br/> ".substr($commentData["board_time"], 0, -6); ?>
																	</span>
																
															</span>
														</td>	
														<?php if($key>0){?>			
														<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
														<?php }?>
													</tr>
												</table>
												<?php }?>
												</div>		
												<div style="width: 100%;">
												<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
												<table id="board_bottom" height="100" width="100%" cellpadding="0" cellspacing="0" border="0" align="center" >
													<tr>
														<td valign="middle" align="left">
															<p>總共資料有 <?php echo $totalRecords; ?> 筆 | 現在是第 <?php echo $numPage; ?> 頁 | 全部有 <?php echo $totalPages; ?> 頁
															</p>
														</td>
														<td align="right">
														<div id="page_selector">
															<p>
															<?php if($numPage>1){?>
															<a href="/message_board/index/<?php if($loginStatus != "loginSucess"){echo $userId."/1/0/".$loginStatus."/0";}else{echo $userId."/1/0/".$loginStatus."/0";}?>">第一頁 |</a>
															<a href="/message_board/index/<?php echo  $userId."/"; if($numPage<1){ $numPage=1;}  if($loginStatus != "loginSucess"){ echo ($numPage-1)."/0/".$loginStatus."/0";}else{ echo ($numPage-1)."/0/".$loginStatus."/0";} ?>"> 上一頁 |</a>
															<?php }?>
															<?php if($numPage<$totalPages){?>
															<a href="/message_board/index/<?=$userId;?>/<?php if($numPage>$totalPages){ $numPage=$totalPages;}  if($loginStatus != "loginSucess"){ echo ($numPage+1)."/0/".$loginStatus."/0";}else{echo ($numPage+1)."/0/".$loginStatus."/0";} ?>"> 下一頁|</a>
															<a href="/message_board/index/<?=$userId;?>/<?php if($loginStatus != "loginSucess"){ echo $totalPages."/0/".$loginStatus."/0";}else{ echo $totalPages."/0/".$loginStatus."/0";} ?>"> 最後頁</a>
															<?php }?>
															</p>
														</div>
														<?php if(!empty($sendStatus)){?>
															<input type="hidden" id="sendStatus" class="sendStatus" value="<?=$sendStatus;?>"/>
														<?php }?>
														<?php if(!empty($loginStatus)){?>
															<input type="hidden" id="loginStatus" class="loginStatus" value="<?=$loginStatus;?>"/>
														<?php }?>
														<?php if(!empty($dataStatus)){?>
															<input type="hidden" id="dataStatus" class="dataStatus" value="<?=$dataStatus;?>"/>
														<?php }?>				
														</td>
													</tr>
												</table><!--資料顯示與頁數控制-->		
												</div>
											</div>	
										</td>
									</tr>
								</table>
							</td>
							<!--左側填寫內容-->
							<!--右下方圖案-->
							<td valign="bottom" align="right" width="10%" style="background-color: #ffffff;">
								<div id="corner_img_div" style="display: none;">
									<img id="corner_img" src="<?=$images_root;?>/talking.png" style="margin-right: 30px;margin-bottom: 15px;">
								</div>
							</td>
							<!--右下方圖案-->
							<!--Menu跳出小視窗1-->
							<div id="pop_menu_login" class="pop_menu" style="display: none;">
								<!-- 中間區塊2-2的跳出小視窗1  -->
								<p class="heading" align="center">&#9674; &#9830; 登出會員系統 &#9830; &#9674;</p>
								<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
								<p align="center" class="smalltext7">&#8855;  如有修改會員、管理員密碼 &#8855;<br/>&#8855; 請重新登入!謝謝! &#8855; </p>
								<p align="center" style="opacity: 50%">
									<a href="/home/logout" >
										<img class="button_margin_1" name="index_logout" id="index_logout" src="<?=$images_root;?>/buttom_logout_1.png">
									</a>
								</p>
							</div>
							<!--Menu跳出小視窗1-->
							<!--Menu跳出小視窗2-->
							<div id="pop_menu_loginBoardAdmin" style="display: none;">
							<!-- 中間區塊2-2的跳出小視窗2  -->							
							<?php if($loginStatus == "notLogin" || $loginStatus == "logoutSucess"){
							//未登入管理頁面?>
							<!--中間區塊2-2-->
									<form name="formAdminLogin" id="formAdminLogin" method="post" action="/message_board/adminLogin" onSubmit="return checkSubForm();">
										<p class="heading_1" align="center" style="padding: 0px;margin-top: 10px;"> &#9830; &#9674; 留言板管理系統 &#9674; &#9830; </p>
										<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
										<p class="context_regbox"  style="text-align: left;">
											<label class="form-check-label" for="username" aria-describedby="inputGouprFileAddon02">管理帳號: </label>
											<input name="username" id="username" type="text" class="form-control"/>
										</p>
										<p class="context_regbox"  style="text-align: left;">
											<label class="form-check-label" for="passwd" aria-describedby="inputGouprFileAddon02">管理密碼: </label>
											<input name="passwd" id="passwd" type="password" class="form-control"/>
										</p>
										<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />										
										<input type="hidden" id="chkLoginName" class="chkLoginName" value=""/>
										<input type="hidden" id="chkLoginPw" class="chkLoginPw" value=""/>
										<p align="right" style="padding: 0px;">
											<p align="right">

											<input class="button_margin_1" id="loginBoardAdmin" src="<?=$images_root;?>/buttom_login_1.png" type="image"/>

											<a href="javascript:window.history.back()" bgcolor="#d8bfd8"><img class="button_margin_2" src="<?=$images_root;?>/buttom_backward_1.png"></a>
											</p>
										</p>
										<img id="board_key" src="<?=$images_root;?>/navi_content_key.png">
									</form>
 							<!--中間區塊2-2-->
							<?php }else if($loginStatus == "loginSucess"){ //已登入管理頁面?>
							<!--中間區塊2-2-->
								<p class="heading" align="right"> &#9674; &#9830; 留言板管理系統 &#9830; &#9674;</p>
								<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
								<p align="center">
									<a href="/message_board/adminLogout" bgcolor="#d8bfd8"><img class="button_margin_1" id="regbox3_backward" src="<?=$images_root;?>/buttom_logout_1.png"></a>
									<img id="board_key_1" src="<?=$images_root;?>/navi_content_key.png">
								</p>
							<!--中間區塊2-2-->
							<?php }?>
							</div>
							<!--Menu跳出小視窗2-->
              <!--Menu跳出小視窗3(經歷與自傳:輸入密碼)-->
              <?php if(!isset($loginAutobiography) || $loginAutobiography != "sucess"){?>
                <div class="pop_menu" id="autobiography-pw-dialog" style="display: none;">
                  <p class="context_regbox">
                    <label class="form-check-label" for="account-input" aria-describedby="inputGroupFileAddon02">自傳閱覽帳號: </label>
                    <input class="form-control account-input" id="account-input" type="text">
                  </p>
                  <p class="context_regbox">
                    <label class="form-check-label" for="password-input" aria-describedby="inputGroupFileAddon02">自傳閱覽密碼: </label>
                    <input class="form-control password-input" id="password-input" type="password">
                  </p>
                  <p class="align-center" style="opacity: 50%;">
                    <input  class="button_margin_1 password-submit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
                  </p>
                </div>
              <?php }?>
              <!--Menu跳出小視窗3(經歷與自傳:輸入密碼)-->
							<!--中間區塊2-2-->
							<td id="regbox" align="center" valign="top" width="15%">
								<table id="pop_menu_content" width="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="top">
									<?=$menuContext;?>
								</table>
								<div id="menuLastArea" style="background-image:url(<?=$images_root;?>/pop_menu_buttom_bg.png);"></div>
							</td>
							<!--中間區塊2-2-->
						</tr>
				</table>	
				</td>
			</tr>
			<!--中間區塊2-->