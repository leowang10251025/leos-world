			<style type="text/css">
			<!--
				#exp_content{
					cursor: help;
				}
				
				#light_button:hover{
					cursor: pointer;
				}
				
				#point2_1:hover,#point2_2:hover{
					cursor: crosshair;
				}
				
				.point{
					width:20%;
					font-family: "微軟正黑體";
					font-size: 14pt;
					color: #FF3300;
					text-align: center;
					font-style:italic;
					font-weight: bolder;
				}
				
				.point_content{
					font-family: "微軟正黑體";
					font-size: 11pt;
					color: #242376;
					font-weight: normal;
				}
				
				#point2_2_1{
					margin-top: 45px;
					margin-left: 615px;
				}

				#point2_2_1_box{
					margin-top:0px;
					margin-left: 660px;
				}
				
				#point2_2_2{
					margin-top: 98px;
					margin-left: 645px;
				}
				
				#point2_2_2_box{
					margin-top:0px;
					margin-left: 660px;
				}

				#point2_2_1_content{
				}
				
				#point2_2_2_content{
				}
				
				.photo{
					cursor:auto;
					padding-top:45px;
					background-attachment:scroll;
					background-position:center,center;
					background-repeat:no-repeat;
				}
				
				.player_button1 {
						margin-top: 8px;
						margin-bottom: 8px;
						margin-left: 5px;
						margin-right: 5px;
				}
					
				.player_skin1 {
					margin-top: 4px;
					margin-left: 115px;
					margin-bottom: 8px;
				}
			-->
			</style>			

			<!--中間區段2-->
			<tr align="left">
				<td align="center" width="100%" height="600">
					<table id="board_main" width="100%"  height="100%" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
						<tr valign="middle">
							<td valign="top" align="center" width="75%" height="100%" style="background-color: #ffffff;">
								<table id="maincontent4" width="1000" height="100%" border="0" cellpadding="0" cellspacing="0" align="left"  valign="top" >
									<tr height="100%" width="100%">
										<td>
											<div id="mainRegion4">
												<form name="formAns" method="post" action="">
												<table width="93%" height="100%"  align="center" cellpadding="0" cellspacing="1"> 													
													<tr valign="top" align="center">
														<td colspan="2" class="heading">【地圖顯示常見錯誤說明】</td>
													</tr>
													<tr valign="top">
														<td background="<?=$images_root;?>/ques2-2.png" width="900" height="450" class="photo">
															<p class="point" id="point2_2_1">錯誤訊息顯示處:</p>
															<div style="width: 220px;height: 60px;" id="point2_2_1_box">
																<b class="point_content" id="point2_2_1_content">通常出現此訊息是連線出現異常狀態，依照各瀏覽器類型所顯示圖樣不同顯示，建議你先檢查網路連線是否正常，有時會是Google地圖的伺服主機出現了問題。你可以嘗試用左下方"聯絡我們"圖樣點選。即可聯絡我們並告知情況問題。
																</b>
															</div>	
															<p class="point" id="point2_2_2">Google地理資訊錯誤訊息:</p>
															<div style="width: 220px;height: 60px;" id="point2_2_2_box">
																<b class="point_content" id="point2_2_2_content">通常出現連結問題時，會出現"連線逾時"、"無法取得位置資訊"，當然還有其他情況，如無法取得位置。另一種則為瀏覽器不支援或無法取得API權限，則會出現"沒有權限使用Geolocation API"字樣。
																</b>
															</div>	
														</td>
													</tr>
													<tr valign="middle" align="right">
														<td >
															<p align="left">
																<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;margin-top: 60px;" />
																<table width="100%" height="100%">
																	<tr align="center">
																		<td align="left" valign="middle">
																			<p align="left" valign="middle">
																				<strong>
																					※如果出現以上地圖頁面錯誤訊息頁面，通常除了Google Map伺服器出了問題之外，一般是使<br/>
																					 &nbsp;&nbsp;用者本身與網際網路連線上的問題。如出現<a id="exp_content" href="#point2_2_1">"錯誤訊息顯示處"</a>出現圖示。除了地圖主視窗會出現<br/> 
																					&nbsp;&nbsp;連線的問題圖示告知之外，其他系統會自動偵錯並出現各式的錯誤訊息，此時會出現<a id="exp_content" href="#point2_2_2">Google地<br/>
																					&nbsp;&nbsp;理資訊錯誤訊息</a>資訊告知相關使用者。<br/>
																				</strong>
																			</p>
																		</td>
																		<td align="right" valign="middle">
																			<!--a href="javascript:window.history.back()"><img id="light_button" src="<?=$images_root;?>/Light-1.png"/></a-->
																			<input type="image" id="light_button1" src="<?=$images_root;?>/Light-1.png" value="solve" onClick="return checkForm(this.id);"/>
																			<input type="image" id="light_button2" src="<?=$images_root;?>/Light-2.png" value="issue" onClick="return checkForm(this.id);"/>
																			<input type="image" id="reply_button1" src="<?=$images_root;?>/ques_reply.png" value="reply" onClick="return checkForm(this.id);"/>
																		</td>
																	</tr>
																</table>
															</p>	
														</td>
													</tr>
												</table>
											</form>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<!--右下方圖案-->
							<td valign="bottom" align="right" width="10%" style="background-color: #ffffff;">
								<div id="corner_img_div" style="display: none;">
									<img id="corner_img" src="<?=$images_root;?>/talking.png" style="margin-right: 30px;margin-bottom: 15px;">
								</div>
							</td>
							<!--右下方圖案-->
							<!--Menu跳出小視窗1-->
							<div id="pop_menu_login" class="pop_menu" style="display: none;">
								<!-- 中間區塊2-2的跳出小視窗1  -->
								<form name="formReg" id="formReg" method="post" >
									<p class="heading" align="center">&#9674; &#9830; 登出會員系統 &#9830; &#9674;</p>
									<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
									<p align="center" class="smalltext7">&#8855;  如有修改會員、管理員密碼 &#8855;<br/>&#8855; 請重新登入!謝謝! &#8855; </p>
									<p align="center" style="opacity: 50%">
										<a href="/home/logout" >
											<img class="button_margin_1" name="index_logout" id="index_logout" src="<?=$images_root;?>/buttom_logout_1.png">
										</a>
									</p>
								<form/>
							</div>
							<!--Menu跳出小視窗1-->
              <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->
              <?php if(!isset($loginAutobiography) || $loginAutobiography != "sucess"){?>
              <div class="pop_menu" id="autobiography-pw-dialog" style="display: none;">
                <p class="context_regbox">
                  <label class="form-check-label" for="account-input" aria-describedby="inputGroupFileAddon02">自傳閱覽帳號: </label>
                  <input class="form-control account-input" id="account-input" type="text">
                </p>
                <p class="context_regbox">
                  <label class="form-check-label" for="password-input" aria-describedby="inputGroupFileAddon02">自傳閱覽密碼: </label>
                  <input class="form-control password-input" id="password-input" type="password">
                </p>
                <p class="align-center" style="opacity: 50%;">
                  <input  class="button_margin_1 password-submit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
                </p>
              </div>
              <?php } ?>
              <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->
							<!--中間區塊2-2-->
							<td id="regbox" align="center" valign="top" width="15%">
								<table id="pop_menu_content" width="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="top">
									<?=$menuContext;?>
								</table>
								<div id="menuLastArea" style="background-image:url(<?=$images_root;?>/pop_menu_buttom_bg.png);"></div>
							</td>
							<!--中間區塊2-2-->
						</tr>
				</table>	
				</td>
			</tr>
			<!--中間區塊2-->