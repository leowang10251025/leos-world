			<style type="text/css">
				#annouceForm, #resetForm, #closeForm{
					background: transparent;
    				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE8 */ 
    				filter: progid:DXImageTransform.Microsoft.gradient(
        						startColorstr=#00FFFFFF,endColorstr=#00FFFFFF); /* IE6 & 7 */
				}

				#layer1{
					z-index:1;
				}
				#layer2{
					z-index:2;
				}
				
				.context_regbox{
						font-family: Georgia, "Times New Roman", Times, serif;
						font-size: 10pt;
						color: #0000cd;
						line-height: 120%;
						margin-bottom:8px;
						margin-top:8px;
					}
					
					.context_regbox_2{
						font-family: "微軟正黑體";
						font-size: 10pt;
						color: #FF0000;
						line-height: 120%;
						margin-top: 8px;
						margin-bottom: 8px;
					}
					
					.context_regbox_3{
						font-family: Georgia, "Times New Roman", Times, serif;
						font-size: 10pt;
						color: #0000cd;
						line-height: 130%;
						margin-bottom:4px;
						margin-top:4px;
					}
					
					.smalltext7{
						font-size: 13px;
						color: #FF0000;
						font-family: Georgia, "Times New Roman", Times, serif;
						vertical-align: middle;
					}
					
					.heading_1{
						margin-top: 110px;
						padding-left: 25px;
						font-family: "微軟正黑體";
						font-size: 13pt;
						color: #ff3300;
						line-height: 110%;
						font-weight: bold;
					}
					
					.button_margin_1 {
						margin-left: 10px;
						margin-top: 16px;
						margin-bottom: 8px;
					}

					.button_margin_2 {
						margin-left: 20px;
						margin-top: 8px;
						margin-bottom: 16px;
					}

					.button_margin_3 {
						margin-right: 10px;
						margin-top: 5px;
					}

					.player_button1 {
						margin-top: 8px;
						margin-bottom: 8px;
						margin-left: 5px;
						margin-right: 5px;
					}
					
					.player_skin1 {
						margin-top: 4px;
						margin-left: 115px;
						margin-bottom: 8px;
					}
			</style>	

			<!--中間區塊2-->
			<!--中間區塊2-1-->
			<tr align="left">
				<td align="center" width="100%" height="600">
					<table id="board_main" width="100%"  height="100%" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
						<tr valign="middle">
							<td id="mainbox3" valign="top" align="center" width="75%" height="100%" style="background-color: #e6e6e6;">
								<table id="maincontent3" width="800" height="100%" border="0" cellpadding="0" cellspacing="0" align="left"  valign="top">
									<tr  height="700" width="530" valign="top">
										<td class="board_list" valign="top">
											<form id="formPost" name="formPost" action="/post/leaveSomeMsg" method="post">
												<table width="100%" height="300" cellpadding="5" cellspacing="0" border="0" align="center" background="<?=$images_root;?>/msg_content_bg.png" style="background-color: #e6e6e6;background-repeat: no-repeat;background-size: 100% 99%;width: 87%;">
													<tr  valign="top">
														<td align="left" id="board_list_main" style="padding-left: 50px;" >
															<p  id="board_list_icon" align="center" width="80" class="context_regbox_3" style="margin-bottom: 10px;">
																<img name="talk" src="<?=$images_root;?>/talk.png" width="80" height="80" alt="我要留言"/>
																<span class="heading">-留言版-</span>
															</p>
															<p  class="context_regbox_3">
																<span style="color: #ff0000;margin-top: 10px;">* 
																</span>
																<label class="form-check-label" for="board_subject" aria-describedby="inputGroupFileAddon02">標題 :</label> 
																<input id="board_subject" name="board_subject" type="text" class="form-control" style="width: 85%"  placeholder="請輸入留言的主題">
															</p>
															<p  class="context_regbox_3">
																<span style="color: #ff0000;margin-top: 20px;">* 
																</span>
																<label class="form-check-label" for="board_name" aria-describedby="inputGroupFileAddon02">姓名 :</label>  
																<input id="board_name" name="board_name" type="text" placeholder="請輸入留言者的暱稱" class="form-control" style="width: 85%">
															</p>
															<p class="context_regbox_3" name="board_sex_group">
																<span style="color: #ff0000;margin-top: 20px;">* 
																</span>
																<label class="form-check-label" for="board_sex" aria-describedby="inputGroupFileAddon02">姓別 :</label>   
																<b><input type="radio" id="board_sex-1"  name="board_sex" value="M" />男</b>
																<b><input type="radio" id="board_sex-2" name="board_sex" value="F"/>女</b>				
															</p>
															<p  class="context_regbox_3">
																<span style="color: #ff0000;margin-top: 20px;">* 
																</span>
																<label class="form-check-label" for="board_email" aria-describedby="inputGroupFileAddon02">信件地址 :</label>
																<input id="board_email" name="board_email" type="text" placeholder="輸入格式如:xxx@ooo.com"
																class="form-control" style="width: 85%">
															</p>
															<p  class="context_regbox_3">
																<label class="form-check-label" for="board_web" aria-describedby="inputGroupFileAddon02">&nbsp;&nbsp;個人網站 :</label> 
																<input id="board_web" name="board_web" type="text" placeholder="輸入格式如:http://xxxx.org.tw" class="form-control" style="width: 85%">
															</p>
															<p  class="context_regbox_3">
																<label class="form-check-label" for="board_content" aria-describedby="inputGroupFileAddon02">&nbsp;&nbsp;留言內容 :</label> 
																<textarea id="board_content" name="board_content" cols="40" rows="13"></textarea>
															</p>
														</td>
													</tr>
													<?php if(isset($type)){?>
														<input type="hidden" name="type" class="type" value="<?=$type;?>"/>
													<?php }?>
													<!--表單內文填寫區塊-->
													<tr valign="top">	
														<td valign="middle"align="center" colspan="3">
															<input id="action" type="hidden" value="add"/>
															<input class="button_margin_3" style="background-image:url(<?=$images_root;?>/buttom_announce_2.png);width: 140px; height: 40px;border:none;margin-right: 0px;" type="button" id="annouceForm" onclick="checkForm();" value=""/>
															<input class="button_margin_3" style="background-image:url(<?=$images_root;?>/buttom_reset_2.png);width: 140px; height: 40px;border:none;margin-right: 5px;" type="button" id="resetForm" onclick="javascript:document.formPost.reset();" value=""/>
															<input class="button_margin_3" style="background-image:url(<?=$images_root;?>/buttom_backward_1.png);width: 140px; height: 40px;border:none;margin-right: 5px;" type="button" id="closeForm" onclick="javascript:window.history.back();" value=""/>	
														</td>
													</tr><!--表單內文填寫區塊-->
							
												</table>
											</form>
										</td><!--左側填寫內容-->
									</tr>
								</table>
							</td>
							<!--中間區塊2-1-->
							<!--右下方圖案-->
							<td valign="bottom" align="right" width="10%" style="background-color: #e6e6e6;">
								<div id="corner_img_div" style="display: none;">
									<img id="corner_img" src="<?=$images_root;?>/talking.png" style="margin-right: 30px;margin-bottom: 15px;">
								</div>
							</td>
							<!--右下方圖案-->
							<!--Menu跳出小視窗1-->
							<div id="pop_menu_login" class="pop_menu" style="display: none;">
								<!-- 中間區塊2-2的跳出小視窗1  -->
								<p class="heading" align="center">&#9674; &#9830; 登出會員系統 &#9830; &#9674;</p>
								<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
								<p align="center" class="smalltext7">&#8855;  如有修改會員、管理員密碼 &#8855;<br/>&#8855; 請重新登入!謝謝! &#8855; </p>
								<p align="center" style="opacity: 50%">
									<a href="/home/logout" >
										<img class="button_margin_1" name="index_logout" id="index_logout" src="<?=$images_root;?>/buttom_logout_1.png">
									</a>
								</p>
							</div>
							<!--Menu跳出小視窗1-->
							<!--Menu跳出小視窗2-->
							<div id="pop_menu_loginBoardAdmin" style="display: none;">
							<!-- 中間區塊2-2的跳出小視窗2(未登入)  -->
							<!--中間區塊2-2-->
								<form name="formAdminLogin" id="formAdminLogin" method="post" action="/message_board/adminLogin" onSubmit="return checkSubForm();">
									<p class="heading_1" align="center" style="padding: 0px;margin-top: 10px;"> &#9830; &#9674; 留言板管理系統 &#9674; &#9830; </p>
									<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
									<p class="context_regbox"  style="text-align: left;">
										<label class="form-check-label" for="username" aria-describedby="inputGouprFileAddon02">管理帳號: </label>
										<input name="username" id="username" type="text" class="form-control"/>
									</p>
									<p class="context_regbox"  style="text-align: left;">
										<label class="form-check-label" for="passwd" aria-describedby="inputGouprFileAddon02">管理密碼: </label>
										<input name="passwd" id="passwd" type="password" class="form-control"/>
									</p>
									<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />										
									<input type="hidden" id="chkLoginName" class="chkLoginName" value=""/>
									<input type="hidden" id="chkLoginPw" class="chkLoginPw" value=""/>
									<p align="right" style="padding: 0px;margin: 0px;">
										<input  class="button_margin_1" id="loginSubmit" src="<?=$images_root;?>/buttom_login_1.png" type="image" />

										<a href="javascript:window.history.back()" bgcolor="#d8bfd8"><img class="button_margin_2" src="<?=$images_root;?>/buttom_backward_1.png"></a>
									</p>
									<img id="board_key" src="<?=$images_root;?>/navi_content_key.png">
								</form>
							</div>
							<!--Menu跳出小視窗2-->
              <!--Menu跳出小視窗3(經歷與自傳:輸入密碼)-->
              <?php if(!isset($loginAutobiography) || $loginAutobiography != "sucess"){?>
              <div class="pop_menu" id="autobiography-pw-dialog" style="display: none;">
                <p class="context_regbox">
                  <label class="form-check-label" for="account-input" aria-describedby="inputGroupFileAddon02">自傳閱覽帳號: </label>
                  <input class="form-control account-input" id="account-input" type="text">
                </p>
                <p class="context_regbox">
                  <label class="form-check-label" for="password-input" aria-describedby="inputGroupFileAddon02">自傳閱覽密碼: </label>
                  <input class="form-control password-input" id="password-input" type="password">
                </p>
                <p class="align-center" style="opacity: 50%;">
                  <input  class="button_margin_1 password-submit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
                </p>
              </div>
              <?php }?>
              <!--Menu跳出小視窗3(經歷與自傳:輸入密碼)-->
							<!--中間區塊2-2-->
							<td id="regbox" align="center" valign="top" width="15%">
								<table id="pop_menu_content" width="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="top">
									<?=$menuContext;?>
								</table>
								<div id="menuLastArea" style="background-image:url(<?=$images_root;?>/pop_menu_buttom_bg.png);"></div>
							</td>
							<!--中間區塊2-2-->
						</tr>
				</table>	
				</td>
			</tr>
			<!--中間區塊2-->

<script>
	//Ckeditor

	var this_ckeditor;

	function createEditor() {
	    if (ckeditor != null) {
	        if (this_ckeditor) {
	            this_ckeditor.destroy();
	        }
	    }
	}

	CKEDITOR.env.isCompatible = true;
	this_ckeditor = CKEDITOR.replace('board_content', {
		width: '85%',
		height: 160,
		resize_enabled: false,
		enterMode: 2,
		forcePasteAsPlainText: true,
		toolbar: [
			['Maximize', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
			['TextColor', 'BGColor', '-', 'NumberedList', 'BulletedList', ],
			'/', ['Outdent', 'Indent', 'Iineheight'],
			['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
			['Link', 'Unlink', 'Anchor'],
			['Image', 'Flash', 'Table', 'HorizontalRule', 'SpecialChar', 'PageBreak'],
			['Format', 'Font', 'FontSize']
			]
	});
</script>