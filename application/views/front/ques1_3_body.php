			<style type="text/css">
			<!--
				#exp_content{
					cursor: help;
				}
				
				#light_button:hover{
					cursor: pointer;
				}
				
				#point1_3_1:hover,#point1_3_2:hover,#point1_3_3:hover{
					cursor: crosshair;
				}
				
				.point{
					width:30%;
					font-family: "微軟正黑體";
					font-size: 14pt;
					color: #FF3300;
					text-align: center;
					font-style:italic;
					font-weight: bolder;
				}
				
				.point_content{
					font-family: "微軟正黑體";
					font-size: 11pt;
					color: #242376;
					font-weight: normal;
				}
				
				#point1_3_1{
					margin-top: 227px;
					margin-left: 4px;
				}
				
				#point1_3_2{
					margin-top: 248px;
					margin-left: 10px;
				}
				
				#point1_3_3{
					margin-top: 248px;
					margin-left: 15px;
				}
				
				#point1_3_1_content{
					margin-top: 5px;
					margin-left: 20px;
					margin-right: 10px;
				}
				
				#point1_3_2_content{
					margin-top: 5px;
					margin-left: 27px;
					margin-right: 15px;
				}
				
				#point1_3_3_content{
					margin-top: 5px;
					margin-left: 30px;
					margin-right: 13px;
				}
				
				.photo{
					cursor:auto;
					padding-top:45px;
					background-attachment:scroll;
					background-position:center,center;
					background-repeat:no-repeat;
				}
				
				.player_button1 {
						margin-top: 8px;
						margin-bottom: 8px;
						margin-left: 5px;
						margin-right: 5px;
				}
					
				.player_skin1 {
					margin-top: 4px;
					margin-left: 115px;
					margin-bottom: 8px;
				}
			-->
			</style>

			<!--中間區段2-->
			<tr align="left">
				<td align="center" width="100%" height="600">
					<table id="board_main" width="100%"  height="100%" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
						<tr valign="middle">
							<td valign="top" align="center" width="75%" height="100%" style="background-color: #ffffff;">
								<table id="maincontent4" width="1000" height="100%" border="0" cellpadding="0" cellspacing="0" align="left"  valign="top" >
									<tr height="100%" width="100%">
										<td>
											<div id="mainRegion4">
												<form name="formAns" method="post" action="">
												<table width="93%" height="100%" border="0" align="center" cellpadding="0" cellspacing="1"> 													
													<tr valign="top" align="center">
														<td colspan="2" class="heading">【各頁尾下方功能說明】</td>
													</tr>
													<tr valign="top">
														<td background="<?=$images_root;?>/ques1-3.png" width="900" height="450" class="photo">
															<table width="93%" height="100%" border="0" cellpadding="0" cellspacing="0" align="center"  valign="top" >
																<tr width="93%" height="100%" >
																	<td width="32%"  align="left" valign="top">
																		<p class="point" id="point1_3_1">地理資訊:</p>
																		<div class="point_content" id="point1_3_1_content">
																			<em>
																			主要可以顯示你的設備所處的地理資訊:如經、緯度等。其顯示內容的左下方有個Google Map顯示的按鈕。可以更清楚顯示你所在位置的電子地圖圖示內容。
																			</em>
																		</div>	
																	</td>
																	<td width="32%" align="left" valign="top">
																		<p class="point" id="point1_3_2">文字時鐘:</p>
																		<div class="point_content" id="point1_3_2_content">
																			<em>
																			此區域內容主要可顯示現在時間與日期資訊。日期預設為西元顯示方式(年/月/日)、時間(點/分/秒)。時間如
																			要更新則可按其下方文字內容更新(或按鍵盤上"F5"再次更新)。
																			</em>
																		</div>	
																	</td>
																	<td width="32%" align="left" valign="top">
																		<p class="point" id="point1_3_3">背景音樂:</p>
																		<div class="point_content" id="point1_3_3_content">
																			<em>
																			背景音樂播放時，會顯示其控制面盤。可以使其暫停或繼續播放。如果喜歡此背景音樂也可以面板最右方下
																			載鈕下載。
																			</em>
																		</div>
																	</td>
																</tr>	
															</table>	
														</td>
													</tr>
													<tr valign="middle" align="right">
														<td >
															<p align="left">
																<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;margin-top: 60px;" />
																<table width="100%" height="100%">
																	<tr align="center">
																		<td align="left" valign="middle">
																			<p align="left" valign="middle">
																				<strong>
																				※當你使用每個頁面時，下方頁尾有三個主要功能，你分別可使用<a id="exp_content" href="#point1_3_1">地理資訊</a>，
																				內容判斷你所在的位置，<br/>&nbsp;&nbsp;如果你需要現在日期、時間，可以查看<a id="exp_content" href="#point1_3_2">文字時鐘</a>
																				的內容得知。
																				</strong>
																			</p>
																			<p align="left" valign="middle">
																				<strong>※對了，每個頁面有其不同的<a id="exp_content" href="#point1_3_3">背景音樂</a>播放，如果你想暫停或下載音樂，請從面板上去操控瞜!</strong>
																			</p>	
																		</td>
																		<td align="right" valign="middle">
																			<!--a href="javascript:window.history.back()"><img id="light_button" src="<?=$images_root;?>/Light-1.png"/></a-->
																			<input type="image" id="light_button1" src="<?=$images_root;?>/Light-1.png" value="solve" onClick="return checkForm(this.id);"/>
																			<input type="image" id="light_button2" src="<?=$images_root;?>/Light-2.png" value="issue" onClick="return checkForm(this.id);"/>
																			<input type="image" id="reply_button1" src="<?=$images_root;?>/ques_reply.png" value="reply" onClick="return checkForm(this.id);"/>
																		</td>
																	</tr>
																</table>
															</p>	
														</td>
													</tr>
												</table>
											</form>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<!--右下方圖案-->
							<td valign="bottom" align="right" width="10%" style="background-color: #ffffff;">
								<div id="corner_img_div" style="display: none;">
									<img id="corner_img" src="<?=$images_root;?>/talking.png" style="margin-right: 30px;margin-bottom: 15px;">
								</div>
							</td>
							<!--右下方圖案-->
							<!--Menu跳出小視窗1-->
							<div id="pop_menu_login" class="pop_menu" style="display: none;">
								<!-- 中間區塊2-2的跳出小視窗1  -->
								<form name="formReg" id="formReg" method="post" >
									<p class="heading" align="center">&#9674; &#9830; 登出會員系統 &#9830; &#9674;</p>
									<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
									<p align="center" class="smalltext7">&#8855;  如有修改會員、管理員密碼 &#8855;<br/>&#8855; 請重新登入!謝謝! &#8855; </p>
									<p align="center" style="opacity: 50%">
										<a href="/home/logout" >
											<img class="button_margin_1" name="index_logout" id="index_logout" src="<?=$images_root;?>/buttom_logout_1.png">
										</a>
									</p>
								<form/>
							</div>
							<!--Menu跳出小視窗1-->
              <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->
              <?php if(!isset($loginAutobiography) || $loginAutobiography != "sucess"){?>
              <div class="pop_menu" id="autobiography-pw-dialog" style="display: none;">
                <p class="context_regbox">
                  <label class="form-check-label" for="account-input" aria-describedby="inputGroupFileAddon02">自傳閱覽帳號: </label>
                  <input class="form-control account-input" id="account-input" type="text">
                </p>
                <p class="context_regbox">
                  <label class="form-check-label" for="password-input" aria-describedby="inputGroupFileAddon02">自傳閱覽密碼: </label>
                  <input class="form-control password-input" id="password-input" type="password">
                </p>
                <p class="align-center" style="opacity: 50%;">
                  <input  class="button_margin_1 password-submit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
                </p>
              </div>
              <?php }?>
              <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->
							<!--中間區塊2-2-->
							<td id="regbox" align="center" valign="top" width="15%">
								<table id="pop_menu_content" width="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="top">
									<?=$menuContext;?>
								</table>
								<div id="menuLastArea" style="background-image:url(<?=$images_root;?>/pop_menu_buttom_bg.png);"></div>
							</td>
							<!--中間區塊2-2-->
						</tr>
				</table>	
				</td>
			</tr>
			<!--中間區塊2-->