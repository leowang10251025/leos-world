<!DOCTYPE>
<html lang="zh-tw">
	<head>
		<meta http-equiv="Content" Content-Type="text/html; charset=utf-8">
		<link rel="stylesheet" href="<?=$css_root;?>/front/style_1.css" type="text/css">
		<link rel="stylesheet" href="<?=$js_root;?>/lib/bootstrap-sweetalert-master/dist/sweetalert.css" >
		<title>Leo's World的入口處</title>
		<script src="<?=$js_root;?>/lib/jquery-3.3.1.min.js"></script> 
		<script src="<?=$js_root;?>/lib/bootstrap-sweetalert-master/dist/sweetalert.min.js"></script>
		<script src="<?=$js_root;?>/mapFunc.js"></script>
		<script src="<?=$js_root;?>/timer.js"></script>
<!-- 		<script src="<?=$js_root;?>/player.js"></script> -->
		<script src="<?=$js_root;?>/front/index.js"></script>
		<script type="text/javascript">

			var path="";
			var max_item=4;
			var min_item=0;
			var item=Math.floor((Math.random()*(max_item-min_item))+min_item);
			var item_arr=new Array("If_I_Had_a_Chicken.mp3","Moonlight_Haze.mp3","Neck_Pillow.mp3",
				"Whistling_Down_the_Road.mp3");
			var song_name=item_arr[item];
			path="<?=$host_root;?>"+"/audio/"+song_name;

			$(function(){
				$('#audio_info').html("※現在播放的是:"+song_name);
				$('#open_broswer').attr('href',path);
			});
			
			var player=new Audio(path);
			player.loop=true;

			function media_play(){
				player.play();
			}

			function media_pause(){
				player.pause();
			}

			function setStop(){
				player.pause();
				player.currentTime=0;
			}


		</script>
	</head>
	<body bgcolor="#d8bfd8">
	<style type="text/css">
		<!--
			.button_margin_1 {
				margin-top: 16px;
				margin-bottom: 8px;
			}

			 .button_margin_2 {
				margin-top: 8px;
				margin-bottom: 16px;
			}
			
			.context_regbox{
				font-family: Georgia, "Times New Roman", Times, serif;
				font-size: 10pt;
				color: #0000cd;
				line-height: 120%;
			}
			
			.context_regbox_2{
				font-family: "微軟正黑體";
				font-size: 10pt;
				color: #FF0000;
				line-height: 120%;
				margin-top: 8px;
				margin-bottom: 8px;
			}
			
			.smalltext7{
				font-size: 13px;
				color: #FF0000;
				font-family: Georgia, "Times New Roman", Times, serif;
				vertical-align: middle;
			}
			
			.player_button1 {
				margin-top: 8px;
				margin-bottom: 8px;
				margin-left: 5px;
				margin-right: 5px;
			}
			
			.player_skin1 {
				margin-top: 4px;
				margin-left: 115px;
				margin-bottom: 8px;
			}
		-->
	</style>	
		<table width="80%" height="100%" border="0"  cellpadding="0" cellspacing="0"  align="center" class="mainline" bgcolor="#ff4500">
		<!--中間區段1-->	
			<!--中間區段1-1-->
			<tr>
				<td  align="right" valign="top" height="104" width="1085" background="<?=$images_root;?>/banner_2_1.png" class="bannerlinker | bannerline1">
					<a href="javascript:checkTopLeftTitleState();">需要協助 </a>|<a href="javascript:checkTopLeftTitleState();"> 聯絡我們</a>
				</td>
			</tr>
			<!--中間區段1-1-->
			<!--中間區段1-2-->
			<tr>
				<td>
					<table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td  width="93">
								<img name="banner_2_2_1" src="<?=$images_root;?>/banner_2_2_1.png" width="93" height="98" border="0" alt="">
							</td>
							<td  width="95">
									<input type="button" id="banner_2_2_2" style="background-image:url(<?=$images_root;?>/banner_2_2_2.png);width: 95px; height: 98px;border:none;"></input>	
							</td>
							<td  width="94">
								<input type="button" id="banner_2_2_3" style="background-image:url(<?=$images_root;?>/banner_2_2_3.png);width: 94px; height: 98px;border:none;"></input>
	
							</td>
							<td  width="89">
								<input type="button" id="banner_2_2_4" style="background-image:url(<?=$images_root;?>/banner_2_2_4.png);width: 89px; height: 98px;border:none;"></input>
							</td>
							<td  width="113">
								<input type="button" id="banner_2_2_5" style="background-image:url(<?=$images_root;?>/banner_2_2_5.png);width: 113px; height: 98px;border:none;"></input>	
							</td>
							<td  width="600">
								<input type="button" id="banner_2_3" style="background-image:url(<?=$images_root;?>/banner_2_3.png);width: 600px; height: 98px;border:none;"></input>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!--中間區段1-2-->
		<!--中間區段1-->	
		<!--中間區段2-->
				<!--中間區塊2-1-->
				<tr align="left">
					<td align="center" width="100%" height="600">
						<table  class="home_main" width="100%"  height="100%" align="center" valign="middle" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td id="mainbox" valign="top" align="center" width="78%" background="<?=$images_root;?>/main_content3_1.png">
									<table id="maincontent1" width="825" height="544" border="0" align="center">
										<tr height="100%" width="100%" border="0"  valign="top">
											<td background="/images/">
												<p  valign="top" align="center">
													<marquee id="title_marquee1" class="marquee" behavior="alternate" width="90%" scrolldelay="150" direction="right">
														<?=$title;?>
													</marquee>
												</p>
												<p class="heading0">
													<?=$sub_title;?>
												</p>
												<?=$content;?>
											</td><!--左側填寫內容-->
										</tr>
									</table>
								</td>
				<!--中間區塊2-1-->
				<!--中間區塊2-2-->
						<td id="regbox" align="center" valign="top" width="22%" background="<?=$images_root;?>/navi_content_bg.png">
								<table id="regboxContent" width="200" height="549" border="0" cellpadding="0" cellspacing="0" align="left" valign="top">
									<tr width="100%" height="100%" valign="top" align="left">
										<td>
										<!-- 中間區塊2-2的頁面呈現處理(表單部分)  -->
											<form name="formReg" id="formReg" method="post" action="/home/login" onSubmit="return checkSubForm();">
												<p class="heading" align="center">&#9830; &#9674; 登入會員系統 &#9674; &#9830; </p>
												<hr width="85%"/>
												<p class="context_regbox">
													帳號: <input name="loginName" id="loginName" type="text" value="<?php if(isset($cookie_name)){echo $cookie_name;}else{echo '';} ?>">
												</p>
												<p class="context_regbox">
													密碼: <input name="loginPW" id="loginPW" type="password" value="<?php if(isset($cookie_pw)){echo $cookie_pw;}else{echo '';} ?>">
												</p>
												<p class="context_regbox">
													<input name="loginRem" id="loginRem" type="checkbox">
													<input name="loginRemChk" id="loginRemChk" type="hidden" value="<?php if(isset($loginRemChk)){echo $loginRemChk;}else{echo 'false';} ?>">
													記住我的帳號與密碼
												</p>
												<p align="center">
													<input  class="button_margin_1" name="loginSubmit" id="loginSubmit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
												</p>
												<p align="center" class="smalltext ">
													<a href="/admin_passmail" >忘記密碼，補寄密碼信。</a>
												</p>
												<hr width="85%"/>
												<p class="context_regbox_2">還沒有會員帳號?</p>
												<p class="context_regbox_2">註冊帳號免費又容易!</p>
												<p align="right" class="smalltext7 ">
													<a href="/member_join" >&#9001;&#9001; 馬上申請會員!! &#9002;&#9002;</a>
												</p>
											</form>
										<!-- 中間區塊2-2的頁面呈現處理(表單部分)  -->
											<?php if(isset($errMsg)){?>
												<input type="hidden" id="errMsg" class="errMsg" value="<?=$errMsg;?>"/>
											<?php 
												#中間區塊2-2的頁面呈現處理(表單部分) 
											}?>
										</td>
									</tr>
								</table>
							</td>
							<!--中間區塊2-2-->
						</tr>
				</table>
				</td>
			</tr>
		<!--中間區塊2-->
		<!--中間區塊3-->	
			<!--中間區塊3-1-->
			<tr>
				<td class="smalltext" align="center" width="100%" height="240" background="<?=$images_root;?>/shadow_1.png">
					<table  width="890" height="240" border="0" cellpadding="0" cellspacing="0" align="right" valign="bottom">
						<tr width="100%" height="80%" valign="middle" align="center" border="0" >
							<td width="60%" valign="top" >
								<p class="footer_p1"><strong>【目前裝置所處位置資訊】</strong></p><br/>
								<hr size="1" width="90%" color="#c8b3cc"/>
								<div id="geo_result" align="left" height="80">不顯示現在裝置所處位置!</div>
							</td>
							<td width="40%" valign="top" align="center">
								<form id="formDate" action="" method="post">
									<p class="footer_p1_1">
										<strong>【目前日期為&nbsp;:&nbsp;<?=$date_now;?>】<br/>
												【目前時間為&nbsp;:&nbsp;
												<q id="showTime">請按下方文字</q>】
										</strong>
									</p>
									<p>
										<u><a class="footer_p5" onclick="javascript:setTime();">※如需更新時間請按我&nbsp;&nbsp;/&nbsp;&nbsp;日期需更新，請F5重新整理</a></u>
									</p>
									<hr size="1" width="90%" color="#c8b3cc"/>
										<p id="audio_info" class="footer_p3" width="60" align="center">※準備播放的是:&nbsp;&nbsp;</p><br/>
											<table class="player_skin1" width="130" height="45" border="0" cellpadding="0" cellspacing="0" align="left" valign="middle">
												<tr align="center" valign="middle">
													<td  background="<?=$images_root;?>/player_skin.png">
														<img class="player_button1" src="<?=$images_root;?>/play_buttom.png" value="play" onclick="media_play();"/>
														<img class="player_button1" src="<?=$images_root;?>/pause_buttom.png" value="pause" onclick="media_pause();"/>
														<img class="player_button1" src="<?=$images_root;?>/stop_buttom.png" value="stop" onclick="setStop();"/>
													</td>
												</tr>
											</table>	
										<p class="footer_p3">你所使用的瀏覽器無法撥放檔案，請<a id="open_broswer" href="">點擊</a></p>
								</form>	
							</td>
						</tr>
			<!--中間區塊3-1-->
		<!--中間區塊3-->
		<!--中間區塊4-->	
			<!--中間區塊4-1-->			
						<tr width="100%" height="20%" valign="middle" align="center" border="1" >
							<td width="60%" valign="top" >
								<table   id="geo" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="bottom">
										<tr>
											<td valign="left"> 
													<form id="formMap" action="" method="get" onSubmit="return getMapURL();">
													<span id="geo_map">
															<a href="javascript:getMapURL();" id="geo_button"/><img src="<?=$images_root;?>/google_map.png">&#160;&#8776;&#8658;&#160;&#8220;精確位置鈕&#8221;&#160;&#8656;&#8776;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</a>
															<a href="/pet/index" id="geo_button"/><img src="<?=$images_root;?>/my_favorest_pet.png"/>&#160;&#8776;&#8658;&#160;&#8220;My favorest pet鈕&#8221;&#160;&#8656;&#8776;&#160;</a>		
													</span>
													</form>
											</td>
										</tr>	
								</table>
							</td>
							<td width="40%" valign="top" align="left">
								<p class="footer_p4" >※聆聽背景音樂時請注意音量，</br>以免損及聽力</p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!--中間區塊4-1-->
		<!--中間區塊4-->
		<!--中間區塊5-->		
			<!--中間區塊5-1-->	
			<tr align="center" valign="bottom" >
				<td class="footerline1 | trademark" width="100%" height="100"  background="<?=$images_root;?>/footer_1.png"  >
						<hr size="2" width="100%" color="#476c6c" />
						<p>
							Edited By <ins><a href="https://www.facebook.com/leowang10251025">Wang How-Wei(Leo)/網站版本: v0.02c</a></ins>
							&nbsp;&nbsp;聯絡方式&nbsp;:&nbsp;<ins><a href="mailto:leowang1025rd@gmail.com?
							subject=你好，我是瀏覽者，想與你連絡...&cc=leowang10251025@gmail.com&
							body=To Leo:%0d%0a你好，我想請問...%0d%0a%0d%0a%0d%0a連絡方式:%0d%0a
							(你的大名:請留下你的暱稱或名字，以方便與你連絡)/%0d%0a(你的連絡信箱:格式如xxx.oo@gmail.com)。">leowang1025rd@gmail.com</a></ins>
							&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;最佳解析度:1280x768&nbsp;&nbsp;建議使用瀏覽器種類:
							<a href="http://www.google.com.tw/chrome/browser">Chrome</a>
						</p>
						<p class="trademark_bottom">
							&#171;&nbsp;此版面僅提供技術交流使用，請注重智慧財產權，所有商標的權利為原創者所有。&nbsp;&#187;&nbsp;&nbsp;&nbsp; © 2016 All Rights Reserved.
						</p>
				</td>
			</tr>
			<!--中間區塊5-1-->
		<!--中間區塊5-->
		</table>
	</body>
</html>