			<style type="text/css">
			<!--
				#exp_content{
					cursor: help;
				}
				
				#light_button:hover{
					cursor: pointer;
				}
				
				#point2_1:hover,#point2_2:hover{
					cursor: crosshair;
				}
				
				.point{
					width:20%;
					font-family: "微軟正黑體";
					font-size: 14pt;
					color: #FF3300;
					text-align: center;
					font-style:italic;
					font-weight: bolder;
				}
				
				.point_content{
					font-family: "微軟正黑體";
					font-size: 11pt;
					color: #242376;
					font-weight: normal;
				}
				
				#point2_4_1{
					margin-top: 10px;
					margin-left: 570px;
				}

				#point2_4_1_box{
					margin-top:-5px;
					margin-left: 655px;			
				}				
				
				#point2_4_2{
					margin-top: 133px;
					margin-left: 570px;
				}

				#point2_4_2_box{
					margin-top:-25px;
					margin-left: 655px;
				}
				
				#point2_4_1_content{
					margin-top: 100px;
				}
				
				#point2_4_2_content{
					margin-left: 660px;
				}
				
				.photo{
					cursor:auto;
					padding-top:45px;
					background-attachment:scroll;
					background-position:center,middle;
					background-repeat:no-repeat;
				}
				
				#icon_normal{
					background-position:center,middle;
					background-repeat:no-repeat;
				}
				
				.player_button1 {
						margin-top: 8px;
						margin-bottom: 8px;
						margin-left: 5px;
						margin-right: 5px;
					}
					
				.player_skin1 {
					margin-top: 4px;
					margin-left: 115px;
					margin-bottom: 8px;
				}
			-->
			</style>

			<!--中間區段2-->
			<tr align="left">
				<td align="center" width="100%" height="600">
					<table id="board_main" width="100%"  height="100%" align="center" valign="top" cellspacing="0" cellpadding="0" border="0">
						<tr valign="middle">
							<td valign="top" align="center" width="75%" height="100%" style="background-color: #ffffff;">;
								<table id="maincontent4" width="1000" height="100%" border="0" cellpadding="0" cellspacing="0" align="left"  valign="top" >
									<tr height="100%" width="100%">
										<td>
											<div id="mainRegion4">
												<form name="formAns" method="post" action="">
												<table width="93%" height="100%"  align="center" cellpadding="0" cellspacing="1"> 													
													<tr valign="top" align="center">
														<td colspan="2" class="heading">【常見瀏覽器當機的狀況】</td>
													</tr>
													<tr valign="top">
														<td background="<?=$images_root;?>/ques2-4.png" width="900" height="450" class="photo">
															<p class="point" id="point2_4_1">狀況一 : </p>
															<div style="width: 220px;height: 60px;" id="point2_4_1_box">
																<b class="point_content" id="point2_4_1_content">
																	最常見原因不外乎幾個:記憶體空間不足、防毒或垃圾軟體封鎖或限制導致當機。發生第一種狀況時，一般會先釋放記憶體空間，也就是關閉其他分頁以及瀏覽器擴充功能或其他軟體。如果是第二種狀況，請檢視你裝備的防毒軟體是否封鎖你要瀏覽的網頁，或特定軟體導致網頁封鎖。
																</b>
															</div>
															<p class="point" id="point2_4_2">狀況二 : </p>
															<div style="width: 220px;height: 60px;" id="point2_4_2_box">
																<b class="point_content" id="point2_4_2_content">
																	原因大致如狀況一，是以對話框形式表示，當網頁分頁過多或載入項目較多，就有可能導致停止或當機。對話框下方有兩個選項，可以中止網頁繼續存取，或是等待它並繼續載入。不過繼續載入也可能導致瀏覽器當機停止。解決方式同上。
																</b>
															</div>
														</td>
													</tr>
													<tr valign="middle" align="right">
														<td >
															<p align="left">
																<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;margin-top: 60px;" />
																<table width="100%" height="100%">
																	<tr align="center">
																		<td align="left" valign="middle">
																			<p align="left" valign="middle">
																				<strong>
																					※上面所列的當機狀況，無論以<a id="exp_content" href="#point2_4_1">"錯誤訊息內容"</a>或是<a id="exp_content" href="#point2_4_2">"提示對話框"</a>顯示告知使用者，其實都需先<br/>
																					 &nbsp;&nbsp;排除上述可能導致的原因，並且"重新開啟"瀏覽器或"重新啟動"電腦或行動裝備才能回歸正常<br/> 
																					&nbsp;&nbsp;網頁功能。再更進一步如果還是無法解決，請將瀏覽器重設成"預設"狀態或重新安裝。<br/>
																				</strong>
																			</p>
																		</td>
																		<td align="right" valign="middle">
																			<!--a href="javascript:window.history.back()"><img id="light_button" src="<?=$images_root;?>/Light-1.png"/></a-->
																			<input type="image" id="light_button1" src="<?=$images_root;?>/Light-1.png" value="solve" onClick="return checkForm(this.id);"/>
																			<input type="image" id="light_button2" src="<?=$images_root;?>/Light-2.png" value="issue" onClick="return checkForm(this.id);"/>
																			<input type="image" id="reply_button1" src="<?=$images_root;?>/ques_reply.png" value="reply" onClick="return checkForm(this.id);"/>
																		</td>
																	</tr>
																</table>
															</p>	
														</td>
													</tr>
												</table>
											</form>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<!--右下方圖案-->
							<td valign="bottom" align="right" width="10%" style="background-color: #ffffff;">
								<div id="corner_img_div" style="display: none;">
									<img id="corner_img" src="<?=$images_root;?>/talking.png" style="margin-right: 30px;margin-bottom: 15px;">
								</div>
							</td>
							<!--右下方圖案-->
							<!--Menu跳出小視窗1-->
							<div id="pop_menu_login" class="pop_menu" style="display: none;">
								<!-- 中間區塊2-2的跳出小視窗1  -->
								<form name="formReg" id="formReg" method="post" >
									<p class="heading" align="center">&#9674; &#9830; 登出會員系統 &#9830; &#9674;</p>
									<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
									<p align="center" class="smalltext7">&#8855;  如有修改會員、管理員密碼 &#8855;<br/>&#8855; 請重新登入!謝謝! &#8855; </p>
									<p align="center" style="opacity: 50%">
										<a href="/home/logout" >
											<img class="button_margin_1" name="index_logout" id="index_logout" src="<?=$images_root;?>/buttom_logout_1.png">
										</a>
									</p>
								<form/>
							</div>
							<!--Menu跳出小視窗1-->
              <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->
              <?php if(!isset($loginAutobiography) || $loginAutobiography != "sucess"){?>
              <div class="pop_menu" id="autobiography-pw-dialog" style="display: none;">
                <p class="context_regbox">
                  <label class="form-check-label" for="account-input" aria-describedby="inputGroupFileAddon02">自傳閱覽帳號: </label>
                  <input class="form-control account-input" id="account-input" type="text">
                </p>
                <p class="context_regbox">
                  <label class="form-check-label" for="password-input" aria-describedby="inputGroupFileAddon02">自傳閱覽密碼: </label>
                  <input class="form-control password-input" id="password-input" type="password">
                </p>
                <p class="align-center" style="opacity: 50%;">
                  <input  class="button_margin_1 password-submit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
                </p>
              </div>
              <?php } ?>
              <!--Menu跳出小視窗2(經歷與自傳:輸入密碼)-->
							<!--中間區塊2-2-->
							<td id="regbox" align="center" valign="top" width="15%">
								<table id="pop_menu_content" width="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="top">
									<?=$menuContext;?>
								</table>
								<div id="menuLastArea" style="background-image:url(<?=$images_root;?>/pop_menu_buttom_bg.png);"></div>
							</td>
							<!--中間區塊2-2-->							
						</tr>
				</table>	
				</td>
			</tr>
			<!--中間區塊2-->