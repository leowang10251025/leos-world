			<!--中間區段2-->
			<!--中間區段2-1-->
			<tr align="left">
				<td align="center" width="100%" height="600">
					<table  class="home_main" width="100%"  height="100%" align="center" valign="middle" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td id="mainbox" valign="top" align="center" width="75%" style="background-color: #ffffff;">
								<table id="maincontent1" width="825" height="544" border="0" align="center">
									<tr  height="100%" width="100%" border="0"  valign="top">
										<td>
											<form id="formUpdate" name="formUpdate" action="/member_center_update/updateCenterInfo/<?=$result_row["m_id"]?>" method="post">
											<table width="85%" border="0" cellpadding="10" cellspacing="0" align="center" >
												<tr>
													<td>
														<p class="heading">會員帳號資料</p>
														<p class="context_regbox">
															<label class="form-check-label font-weight-bold my-2" for="input_username" aria-describedby="inputGroupFileAddon02">
																使用帳號：
															</label>
															<?php echo $result_row["m_username"];?></p>
														<p class="context_regbox" id="original_password_p">
															<label class="form-check-label font-weight-bold my-2" for="original_password" aria-describedby="inputGroupFileAddon02">
																請輸入原密碼：
															</label>
															<input id="original_password" name="original_password" class="form-control" type="password"/>
														</p>
														<p class="context_regbox" id="input_password_p">
															<label class="form-check-label font-weight-bold my-2" for="input_password" aria-describedby="inputGroupFileAddon02">
																請輸入新密碼：
															</label>
															<input id="input_password" name="input_password" class="form-control" type="password"/>
														</p>
														<p class="context_regbox" id="input_confirmPW_p">
															<label class="form-check-label font-weight-bold my-2" for="input_confirmPW" aria-describedby="inputGroupFileAddon02">
																請再次確認密碼：
															</label>
															<input id="input_confirmPW" name="input_confirmPW" class="form-control" type="password"/>
														</p>
														<div class="ml-2 mb-1 text-secondary">
															<ul>
																<li>若不修改密碼，請不要填寫</li>
																<li>若要修改，請輸入密碼二次</li>
																<li>若修改密碼，系統會自動登出，請用新密碼登入</li>
															</ul>
														</div>
														<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
														<p class="heading">會員個人資料</p>
														<p class="context_regbox">
															<label class="form-check-label font-weight-bold my-2" for="input_name"  aria-describedby="inputGroupFileAddon02">
																<font color="#ff0000">* </font>真實姓名：
															</label>
															<input id="input_name" name="input_name" type="text" class="form-control" value="<?php echo trim($result_row["m_name"]); ?>"/>
														</p>
														<p class="context_regbox">
															<label class="form-check-label font-weight-bold my-2" for="input_sex" aria-describedby="inputGroupFileAddon02">
																<font color="#ff0000">* </font>性　　別：
															</label>
															<input name="input_sex" type="radio" value="M"
															<?php if($result_row["m_sex"]=="M") echo "checked"; ?>>男&nbsp;
															<input  name="input_sex" type="radio" value="F" 
															<?php if($result_row["m_sex"]=="F") echo "checked";?>>女
														</p>
														<p class="context_regbox">
															<label class="form-check-label font-weight-bold my-2" for="input_birthday" aria-describedby="inputGroupFileAddon02">
																<font color="#ff0000">* </font>生　　日：
															</label>
														    <input type="date" class="form-control" data-format="yyyy/MM/dd" id="input_birthday" name="input_birthday" value="<?php echo trim($result_row["m_birthday"]); ?>">
                              <div class="ml-4 mb-1 text-secondary">
                                <small>為西元格式(YYYY-MM-DD)。</small>
                              </div>
														</p>
														<p class="context_regbox">
															<label class="form-check-label font-weight-bold my-2" for="input_email"  aria-describedby="inputGroupFileAddon02">
																<font color="#ff0000">* </font>電子郵件：
															</label>
															<input id="input_email" name="input_email" type="text" class="form-control" value="<?php echo trim($result_row["m_email"]); ?>"/>
                              <div class="ml-4 mb-1 text-secondary">
                                <small>
                                  請確定此電子郵件為可使用狀態，以方便未來系統使用，如補寄會員密碼信。
                                </small>
                              </div>
														</p>
														<p class="context_regbox">
															<label class="form-check-label font-weight-bold my-2" for="input_url" aria-describedby="inputGroupFileAddon02">
																個人網頁：
															</label>
															<input id="input_url" name="input_url" type="text" class="form-control" value="<?php echo trim($result_row["m_url"]); ?>"/>
															<div class="ml-4 mb-1 text-secondary">
                                <small>請以「https://」 為開頭。</small>
                              </div>
														</p>
														<p class="context_regbox">
															<label class="form-check-label font-weight-bold my-2" for="input_phone" aria-describedby="inputGroupFileAddon02">
																電　　話：
															</label>
															<input id="input_phone" name="input_phone" type="text" class="form-control" value="<?php echo trim($result_row["m_phone"]); ?>" />
														</p>
														<p class="context_regbox">
															<label class="form-check-label font-weight-bold my-2" for="input_addr" aria-describedby="inputGroupFileAddon02">
																地　　址：
															</label>
															<input id="input_addr" name="input_addr" type="text" class="form-control" size="40" value="<?php echo trim($result_row["m_address"]); ?>"/><br/>							
														</p>
														<p class="context_regbox">
															<font color="#ff0000">*</font>
															<font class="context_regbox"> 表示為必填的欄位</font>	
														</p>	
														<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
													</td>
												</tr>
											</table>
											<p align="right" style="margin-right : 55px">
												<?php if(isset($isUpd)){?>
												<input id="isUpd" name="isUpd" type="hidden" value="<?php echo $isUpd; ?>" />
												<?php }?>
												<?php if(isset($m_id)){?>
												<input id="m_id" name="m_id" type="hidden" value="<?php echo $m_id; ?>" />
												<?php }?>
												<input id="action" name="action" type="hidden" value="update"/>
												<input class="button_margin_3" style="background-image:url(<?=$images_root;?>/buttom_modify_1.png);width: 140px; height: 40px;border:none;margin-right: 20px;" type="button" id="loginSubmit" onclick="checkForm();" value=""/>
												<input class="button_margin_3" style="background-image:url(<?=$images_root;?>/buttom_backward_1.png);width: 140px; height: 40px;border:none;margin-right: 20px;" type="button" id="loginBackward" onclick="window.history.back();" value=""/>
											</p>
											</form>
										</td>
										<!--中間區塊2-1-->
									</tr>
								</table>
							</td>
							<!--右下方圖案-->
							<td valign="bottom" align="right" width="10%" style="background-color: #ffffff;">
								<div id="corner_img_div" style="display: none;">
									<img id="corner_img" src="<?=$images_root;?>/talking.png" style="margin-right: 30px;margin-bottom: 15px;">
								</div>
							</td>
							<!--右下方圖案-->
							<!--Menu跳出小視窗1-->
								<div id="pop_menu_login" class="pop_menu" style="display: none;">
									<!-- 中間區塊2-2的跳出小視窗1  -->
									<form name="formReg" id="formReg" method="post" >
										<p class="heading" align="center">&#9674; &#9830; 登出會員系統 &#9830; &#9674;</p>
										<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
										<p align="center" class="smalltext7">&#8855;  如有修改會員、管理員密碼 &#8855;<br/>&#8855; 請重新登入!謝謝! &#8855; </p>
										<p align="center" style="opacity: 50%">
											<a href="/home/logout" >
												<img class="button_margin_1" name="index_logout" id="index_logout" src="<?=$images_root;?>/buttom_logout_1.png">
											</a>
										</p>
									<form/>
								</div>
							<!--Menu跳出小視窗1-->
							<!--Menu跳出小視窗2-->
								<div id="pop_menu_memberInfo" style="display: none;">
								<!-- 中間區塊2-2的跳出小視窗2  -->
									<p class="heading" align="center" style="margin-top: 20px;">&#9674; &#9830;會員資訊&#9830; &#9674;</p>
									<hr width="85%" style="background-color: #2F4F4F;height: 1px;border: none;" />
									<p class="context_regbox"><strong>你好，會員 : <?=$memberName;?> <?=$memberSex;?></strong></p>
									<p class="context_regbox">
										<ul class="pop_menu_text">
											<li>
												<span class="pop_menu_text_title">您已登入次數 :</span>
												<?=$loginTimes;?> 次
											</li>
											<li>
												<span class="pop_menu_text_title">最後登入時間 :</span> <?=$loginTime;?>
											</li>
											<li>
												<span class="pop_menu_text_title">您的星座 : </span> 
												<?=$constellation;?>
											</li>
											<?php 
												if(!empty($email)){
													echo '<li><span class="pop_menu_text_title">您的聯絡信箱 : </span><a href="mailto:'.$email.'" id="menu_email_linkage">'.$email.'</a></li>';
												}
												if(!empty($url)){									
													echo '<li><span class="pop_menu_text_title">您的個人網站 : </span><a href="'.$url.'" id="menu_url_linkage">'.$url.'</a></li>';
												}
											?>										
										</ul>
									</p>
									<?php if(isset($infoStatus)){ ?>
									<?php 	if($infoStatus == "isUpdate"){?>
												<input type="hidden" name="infoStatus" id="infoStatus" value="isUpdate">
									<?php 	}else if($infoStatus == "notUpdate"){?>
												<input type="hidden" name="infoStatus" id="infoStatus" value="notUpdate">
									<?php 	} ?>
									<?php } ?>
									<?php if(isset($loginTimes)){ ?>
											<input type="hidden" name="loginTimes" id="loginTimes" value="<?=$loginTimes;?>">
									<?php } ?>
								</div>
							<!--Menu跳出小視窗2-->
              <!--Menu跳出小視窗3(經歷與自傳:輸入密碼)-->
              <?php if(!isset($loginAutobiography) || $loginAutobiography != "sucess"){?>
              <div class="pop_menu" id="autobiography-pw-dialog" style="display: none;">
                <p class="context_regbox">
                  <label class="form-check-label" for="account-input" aria-describedby="inputGroupFileAddon02">自傳閱覽帳號: </label>
                  <input class="form-control account-input" id="account-input" type="text">
                </p>
                <p class="context_regbox">
                  <label class="form-check-label" for="password-input" aria-describedby="inputGroupFileAddon02">自傳閱覽密碼: </label>
                  <input class="form-control password-input" id="password-input" type="password">
                </p>
                <p class="align-center" style="opacity: 50%;">
                  <input  class="button_margin_1 password-submit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
                </p>
              </div>
              <?php } ?>
              <!--Menu跳出小視窗3(經歷與自傳:輸入密碼)-->
							<!--中間區塊2-2-->
							<td id="regbox" align="center" valign="top" width="15%">
								<table id="pop_menu_content" width="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="top">
									<?=$menuContext;?>
								</table>
								<div id="menuLastArea" style="background-image:url(<?=$images_root;?>/pop_menu_buttom_bg.png);"></div>
							</td>
							<!--中間區塊2-2-->
						</tr>
				</table>	
				</td>
			</tr>
			<!--中間區塊2-->
