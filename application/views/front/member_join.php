			<!--中間區段1-->
			<tr align="left">
				<td align="center" width="100%" height="600">
					<table  class="home_main" width="100%"  height="100%" align="center" valign="middle" border="0" cellspacing="0" cellpadding="0">
						<tbody>
							<tr>
								<td id="mainbox" valign="top" align="center" width="75%" style="background-color: #ffffff;">
									<table id="maincontent2" width="825" height="622" border="0" align="center" style="margin-top: 35px;">
										<tr  height="100%" width="100%" border="0"  valign="top">
											<td>
												<form id="formJoin" name="formJoin" action="member_join/joinMember" method="post">
												
												<p class="heading">帳號資料</p>
												<p class="context_regbox">
													<label class="form-check-label font-weight-bold" for="input_username" aria-describedby="inputGroupFileAddon02">
														<font color="#ff0000">* </font>使用帳號：
													</label>
													<input id="input_username" name="input_username" type="text" class="form-control"/>
													<div id="smalltext_7" class="ml-2">請填入20個字元以內的小寫英文字母、數字、或是_ 符號，混合而成的字串(至少需英文、數字兩種)。
													</div>
												</p>
												<p class="context_regbox">
													<label class="form-check-label font-weight-bold" for="input_password" aria-describedby="inputGroupFileAddon02">
														<font color="#ff0000">* </font>使用密碼：
													</label>
													<input id="input_password" name="input_password" class="form-control" type="password"/>
													<div id="smalltext_7" class="ml-2">請填入20個字元以內的英文字母、數字、以及各種符號組合</div>
												</p>
												<p class="context_regbox">
													<label class="form-check-label font-weight-bold" for="input_confirmPW" aria-describedby="inputGroupFileAddon02">
														<font color="#ff0000">* </font>確認密碼：
													</label>
													<input id="input_confirmPW" name="input_confirmPW" class="form-control" type="password"/>
													<div id="smalltext_7" class="ml-2">再輸入一次密碼</div>
													<hr width="95%" style="background-color: #2F4F4F;height: 1px;border: none;margin-left: 50px;" />
												</p>
												<p class="context_regbox">
													<label class="form-check-label font-weight-bold" for="input_adminPW" aria-describedby="inputGroupFileAddon02">
													管理者密碼：
													</label>
													<input id="input_adminPW" name="input_adminPW" class="form-control" type="password"/>
													<div id="smalltext_7" class="ml-2">如非管理者請勿輸入密碼!</div>
													<hr width="95%" style="background-color: #2F4F4F;height: 1px;border: none;margin-left: 50px;" />
												</p>
												<p class="heading2">個人資料</p>
												<p class="context_regbox">
													<label class="form-check-label font-weight-bold" for="input_name" aria-describedby="inputGroupFileAddon02">
														<font color="#ff0000">* </font>真實姓名：
													</label>
													<input id="input_name" name="input_name" type="text" class="form-control"/>
												</p>
												<p class="context_regbox">
													<label class="form-check-label font-weight-bold" for="input_sex" aria-describedby="inputGroupFileAddon02">
														<font color="#ff0000">* </font>性　　別：
													</label>
													<input name="input_sex" type="radio" value="M" checked/>男&nbsp;&nbsp;&nbsp;&nbsp;
													<input name="input_sex" type="radio" value="F"/>女
												</p>
												<p class="context_regbox">
													<label class="form-check-label font-weight-bold" for="input_birthday" aria-describedby="inputGroupFileAddon02">
														<font color="#ff0000">* </font>生　　日：
													</label>
												    <input type="date" class="form-control" data-format="yyyy/MM/dd" id="input_birthday" name="input_birthday">
													<div id="smalltext_7" class="ml-2">為西元格式(YYYY-MM-DD)。</div>
												</p>
												<p class="context_regbox">
													<label class="form-check-label font-weight-bold" for="input_email" aria-describedby="inputGroupFileAddon02">
														<font color="#ff0000">* </font>電子郵件：
													</label>
													<input id="input_email" name="input_email" type="text" class="form-control" />
													<div id="smalltext_7" class="ml-2">請確定此電子郵件為可使用狀態，以方便未來系統使用，如補寄會員密碼信。</div>
												</p>
												<p class="context_regbox">
													<label class="form-check-label font-weight-bold" for="input_web" aria-describedby="inputGroupFileAddon02">
													個人網頁：
													</label>
													<input id="input＿web" name="input_web" type="text" class="form-control" />
													<div id="smalltext_7" class="ml-2">請以「http://」 為開頭。</div>
												</p>
												<p class="context_regbox">
													<label class="form-check-label font-weight-bold" for="input_phone" aria-describedby="inputGroupFileAddon02">
													電　　話：
													</label>
													<input id="input＿phone" name="input_phone" type="text" class="form-control" />
												</p>
												<p class="context_regbox">
													<label class="form-check-label font-weight-bold" for="input_addr" aria-describedby="inputGroupFileAddon02">
													地　　址：
													</label>
													<input id="input＿addr" name="input_addr" type="text" class="form-control"/><br/>
												</p>
												<p class="context_regbox">
													<font color="#ff0000">*</font><font class="context_regbox"> 表示為必填的欄位</font>
													<hr width="95%" style="background-color: #2F4F4F;height: 1px;border: none;margin-left: 50px;" />
												</p>
												<p align="center">
													<input id="action" name="action" type="hidden" value="join"/>
													<input class="button_margin_3" style="background-image:url(<?=$images_root;?>/buttom_apply_1.png);width: 140px; height: 40px;border:none;margin-right: 20px;" type="button" id="loginSubmit" onclick="checkForm();" value=""/>
													<input class="button_margin_3" style="background-image:url(<?=$images_root;?>/buttom_reset_1.png);width: 140px; height: 40px;border:none;margin-right: 20px;" type="button" id="loginReset" onclick="document.formJoin.reset();" value=""/>
													<input class="button_margin_3" style="background-image:url(<?=$images_root;?>/buttom_backward_1.png);width: 140px; height: 40px;border:none;margin-right: 20px;" type="button" id="loginBackward" onclick="window.history.back();" value=""/>
												</p>
												</form>	
											</td>
										</tr>
									</table>
									<!--左側填寫內容-->	
								</td>
								<!--右下方圖案-->
								<td valign="bottom" align="right" width="10%" style="background-color: #ffffff;">
									<div id="corner_img_div" style="display: none;">
										<img id="corner_img" src="<?=$images_root;?>/talking.png" style="margin-right: 30px;margin-bottom: 15px;">
									</div>
								</td>
								<!--右下方圖案-->
								<!--中間區塊2-1-->
								<!--Menu跳出小視窗1-->
								<div id="pop_menu_login" class="pop_menu" style="display: none;">
								<!-- 中間區塊2-2的跳出小視窗  -->
									<form name="formReg" id="formReg" method="post" action="/home/login" onSubmit="return checkSubForm();">											
										<p class="heading" align="center" style="padding-right: 5px;">
											&#9830; &#9674; 登入會員系統 &#9674; &#9830; 
											</p>
											<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
											<p class="context_regbox">
												<label class="form-check-label" for="loginName" aria-describedby="inputGroupFileAddon02">帳號: </label>
												<input name="loginName" id="loginName" type="text" class="form-control" value="<?php if(isset($cookie_name)){echo $cookie_name;}else{echo '';} ?>">
											</p>
											<p class="context_regbox">
												<label class="form-check-label" for="loginPW" aria-describedby="inputGroupFileAddon02">密碼: </label> 
												<input name="loginPW" id="loginPW" type="password" class="form-control" value="<?php if(isset($cookie_pw)){echo $cookie_pw;}else{echo '';} ?>">
											</p>
											<p class="context_regbox">
												<input name="loginRem" id="loginRem" type="checkbox">
												<input name="loginRemChk" id="loginRemChk" type="hidden" class="form-check-input" value="<?php if(isset($loginRemChk)){echo $loginRemChk;}else{echo 'false';} ?>">
												<label class="form-check-label" for="loginRemChk" aria-describedby="inputGroupFileAddon02"> 記住我的帳號與密碼 </label>
											</p>
											<p align="center" style="opacity: 50%;">
												<input  class="button_margin_1" name="loginSubmit" id="loginSubmit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
											</p>
									</form>
								</div>
								<!--Menu跳出小視窗1-->
								<!--Menu跳出小視窗2-->
								<div id="pop_menu_applyNotice" style="display: none;">
								<!-- 中間區塊2-2的跳出小視窗  -->
									<p align="center" class="heading" style="color: #0059b3;margin-left: 15px;padding: 0px;">填寫資料注意事項：</p>
									<hr width="100%" style="background-color: #2F4F4F;height: 1px;border: none;" />
									<ul id="navi_notice" style="font-family: '微軟正黑體';font-size: 10pt;font-weight: bolder;color: #0000cc;">
										<li class="context_regbox">請提供您本人正確、最新及完整的資料。</li>
										<li class="context_regbox">在欄位後方出現「*」符號表示為必填的欄位。</li>
										<li class="context_regbox">填寫時請您遵守各個欄位後方的補助說明。</li>
										<li class="context_regbox">關於您的會員註冊以及其他特定資料，本系統不會向任何人出售或出借你所填寫的個人資料。</li>
										<li class="context_regbox">在註冊成功後，除了「使用帳號」外您可以在會員專區內修改您所填寫的個人資料。</li>
									</ul>								
								</div>
								<!--Menu跳出小視窗2-->
                <!--Menu跳出小視窗3(經歷與自傳:輸入密碼)-->
                <?php if(!isset($loginAutobiography) || $loginAutobiography != "sucess"){?>
                <div class="pop_menu" id="autobiography-pw-dialog" style="display: none;">
                  <p class="context_regbox">
                    <label class="form-check-label" for="account-input" aria-describedby="inputGroupFileAddon02">自傳閱覽帳號: </label>
                    <input class="form-control account-input" id="account-input" type="text">
                  </p>
                  <p class="context_regbox">
                    <label class="form-check-label" for="password-input" aria-describedby="inputGroupFileAddon02">自傳閱覽密碼: </label>
                    <input class="form-control password-input" id="password-input" type="password">
                  </p>
                  <p class="align-center" style="opacity: 50%;">
                    <input  class="button_margin_1 password-submit" src="<?=$images_root;?>/buttom_login_1.png" type="image">
                  </p>
                </div>
                <?php } ?>
                <!--Menu跳出小視窗3(經歷與自傳:輸入密碼)-->
								<!--中間區塊2-2-->
								<td id="regbox" align="center" valign="top" width="15%">
									<table id="pop_menu_content" width="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="top">
										<?=$menuContext;?>
									</table>
									<div id="menuLastArea" style="background-image:url(<?=$images_root;?>/pop_menu_buttom_bg.png);background-repeat: repeat-y;"></div>
								</td>
								<!--中間區塊2-2-->
							</tr>
						</tbody>
				</table>	
				</td>
			</tr>
			<!--中間區塊2-->