<!--中間區塊3-->
			<tr>
				<td class="smalltext | footer_bg" align="center" width="100%" height="240" background="<?=$images_root;?>/shadow_1.png">
					<table  width="100%" height="240" border="0" cellpadding="0" cellspacing="0" align="center" valign="bottom">
						<tr width="100%" height="80%" valign="middle" align="center" border="0" >
							<td width="60%" valign="top" >
								<p class="footer_p1">
									<b>&#9001;&#9830; &#9830; 作品交流區 &#9830; &#9830;&#9002;</b>
								</p><br/>
								<hr size="1" width="90%" color="#c8b3cc"/>
								<div id="geo_result" align="left" height="80" style="padding-left: 27%">
									<ul class=".context_footer">
										<li><b>網頁作品(需解壓縮與密碼) : </b><ins><a href="https://goo.gl/fWhcch" target="_blank">https://goo.gl/fWhcch</a></ins></li>
										<li><b>網頁作品架構與內容簡介 : </b><ins><a href="https://goo.gl/YJLbac" target="_blank">https://goo.gl/YJLbac</a></ins></li>
										<li><b>手機App.作品 : </b><ins><a href="https://goo.gl/xSg4wg" target="_blank">https://goo.gl/xSg4wg</a></ins></li>
										<li><b>手機App.作品架構與內容簡介 : </b><ins><a href="https://goo.gl/VcV37i" target="_blank">https://goo.gl/VcV37i</a></ins></li>
									</ul>
								</div>		
							</td>
							<td width="40%" valign="top" >
								<form id="formDate" action="" method="post">
									<p class="footer_p1_1">
										<strong>【目前日期為&nbsp;:&nbsp;<?=$date_now;?>】<br/>
												【目前時間為&nbsp;:&nbsp;<q id="showTime">請按下方文字</q>】
										</strong>
									</p>
									<p>
										<u><a class="footer_p5" onclick="javascript:setTime();" >※如需更新時間請按我&nbsp;&nbsp;/&nbsp;&nbsp;日期需更新，請F5重新整理</a></u>
									</p>
									<hr size="1" width="95%" color="#c8b3cc"/>
									<p id="audio_info" class="footer_p3" width="100%" align="left" style="padding-left: 40px;">
									</p>
									<p>
										<table class="player_skin1" width="130" height="45" border="0" cellpadding="0" cellspacing="0" align="left" valign="middle">
											<tr align="center" valign="middle">
												<td  background="<?=$images_root;?>/player_skin.png">
													<img class="player_button1" src="<?=$images_root;?>/play_buttom.png" value="play" onclick="media_play();"/>
													<img class="player_button1" src="<?=$images_root;?>/pause_buttom.png" value="pause" onclick="media_pause();"/>
													<img class="player_button1" src="<?=$images_root;?>/stop_buttom.png" value="stop" onclick="setStop();"/>
												</td>
											</tr>
										</table>
									</p>	
								</form>	
							</td>
						</tr>
						<tr width="100%" height="20%" valign="middle" align="center" border="0" >
							<td width="60%" valign="top" >
								<table   id="geo" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" align="left" valign="bottom">
										<tr>
											<td valign="middle"> 

											</td>
										</tr>	
								</table>
							</td>
							<td width="40%" valign="top" align="right" style="padding-right: 30px;padding-top: 30px;">
								<p class="footer_p4">
									<small>
										※如果你所使用的瀏覽器無法撥放檔案，請<a id="open_broswer" href="" style="color: #ff5c33;">點擊</a>此處!<br/>
										※聆聽背景音樂時請注意音量，以免損及聽力
									</small>
								</p>
							</td>
						</tr>
					</table>
				</td>
			</tr><!--中間區塊3-->
			<tr align="center" valign="bottom" >
				<td class="trademark | trademark_bg" width="100%" height="100"  background="<?=$images_root;?>/footer_1.png"  >
						<hr size="2" width="100%" color="#476c6c" />
						<p>
							Edited By <ins><a href="https://www.facebook.com/leowang10251025">Wang How-Wei(Leo)/網站版本: v1.00f</a></ins>
							&nbsp;&nbsp;聯絡方式&nbsp;:&nbsp;<ins><a href="mailto:leowang1025rd@gmail.com?
							subject=你好，我是瀏覽者，想與你連絡...&cc=leowang10251025@gmail.com&
							body=To Leo:%0d%0a你好，我想請問...%0d%0a%0d%0a%0d%0a連絡方式:%0d%0a
							(你的大名:請留下你的暱稱或名字，以方便與你連絡)/%0d%0a(你的連絡信箱:格式如xxx.oo@gmail.com)。">leowang1025rd@gmail.com</a></ins>
							&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;最佳解析度:1280x768&nbsp;&nbsp;建議使用瀏覽器種類:
							<a href="http://www.google.com.tw/chrome/browser">Chrome</a>
						</p>
						<p class="trademark_bottom">
							&#171;&nbsp;此版面僅提供技術交流使用，請注重智慧財產權，所有商標的權利為原創者所有。&nbsp;&#187;&nbsp;&nbsp;&nbsp; © 2020 All Rights Reserved.
						</p>
				</td>
			</tr>
			<!--中間區塊5-->