function windowOpen(url, windowName, windowHeight, windowWidth){

	window.open(url, windowName, "height="+windowHeight+", width="+windowWidth+", top=0, left=0, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no");
}

function checkmail(myMail){

	var filter=/^([a-z0-9_\.\-])+\@([a-z0-9\-]+\.)+([a-z0-9\-]{2,4})+$/;

	if(filter.test(myMail.value)){
		return true;
	}else{
		return false;
	}

}

function shortenMenuBarText(linkageId, realText, maxLength, realTextTitle) {
	
	$(linkageId).each(function(){
		realText = $(this).text();
		if($(this).text().length > maxLength){
			$(this).text($(this).text().substring(0, maxLength));
			$(this).html($(this).html()+'...');
		}
	});

	$(linkageId).on("mouseenter mouseleave", function(event){

		if(event.type == "mouseenter"){
			if($(this).text().length > maxLength){
				swal({ 
					title: realTextTitle, 
					text: realText, 
					type: "info",
					showCancelButton:false,
   					showConfirmButton:false,
   					buttons: false,
   					timer: 2000
					}
				);
			}
		}

	});
}
