	/*Google　Map  API:地理資訊讀取*/
			var show_lat,show_lng,URL;
			
			window.addEventListener("load", function(){
				navigator.geolocation.getCurrentPosition(
						// 成功取得位置資訊後的處理程序
						function(position){
								var location;
								show_lat=position.coords.latitude;
								show_lng=position.coords.longitude;
								location = "緯度：" + show_lat + "<br/>";
								location += "經度：" +show_lng + "<br/>";
								location += "高度：" + position.coords.altitude + "<br/>";
								location += "方位：" + position.coords.heading + "<br/>";
								document.getElementById("geo_result").innerHTML = location;
								URL="map_show.php?Show_Lat="+show_lat+"&Show_Lng="+show_lng;
						},
						// 取得失敗時的處理程序
						function(error){
							document.getElementById("geo_result").innerHTML = "發生錯誤了<br/>";
							var msg="";
							switch(error.code){
			
								case error.TIMEOUT:
								msg="連線逾時";
								break;
				
								case error.POSITION_UNAVAILABLE:
								msg="無法取得位置資訊";
								break;
				
								case error.PERMISSION_DENIED:
								msg="沒有權限使用Geolocation API";
								break;
				
								case error.UNKNOWN_ERROR:
								msg="未知的錯誤:"+error.message;
								break;
				
							}
					document.getElementById("geo_result").innerHTML = msg;
					},
					// 選項
					{enableHighAccuracy:false, timeout:3000}
				);
			}, true);
			
			function getMapURL(){
				location.href=URL;
			}