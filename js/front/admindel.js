$(function(){

});



function checkForm(){

	var boardid=$('#boardid').val();

	swal({ 
		title: "---確認---", 
		text: "請問刪除此筆資料嗎?", 
		type: "info",
		confirmButtonColor: "#DD6B55",
		showCancelButton: true,
		confirmButtonText: '確定刪除',
		cancelButton: '取消',
		cancelButtonText: '取消刪除',
		closeOnConfirm: false,
		closeOnCancel: false
	},
	function(isConfirm){

		if(isConfirm){

			swal({ 
			title: "請注意...!", 
			text: "已刪除!", 
			type: "success",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){
				$('#formDelete').submit();
			}
			);
			return true;

		}else{
			swal("---確認---","已取消!","error");
			return false;
		}

	});

	return false;
}