$(function(){

	swal.setDefaults(
		{confirmButtonText:"我了解了" , cancelButtonText:"取消"}
	);

	var isUpd=$('#isUpd').val();
	var isErr=$('#isErr').val();
	$('#input_password_p').hide();
	$('#input_confirmPW_p').hide();

	$('#original_password').change(function(){
		checkOrignalPW($('#original_password').val(), $('#m_id').val());
		$('#smalltext_3').hide();
	});

	//處理Menu Bar 會員資料文字過長問題
	var maxLength = 25;
	var realMailText = "";
	var realUrlText = "";

	shortenMenuBarText("#menu_email_linkage", realMailText, maxLength, "您的正確電子信箱 : "); //電子信箱判斷處理
	shortenMenuBarText("#menu_url_linkage", realUrlText, maxLength, "您的正確個人頁面 : "); //個人頁面判斷處理

});

var input_name="";
var input_birthday="";
var input_email="";
var input_password="";
var input_confirmPW="";
var input_sex="";
var input_url="";
var input_phone="";
var input_addr="";
var password_length="";
var isConfirm=false;

function checkForm(){

	var isUpd="0";
	var upd_id=$('#m_id').val();

	if(checkContent()){
		isUpd="1";
		swal({ 
			title: "---確認---", 
			text: "請問更新此筆資料嗎?", 
			type: "info",
			confirmButtonColor: "#DD6B55",
			showCancelButton: true,
			confirmButtonText: '確定更新',
			cancelButton: '取消',
			cancelButtonText: '取消更新',
			closeOnConfirm: false,
			closeOnCancel: false
		},
		function(isConfirm){

			if(isConfirm){

				isUpd="1";

				swal({ 
				title: "請注意...!", 
				text: "已更新!", 
				type: "success",
				confirmButtonColor: "#DD6B55",
				closeOnConfirm: true
				},
				function(){
					$('#formUpdate').submit();
				}
				);
				return true;

			}else{
				isUpd="0";
				swal("---確認---","已取消!","error");
				return false;
			}

		});

	}else{

		isUpd="0";

	}
}


function checkContent(){

	input_name=$('#input_name').val();
	input_birthday=$('#input_birthday').val();
	input_email=$('#input_email').val();
	input_password=$('#input_password').val();
	input_confirmPW=$('#input_confirmPW').val();
	input_sex=$("input[type='radio'][name='input_sex']:checked").val();
	input_url=$('#input_url').val();
	input_phone=$('#input_phone').val();
	input_addr=$('#input_addr').val();
	password_length=input_password.length;

	// 密碼之正確性檢查

	if(!(input_password === undefined || input_confirmPW === undefined || input_password === null ||input_confirmPW === null))
	{
		for(var idx=0;idx<password_length;idx++){

			if (input_password.charAt(idx) == ' ' || input_password.charAt(idx) == '\"') {
				    swal({
				            title: "請注意...!",
				            text: "密碼不可以含有空白或雙引號!",
				            type: "warning",
				            confirmButtonColor: "#DD6B55",
				            closeOnConfirm: true
				        },
				        function() {
				            $('input_password').css("background-color", "#D6D6FF");
				        }
				    );
		    	return false;
			}

			if (password_length > 20) {
			    swal({
			            title: "請注意...!",
			            text: "密碼長度最多個20字母喔，\n建議使用數字與英文字母或其他符號組成!",
			            type: "warning",
			            confirmButtonColor: "#DD6B55",
			            closeOnConfirm: true
			        },
			        function() {
			            $('input_password').css("background-color", "#D6D6FF");
			        }
			    );
			    return false;
			}

			if (input_password != input_confirmPW){

			    swal({
			            title: "請注意...!",
			            text: "密碼二次輸入不一樣,請重新輸入 !",
			            type: "warning",
			            confirmButtonColor: "#DD6B55",
			            closeOnConfirm: true
			        },
			        function() {
			            $('input_password').css("background-color", "#D6D6FF");
			        }
			    );
			    return false;
			}
			return true;
		} 

	}

	if(input_name==undefined || input_name==""){

		swal({ 
			title: "請注意...!", 
			text: "請填寫真實姓名!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){
				$('#input_name').css("background-color","#D6D6FF");
			}
		);
		
		return false;
	}
			
	if(input_birthday==undefined || input_birthday==""){

		swal({ 
			title: "請注意...!", 
			text: "請填寫生日!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
		},
		function(){
			$('#input_birthday').css("background-color","#D6D6FF");
			}
		);

		return false;
	}

	//信件填寫檢查
	if(input_email==undefined || input_email==""){

		swal({ 
			title: "請注意...!", 
			text: "請填寫電子郵件!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
		},
		function(){
			$('#input_email').css("background-color","#D6D6FF");
			}
		);
		
		return false;
	}
			
	//信件格式正確性檢查
			
	var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	// alert("checkmail");
	if(filter.test(input_email)){
		return true;
	}else{
		swal({ 
		title: "請注意...!", 
		text: "電子郵件格式不正確!", 
		type: "warning",
		confirmButtonColor: "#DD6B55",
		closeOnConfirm: true
		},
		function(){
			$('#input＿email').css("background-color","#D6D6FF");
		});
		return false;
	}

	return true;
}

function checkOrignalPW(orignal_pw, userId){
	$.ajax({
        type: "post", 
        url: '/member_admin_update/checkOrignalPW',
        data: {
            m_id: userId
        },
        cache: false,
        // dataType: "json",
        success: function (response) {

        	if(response != orignal_pw){

        		swal({ 
				  title: "請注意...!", 
				  text: "您輸入的原密碼有誤，\n請確認輸入是否正確，謝謝!!", 
				  type: "warning",
				  confirmButtonColor: "#DD6B55",
				  closeOnConfirm: true
				},
				function(){
					$('#original_password').css("background-color","#D6D6FF");
					$('#smalltext_3').show();
				}
				);

        	}else{
        		$('#original_password_p').hide();
    			$('#input_password_p').show();
				$('#input_confirmPW_p').show();
        	}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}