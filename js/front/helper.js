$(function(){

	var isAddComment = $('#isAddComment').val();

	if(isAddComment == 0){

		swal({ 
			title: "請注意...!", 
			text: "留言失敗!請在留言一次或洽系統管理員!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){

			}
		);

		return false;

	}else if(isAddComment == 1){

		swal({ 
			title: "提醒您", 
			text: "留言成功!歡迎再度發問喔!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){

			}
		);

		return false;

	}
	
});