$(function(){

	if($('#sendStatus').val() == "sendSuccess"){

		swal({ 
			title: "通知您...!", 
			text: "您聯絡管理員的信件已寄出，\n謝謝您的來信!!", 
			type: "info",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){

			}
		);

		return false;

	}else if($('#sendStatus').val() == "sendFail"){

		swal({ 
			title: "請注意...!", 
			text: "非常抱歉，您聯絡管理員的信件寄送出了些問題，\n請再寄送一次或以電話、留言板告知此問題...", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){

			}
		);

		return false;

	}


});

function checkNaviState(){

	swal({ 
	  title: "請注意...!", 
	  text: "請先申請會員資格後並且登入!!\n才能使用導覽列留言板的功能喔...", 
	  type: "warning",
	  confirmButtonColor: "#DD6B55",
	  closeOnConfirm: true
	});

	return false;
}