$(function(){

	$('#helper').click(function(){
		checkTopLeftTitleState('helper');
	});

	$('#contact').click(function(){
		checkTopLeftTitleState('contact');
	});
	
});

function checkTopLeftTitleState(pageType){

	var loginChk = $('#loginChk').val();
	var alertText = '';

	if($('#pageName').val() == 'member_join'){
		alertText = '請先加入會員並重新登入';
	}else if($('#pageName').val() == 'admin_passmail'){
		alertText = '請先申請會員資格後並且登入';
	}else{
		alertText = '請先登入或申請加入會員';
	}

	if(typeof loginChk === "undefined"){ //當值為undefined

		swal({ 
			  title: "提醒你...", 
			  text: alertText+"!!才能使用輔助列的協助聯絡功能喔...", 
			  type: "info",
			  confirmButtonColor: "#DD6B55",
			  closeOnConfirm: true
			},
			function(){
				$('#loginName').css("background-color","#D6D6FF");
				$('#loginPW').css("background-color","#D6D6FF");
			}
		);
		return false;

	}else{
		if(loginChk == "sucess"){
			if(pageType == "helper"){
				location.href="/"+pageType+"/index/"+$('#userId').val();
			}else if(pageType == "contact"){
				location.href="/"+pageType+"/index/1";
			}

		}
	}	
	
}