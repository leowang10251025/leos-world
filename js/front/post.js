		$(function(){
			swal.setDefaults(
				{confirmButtonText:"我了解了" , cancelButtonText:"取消"}
			);

			if($('#errMsg').val()=="login_error"){
				swal({ 
					title: "錯誤", 
					text: "登入帳號或密碼錯誤!!請再次核對!", 
					type: "warning",
					confirmButtonColor: "#DD6B55",
					closeOnConfirm: true
				},
				function(isConfirm){
					if(isConfirm){
						$('#username').css("background-color","#D6D6FF");
						$('#passwd').css("background-color","#D6D6FF");
					}
				}
				);
			}
			
			$('#username').change(function(){
				checkAdminAcc($('#username').val());
			});

			$('#passwd').change(function(){
				if($('#username').val() != ""){
					checkAdminPw($('#username').val(), $('#passwd').val());
				}
			});			

		});
		

		function checkForm(){
			if(document.formPost.board_subject.value==""){

				swal({ 
					  title: "請注意...!", 
					  text: "請填寫標題...", 
					  type: "warning",
					  confirmButtonColor: "#DD6B55",
					  closeOnConfirm: true
				},
				function(){
					$('#board_subject').css("background-color","#D6D6FF");
				});
				return false;
			}
			
			if(document.formPost.board_name.value==""){

				swal({ 
					  title: "請注意...!", 
					  text: "請填寫姓名...", 
					  type: "warning",
					  confirmButtonColor: "#DD6B55",
					  closeOnConfirm: true
				},
				function(){
					$('#board_name').css("background-color","#D6D6FF");
				});
				return false;
			}

			// alert("board_content:"+CKEDITOR.instances.board_content.getData());
			
			if(CKEDITOR.instances.board_content.getData()==""){

				swal({ 
					  title: "請注意...!", 
					  text: "請填寫留言內容!", 
					  type: "warning",
					  confirmButtonColor: "#DD6B55",
					  closeOnConfirm: true
				},
				function(){
					$('#board_content').css("background-color","#D6D6FF");
				});
				return false;
			}
			
			if(document.formPost.board_email.value==""){

				swal({ 
					  title: "請注意...!", 
					  text: "請填寫連絡信箱!", 
					  type: "warning",
					  confirmButtonColor: "#DD6B55",
					  closeOnConfirm: true
				},
				function(){
					$('#board_email').css("background-color","#D6D6FF");
				});
				return false;
			}
			
			var board_email=$('#board_email').val();
			if(!checkmail(board_email)){

				return false;
			}
			
			var form=document.getElementById("formPost" );
			var sex="";
			for(var i=0;i<form.board_sex.length;i++){
				if(form.board_sex[0].checked) {
				sex="帥哥\n";
				break;
				}else if(form.board_sex[1].checked) {
				sex="美女\n";
				break;
				}else{
					swal({ 
					  title: "請注意...!", 
					  text: "請選擇你的性別...", 
					  type: "warning",
					  confirmButtonColor: "#DD6B55",
					  closeOnConfirm: true
					},
					function(){
						$('#board_sex').css("background-color","#D6D6FF");
					});

				return false;
				}
			}

			var board_sex="";

			var board_subject=$('#board_subject').val();
			var board_sex=$("input[type='radio'][name='board_sex']:checked").val();
			var board_name=$('#board_name').val();
			var board_content = CKEDITOR.instances.board_content.getData();
			board_content = board_content.replace(/<[^>]+>/g,"");
			var board_email=$('#board_email').val();
			var board_web=$('#board_web').val();
			var insert="0";

			swal({ 
				title: "---留言紀錄---", 
				text: "Hi!!"+board_name+sex+"\n你的留言【"+board_subject+"】如下:\n"+board_content+"\n"+"-------------------------------------\n以上留言確定送出嗎?", 
				type: "info",
				confirmButtonColor: "#DD6B55",
				showCancelButton: true,
				confirmButtonText: '確定送出',
				cencelButton: '取消',
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(isConfirm){

			if(isConfirm){
				// insert="1";
				$('#formPost').submit();
				return true;
			}else{
				// insert="0";
				return false;
			}
				
			});
			return false;
		}

		
		//留言板登入管理
		function checkSubForm(){

			if($('#username').val() == ""){

				swal({ 
				  title: "請注意...!", 
				  text: "請填入留言板管理帳號!!", 
				  type: "warning",
				  confirmButtonColor: "#DD6B55",
				  closeOnConfirm: true
				},
				function(){
					$('#username').css("background-color","#D6D6FF");
				});

				return false;
			}

			if($('#passwd').val() == ""){

				swal({ 
				  title: "請注意...!", 
				  text: "請填入留言板管理密碼!!", 
				  type: "warning",
				  confirmButtonColor: "#DD6B55",
				  closeOnConfirm: true
				},
				function(){
					$('#passwd').css("background-color","#D6D6FF");
				});

				return false;
			}

		}
		
		function checkmail(myMail){
				var filter=/^([a-z0-9_\.\-])+\@([a-z0-9\-]+\.)+([a-z0-9\-]{2,4})+$/;
				if(filter.test(myMail)){
					return true;
				}

				swal({ 
					  title: "請注意...!", 
					  text: "電子郵件格式不正確!!", 
					  type: "warning",
					  confirmButtonColor: "#DD6B55",
					  closeOnConfirm: true
				},
				function(){
					$('#board_email').css("background-color","#D6D6FF");
				});

				return false;
		}

		function checkAdminAcc(admin_acc){
			$.ajax({
		        type: "post", 
		        url: '/message_board/checkAdminAcc',
		        data: {
		            admin_acc: admin_acc
		        },
		        cache: false,
		        // dataType: "json",
		        success: function (response) {

		        	if(response == ""){
		        		swal({ 
						  title: "請注意...!", 
						  text: "您輸入的管理者帳號有誤或沒填入資料，\n請確認輸入是否正確，謝謝!!", 
						  type: "warning",
						  confirmButtonColor: "#DD6B55",
						  closeOnConfirm: true
						},
						function(){
							$('#username').css("background-color","#D6D6FF");
							$('#chkLoginName').val("fail");
						}
						);
		        	}else{
		        		$('#chkLoginName').val("success");
		        	}
		        },
		        error: function(XMLHttpRequest, textStatus, errorThrown) {

		        }
		    });
		}

		function checkAdminPw(admin_acc, admin_pw){	
			$.ajax({
		        type: "post", 
		        url: '/message_board/checkAdminPw',
		        data: {
		            admin_acc: admin_acc,
		            admin_pw: admin_pw
		        },
		        cache: false,
		        // dataType: "json",
		        success: function (response) {
		        	if(response == "notEqual"){
		        		swal({ 
						  title: "請注意...!", 
						  text: "您輸入的管理者密碼:\""+admin_pw+"\"有誤，\n請確認輸入是否正確，謝謝!!", 
						  type: "warning",
						  confirmButtonColor: "#DD6B55",
						  closeOnConfirm: true
						},
						function(){
							$('#passwd').css("background-color","#D6D6FF");
							$('#chkLoginPw').val("fail");
						}
						);
		        	}else{
		        		$('#chkLoginPw').val("success");
		        	}
		        },
		        error: function(XMLHttpRequest, textStatus, errorThrown) {

		        }
		    });
		}		