$(function(){

	$('#banner_2_2_2').click(function(){
		location.href="/home/index/2";
	});

	$('#banner_2_2_3').click(function(){
		location.href="/home/index/3";
	});

	$('#banner_2_2_4').click(function(){
		location.href="/home/index/4";
	});

	$('#banner_2_2_5').click(function(){
		checkNaviState();
	});

	$('#banner_2_3').click(function(){
		location.href="/home/index/1";
	});

	swal.setDefaults(
	{confirmButtonText:"我了解了" , cancelButtonText:"取消"}
	);

	$('#userName').change(function(){
		$('#userNameDiv').show();
		$('#eMailDiv').hide();
		checkUserName($('#userName').val());
	});

	$('#eMail').change(function(){
		$('#eMailDiv').show();
		$('#userNameDiv').hide();
		checkUserMail($('#eMail').val());
	});


	$('#button_margin_1').click(function(){

		if($('#userName').val()=="" && $('#eMail').val()==""){

			swal({ 
				title: "請注意...!", 
				text: "請先輸入您的申請帳號或聯絡信箱，才能取得新的密碼喔!!", 
				type: "warning",
				confirmButtonColor: "#DD6B55",
				closeOnConfirm: true
				},
				function(){
					$('#userName').css("background-color","#D6D6FF");
					$('#eMail').css("background-color","#D6D6FF");
				}
			);

			return false;

		}else if($('#userName').val()!="" || $('#eMail').val()!=""){

			$("#formForgotPw").submit();

		}

	});

	$('#button_margin_2').click(function(){
		window.history.back();
	});

	if($('#errMsg').val() == "nameError"){

		swal({ 
			title: "請注意...!", 
			text: "查無此帳號!\n請確認輸入是否正確，謝謝!!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){
				$('#userName').css("background-color","#D6D6FF");
			}
		);

		return false;

	}else if($('#errMsg').val() == "mailError"){

		swal({ 
			title: "請注意...!", 
			text: "查無此聯絡信箱!\n請確認輸入是否正確，謝謝!!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){
				$('#eMail').css("background-color","#D6D6FF");
			}
		);

	}

	if($('#errMsg').val() == "sendSuccess"){

		swal({ 
			title: "通知您...!", 
			text: "您索取的帳號/密碼，已經重新寄到您的聯絡信箱，\n請至您的信箱查收，謝謝!!", 
			type: "info",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){

			}
		);

		return false;

	}else if($('#errMsg').val() == "sendFail"){

		swal({ 
			title: "請注意...!", 
			text: "非常抱歉，您的忘記密碼信件寄送出了些問題，\n請洽系統管理員處理!!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){

			}
		);

	}
	
});

function checkUserName(userName){
	$.ajax({
        type: "post", 
        url: '/admin_passmail/chkUserName',
        data: {
            name: userName
        },
        cache: false,
        // dataType: "json",
        success: function (response) {

        	if(response == "notMatch"){

        		swal({ 
				  title: "請注意...!", 
				  text: "查無此帳號 : "+userName+"，\n請確認輸入是否正確，謝謝!!", 
				  type: "warning",
				  confirmButtonColor: "#DD6B55",
				  closeOnConfirm: true
				},
				function(){
					$('#userName').css("background-color","#D6D6FF");
				}
				);

        	}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

function checkUserMail(userMail){

	if(!checkmail(document.formReg.eMail)){

			swal({ 
				title: "請注意...!", 
				text: "電子郵件格式不正確!", 
				type: "warning",
				confirmButtonColor: "#DD6B55",
				closeOnConfirm: true
				},
				function(){
					$('#eMail').css("background-color","#D6D6FF");
				}
			);

			return false;
				
	}else{

		$.ajax({
	        type: "post", 
	        url: '/admin_passmail/checkUserMail',
	        data: {
	            mail: userMail
	        },
	        cache: false,
	        // dataType: "json",
	        success: function (response) {

	        	if(response == "notMatch"){

	        		swal({ 
					  title: "請注意...!", 
					  text: "查無此聯絡信箱 : "+userMail+"，\n請確認輸入是否正確，謝謝!!", 
					  type: "warning",
					  confirmButtonColor: "#DD6B55",
					  closeOnConfirm: true
					},
					function(){
						$('#eMail').css("background-color","#D6D6FF");
					}
					);
					
	        	}
	        },
	        error: function(XMLHttpRequest, textStatus, errorThrown) {

	        }
	    });
		
	}

}

function checkNaviState(){

	swal({ 
	  title: "請注意...!", 
	  text: "請先申請會員資格後並且登入!!\n才能使用導覽列留言板的功能喔...", 
	  type: "warning",
	  confirmButtonColor: "#DD6B55",
	  closeOnConfirm: true
	});

	return false;
}

function checkmail(myMail){

	var filter=/^([a-z0-9_\.\-])+\@([a-z0-9\-]+\.)+([a-z0-9\-]{2,4})+$/;

	if(filter.test(myMail.value)){
		return true;
	}else{
		return false;
	}

}
