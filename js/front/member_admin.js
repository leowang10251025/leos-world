			$(function(){

				swal.setDefaults(
				{confirmButtonText:"我了解了" , cancelButtonText:"取消"}
				);

				var infoStatus=$('#infoStatus').val();
				var loginTimes=$('#loginTimes').val();

				//處理Menu Bar 會員資料文字過長問題
				var maxLength = 25;
				var realMailText = "";
				var realUrlText = "";

				shortenMenuBarText("#menu_email_linkage", realMailText, maxLength, "您的正確電子信箱 : "); //電子信箱判斷處理
				shortenMenuBarText("#menu_url_linkage", realUrlText, maxLength, "您的正確個人頁面 : "); //個人頁面判斷處理

        $('button[class*="btn-primary"]').on('click', function(){
          readyToUpdate($(this).data("mid"));
        });

        $('button[class*="btn-danger"]').on('click', function(){
          confirmDelete($(this).data("mid"));
        });

				if(infoStatus=="isDelete"){

					swal({ 
						title: "刪除成功", 
						text: "您刪除的資料已移除!", 
						type: "info",
						confirmButtonColor: "#DD6B55",
						closeOnConfirm: true,
						}
					);
					return false;

				}else if(infoStatus=="notDelete"){

					swal({ 
						title: "刪除失敗", 
						text: "您想刪除的資料刪除失敗!請洽系統管理員", 
						type: "warning",
						confirmButtonColor: "#DD6B55",
						closeOnConfirm: true,
						}
					);
					return false;

				}

				if(infoStatus=="isUpdate"){

					swal({ 
						title: "更新成功", 
						text: "您更新的資料已更新成功!", 
						type: "info",
						confirmButtonColor: "#DD6B55",
						closeOnConfirm: true,
						}
					);
					return false;

				}else if(infoStatus=="notUpdate"){

					swal({ 
						title: "更新失敗", 
						text: "您想更新的資料更新失敗!請洽系統管理員", 
						type: "warning",
						confirmButtonColor: "#DD6B55",
						closeOnConfirm: true,
						}
					);
					return false;

				}

				if(loginTimes == 1){

					swal({ 
						title: "提醒你...", 
						text: "管理員加入成功!!\n開始使用你的管理員功能吧!", 
						type: "info",
						confirmButtonColor: "#DD6B55",
						closeOnConfirm: false
						}
					);
					return false;

				}

			});

      function readyToUpdate(m_id){
        location.href="/member_admin_update/index/"+m_id;
      }

			function confirmDelete(m_id){
				swal({
          title:'提醒你...',
          text:'\n您確定要刪除這個管理者嗎?\n刪除後無法恢復!\n',
          type:'warning',
          showCancelButton:true,
          confirmButtonColor:'#DD6B55',
          confirmButtonText:'確定刪除！',
          closeOnConfirm:false
        },
        function(){
          swal("刪除","管理者已經刪除","success");
          location.href="/member_admin/deleteMember/"+m_id;
          
        }
        );
				return false;
			}