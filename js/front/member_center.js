			$(function(){

				swal.setDefaults(
				{confirmButtonText:"我了解了" , cancelButtonText:"取消"}
				);

				var infoStatus=$('#infoStatus').val();
				var loginTimes=$('#loginTimes').val();

				//處理Menu Bar 會員資料文字過長問題
				var maxLength = 25;
				var realMailText = "";
				var realUrlText = "";

				shortenMenuBarText("#menu_email_linkage", realMailText, maxLength, "您的正確電子信箱 : "); //電子信箱判斷處理
				shortenMenuBarText("#menu_url_linkage", realUrlText, maxLength, "您的正確個人頁面 : "); //個人頁面判斷處理

				if(infoStatus=="isUpdate"){

					swal({ 
						title: "更新成功", 
						text: "您更新的資料已更新成功!", 
						type: "info",
						confirmButtonColor: "#DD6B55",
						closeOnConfirm: true,
						}
					);
					return false;

				}else if(infoStatus=="notUpdate"){

					swal({ 
						title: "更新失敗", 
						text: "您想更新的資料更新失敗!請洽系統管理員", 
						type: "warning",
						confirmButtonColor: "#DD6B55",
						closeOnConfirm: true,
						}
					);
					return false;

				}

				if(loginTimes == 1){

					swal({ 
						title: "提醒你...", 
						text: "一般會員加入成功!!\n開始使用你的會員功能吧!", 
						type: "info",
						confirmButtonColor: "#DD6B55",
						closeOnConfirm: false
						}
					);
					return false;

				}

			});