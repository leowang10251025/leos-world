			$(function(){

				var loginChk = $('#loginChk').val();
				var pageName = $('#pageName').val();

				$('#banner_2_2_2').click(function(){
					location.href="/home/index/2";
					if(loginChk=='sucess'){
						location.href="/home/haveLogin/"+$('#memberLevel').val()+"/"+$('#userId').val()+"/2";
					}
				});

				$('#banner_2_2_3').click(function(){
					location.href="/home/index/3";
					if(loginChk=='sucess'){
						location.href="/home/haveLogin/"+$('#memberLevel').val()+"/"+$('#userId').val()+"/3";
					}
				});

				$('#banner_2_2_4').click(function(){
					location.href="/home/index/4";
					if(loginChk=='sucess'){
						location.href="/home/haveLogin/"+$('#memberLevel').val()+"/"+$('#userId').val()+"/4";
					}
				});

				$('#banner_2_2_5').click(function(){

					if(typeof loginChk === "undefined"){ //當值為undefined
						checkNaviStateWithoutRHContext();
					}

				});

				$('#banner_2_3').click(function(){
					location.href="/home/index/1";
					if($('#loginChk').val()=='sucess'){
						location.href="/home/haveLogin/"+$('#memberLevel').val()+"/"+$('#userId').val()+"/1";
					}
				});

				swal.setDefaults(
					{confirmButtonText:"我了解了" , cancelButtonText:"取消"}
				);

				var member_level=$('#member').val();
				var admin_level=$('#admin').val();
				var refill=$('#refill').val();
				
				if(member_level!=undefined){
					console.log(member_level);
					swal({ 
					  title: "恭喜你", 
					  text: "一般會員加入成功!!\n歡迎使用新申請的帳號重新登入!\n開始使用你的會員功能吧!^^\n請重新登入...", 
					  type: "info",
					  confirmButtonColor: "#DD6B55",
					  closeOnConfirm: false
					},
					function(isConfirm){
						if(isConfirm){
							window.location.href='index.php?return=2';
						}
					}
					);
				}else if(admin_level!=undefined){
					console.log(admin_level);
					swal({ 
					  title: "恭喜你", 
					  text: "管理者重新加入成功!!\n歡迎使用新帳號好好整理你的天地喔!^^\n請重新登入...", 
					  type: "info",
					  confirmButtonColor: "#DD6B55",
					  closeOnConfirm: false
					},
					function(isConfirm){
						if(isConfirm){
							window.location.href='index.php?return=2';
						}
					}
					);
				}else if(refill!=undefined){
					console.log(refill);
					swal({ 
					  title: "糟糕!", 
					  text: "是不是管理帳號的密碼填寫錯誤呢!?\n請重新確認...", 
					  type: "warning",
					  confirmButtonColor: "#DD6B55",
					  closeOnConfirm: true
					}
					);
				}

				$("#input_username").change(function(){
					checkUserName($("#input_username").val());
				});

				$("#input_email").change(function(){
					checkUserMail($("#input_email").val());
				});

			});

			function checkUserName(userName){
				$.ajax({
	                type: "post", 
	                url: '/member_join/chkUserName',
	                data: {
	                    name: userName
	                },
	                cache: false,
	                // dataType: "json",
	                success: function (response) {
	                	if(response == userName){

	                		swal({ 
							  title: "請注意...!", 
							  text: "帳號 : "+userName+" 已經有人註冊，\n請改用其他名稱，謝謝!!", 
							  type: "warning",
							  confirmButtonColor: "#DD6B55",
							  closeOnConfirm: true
							},
							function(){
								$('#input_username').css("background-color","#D6D6FF");
							}
							);

	                	}
	                },
	                error: function(XMLHttpRequest, textStatus, errorThrown) {

	                }
	            });
			}

			function checkUserMail(userMail){
				$.ajax({
	                type: "post", 
	                url: '/member_join/chkUserMail',
	                data: {
	                    mail: userMail
	                },
	                cache: false,
	                // dataType: "json",
	                success: function (response) {
	                	if(response == userMail){

	                		swal({ 
							  title: "請注意...!", 
							  text: "聯絡信箱 : "+userMail+" 已經有人使用，\n請改用其他信箱，謝謝!!", 
							  type: "warning",
							  confirmButtonColor: "#DD6B55",
							  closeOnConfirm: true
							},
							function(){
								$('#input_email').css("background-color","#D6D6FF");
							}
							);

	                	}
	                },
	                error: function(XMLHttpRequest, textStatus, errorThrown) {

	                }
	            });
			}

			function checkForm(){

				if(document.formJoin.input_username.value==""){
					swal({ 
					  title: "請注意...!", 
					  text: "請填寫帳號！", 
					  type: "warning",
					  confirmButtonColor: "#DD6B55",
					  closeOnConfirm: true
					},
					function(){
						$('#input_username').css("background-color","#D6D6FF");
					}
					);
					return false;
				}else{
					input_un=document.formJoin.input_username.value;
					//針對帳號完整性做檢查
					if(input_un.length<5 || input_un.length>12){
						swal({ 
						  title: "請注意...!", 
						  text: "帳號請填寫5至12個字元！", 
						  type: "warning",
						  confirmButtonColor: "#DD6B55",
						  closeOnConfirm: true
						},
						function(){
							$('#input_username').css("background-color","#D6D6FF");
						}
						);
						return false;
					}

					if(!(input_un.charAt(0)>="a" && input_un.charAt(0)<="z")){
						swal({ 
						  title: "請注意...!", 
						  text: "帳號第一個只能填寫英文小寫字母\"ａ\"-\"ｚ\"！", 
						  type: "warning",
						  confirmButtonColor: "#DD6B55",
						  closeOnConfirm: true
						},
						function(){
							$('#input_username').css("background-color","#D6D6FF");
						}
						);
						return false;
					}
					
						for(idx=0;idx<input_un.length;idx++){
							if(input_un.charAt(idx)>='A'&&input_un.charAt(idx)<='Z'){
								swal({ 
								  title: "請注意...!", 
								  text: "帳號不可以含有大寫字元!", 
								  type: "warning",
								  confirmButtonColor: "#DD6B55",
								  closeOnConfirm: true
								},
								function(){
									$('#input_username').css("background-color","#D6D6FF");
								}
								);
								
								return false;
							}
							if(!(( input_un.charAt(idx)>='a'&&input_un.charAt(idx)<='z')||
								(input_un.charAt(idx)>='0'&& input_un.charAt(idx)<='9')||( input_un.charAt(idx)=='_'))){
								swal({ 
								  title: "請注意...!", 
								  text: "您的帳號只能是數字,英文字母及「_」等符號,其他的符號都不能使用!", 
								  type: "warning",
								  confirmButtonColor: "#DD6B55",
								  closeOnConfirm: true
								},
								function(){
									$('#input_username').css("background-color","#D6D6FF");
								}
								);

								return false;
							}
							if(input_un.charAt(idx)=='_'&&input_un.charAt(idx-1)=='_'){
								swal({ 
								  title: "請注意...!", 
								  text: "「_」符號不可相連!", 
								  type: "warning",
								  confirmButtonColor: "#DD6B55",
								  closeOnConfirm: true
								},
								function(){
									$('#input_username').css("background-color","#D6D6FF");
								});
								return false;				
							}
						} //無檢查效果需檢查
				}
					
				if(!checkPw(document.formJoin.input_password.value, document.formJoin.input_confirmPW.value)){
					return false;
				}
				
				if(document.formJoin.input_name.value==""){
					swal({ 
							title: "請注意...!", 
							text: "請填寫姓名!", 
							type: "warning",
							confirmButtonColor: "#DD6B55",
							closeOnConfirm: true
						},
						function(){
							$('#input_name').css("background-color","#D6D6FF");
						}
					);
					return false;
				}
				
				if(document.formJoin.input_birthday.value==""){
					swal({ 
							title: "請注意...!", 
							text: "請填寫生日!", 
							type: "warning",
							confirmButtonColor: "#DD6B55",
							closeOnConfirm: true
						},
						function(){
							$('#input_birthday').css("background-color","#D6D6FF");
						}
					);
					return false;
				}
				
				if(document.formJoin.input_email.value==""){
					swal({ 
							title: "請注意...!", 
							text: "請填寫電子郵件!", 
							type: "warning",
							confirmButtonColor: "#DD6B55",
							closeOnConfirm: true
						},
						function(){
							$('#input_email').css("background-color","#D6D6FF");
						}
					);
					return false;
				}

				
				if(!checkmail(document.formJoin.input_email)){
					return false;
				}else{

					swal({ 
						title: "---確認---", 
						text: "請問填寫的會員資料無誤嗎?", 
						type: "info",
						confirmButtonColor: "#DD6B55",
						showCancelButton: true,
						confirmButtonText: '確定申請',
						cancelButton: '取消',
						cancelButtonText: '取消申請',
						closeOnConfirm: false,
						closeOnCancel: false
					},
					function(isConfirm){

						if(isConfirm){

							swal({ 
							title: "請注意...!", 
							text: "已申請!", 
							type: "success",
							confirmButtonColor: "#DD6B55",
							closeOnConfirm: true
							},
							function(){
								$('#formJoin').submit();
							}
							);
							return true;

						}else{
							swal("---確認---","已取消!","error");
							return false;
						}

					});

					return false;
				}				
				
			}			
			
			function checkPw(pw1,pw2)
			{
				if(pw1==''){
					swal({ 
						title: "請注意...!", 
						text: "密碼不可以空白!", 
						type: "warning",
						confirmButtonColor: "#DD6B55",
						closeOnConfirm: true
					},
					function(){
						$('#input_password').css("background-color","#D6D6FF");
						}
					);
					return false;
					}
					
				for(var idx=0;idx<pw1.length;idx++){
					if(pw1.charAt(idx) == ' ' || pw1.charAt(idx) == '\"'){
						swal({ 
						title: "請注意...!", 
						text: "密碼不可以含有空白或雙引號!", 
						type: "warning",
						confirmButtonColor: "#DD6B55",
						closeOnConfirm: true
						},
						function(){
							$('#input_password').css("background-color","#D6D6FF");
							}
						);
						return false;
					}
					if(pw1.length>20){
							swal({ 
								title: "請注意...!", 
								text: "密碼長度只能20個字母內!", 
								type: "warning",
								confirmButtonColor: "#DD6B55",
								closeOnConfirm: true
								},
								function(){
									$('#input_password').css("background-color","#D6D6FF");
								}
							);
							return false;
						}
					if(pw1!= pw2){
						swal({ 
							title: "請注意...!", 
							text: "密碼二次輸入不一樣,請重新輸入 !", 
							type: "warning",
							confirmButtonColor: "#DD6B55",
							closeOnConfirm: true
							},
							function(){
								$('#input_password').css("background-color","#D6D6FF");
							}
						);
						return false;
						}
					}
				return true;
			}
			
			function checkmail(myMail){
				var filter=/^([a-z0-9_\.\-])+\@([a-z0-9\-]+\.)+([a-z0-9\-]{2,4})+$/;
				if(filter.test(myMail.value)){
					return true;
				}

				swal({ 
					title: "請注意...!", 
					text: "電子郵件格式不正確!", 
					type: "warning",
					confirmButtonColor: "#DD6B55",
					closeOnConfirm: true
					},
					function(){
						$('#input＿email').css("background-color","#D6D6FF");
					}
				);
				return false;
			}

			//會員加入頁時的未登入檢查(無右側登入框時)

			function checkNaviStateWithoutRHContext(){

				swal({ 
					title: "提醒你...", 
					text: "請先申請寫會員帳號與密碼喔!之後才能使用留言板的功能...", 
					type: "info",
					confirmButtonColor: "#DD6B55",
					closeOnConfirm: true
					}
				);

				return false;		
			}