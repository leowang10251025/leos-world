$(function(){

	//登入面板"記住我"的處理
	if($('#loginRemChk').val() == "true"){
		$('#loginRem').attr('checked', true);
	}else if($('#loginRemChk').val() == "false"){
		$('#loginRem').attr('checked', false);
	}

	swal.setDefaults(
		{confirmButtonText:"我了解了" , cancelButtonText:"取消"}
	);

	if($('#mailStats').val()=="1"){
		// alert(345);
		swal({ 
		  title: "歡迎回來", 
		  text: "已寄出相關密碼~!請到信箱查收!", 
		  type: "info",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: false
		},
		function(){
			location.href="index.php";
		});
		
	}

	var userName=$('#userName').val();
	
	if($('#errMsg').val()=="1"){
		swal({ 
		  title: "糟糕了...", 
		  text: "查無此帳號!"+userName+"，請再查明你的帳號是否有誤!?\n謝謝你!", 
		  type: "warning",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: false
		},
		function(){
			location.href="index.php";
		});
		
	}

	$('#loginName').change(function(){
		$('#loginName').css("background-color","#FFFFFF");
	});
	
	$('#loginPW').change(function(){
		$('#loginPW').css("background-color","#FFFFFF");
	});


	if($('#errMsg').val()=="acc_error"){
		swal({ 
		  title: "錯誤", 
		  text: "您的帳號輸入錯誤，請重新確認!", 
		  type: "warning",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		},
		function(){
			location.href="/home/index/1";
		});
	}

	if($('#errMsg').val()=="pw_error"){
		swal({ 
		  title: "錯誤", 
		  text: "您的密碼輸入錯誤，請重新確認!", 
		  type: "warning",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		},
		function(){
			location.href="/home/index/1";
		});
	}

	if($('#errMsg').val()=="timeout"){
		swal({ 
		  title: "提醒", 
		  text: "因為您超過30分鐘未操作系統，\n已將您登出，請重新登入!", 
		  type: "info",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		});
	}

	if($('#reLogin').val()=="1"){

		swal({ 
		  title: "歡迎回來", 
		  text: "你的密碼已重新更改!請重新登入\n謝謝!", 
		  type: "info",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		},
		function(){
			$('#loginName').css("background-color","#D6D6FF");
			$('#loginPW').css("background-color","#D6D6FF");
		});
		
	}

	if($('#reLogin').val()=="2"){

		swal({ 
		  title: "歡迎加入", 
		  text: "你為新會員!請嘗試登入\n如有問題請與管理員聯絡，謝謝!", 
		  type: "info",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		},
		function(){
			$('#loginName').css("background-color","#D6D6FF");
			$('#loginPW').css("background-color","#D6D6FF");
		});
		
	}
	
});

//一般未登入檢查(有右側登入框時)
function checkSubForm(){
	if(formReg.loginName.value==""){
		swal({ 
		  title: "請注意...!", 
		  text: "請先填寫或登入會員帳號!!", 
		  type: "warning",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		},
		function(){
			$('#loginName').css("background-color","#D6D6FF");
		}
		);
		return false;
	}

	if(formReg.loginPW.value==""){
		swal({ 
		  title: "請注意...!", 
		  text: "請先填寫會員密碼或先註冊!!\n如果你忘記密碼，也請點選下方\"補寄密碼信\""+"連結，即可索取!!!", 
		  type: "warning",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		},
		function(){
			$('#loginPW').css("background-color","#D6D6FF");
		}
		);
		return false;
	}

	if(formReg.loginName.value!="" && formReg.loginPW.value!=""){
		location.href="/home/login";
	}

	return true;
}

//右上方的導覽列檢查
function checkNaviState(){

	if(document.formReg.loginName.value=="" || document.formReg.loginPW.value==""){
		swal({ 
		  title: "提醒你...", 
		  text: "請先登入或申請加入會員!!才能使用導覽列留言板的功能喔...", 
		  type: "info",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		},
		function(){
			$('#loginName').css("background-color","#D6D6FF");
			$('#loginPW').css("background-color","#D6D6FF");
		}
		);
		return false;
	}
	if(document.formReg.loginName.value!="" || document.formReg.loginPW.value!=""){
		swal({ 
		  title: "提醒你...", 
		  text: "請由頁面右側區塊的\"Login\"字樣登入會員帳號!!", 
		  type: "info",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		},
		function(){
			$('#loginName').css("background-color","#D6D6FF");
		}
		);
		return false;
	}	
	
}