$(function(){
	var imgArr = ["/images/web_logo_1.png", "/images/web_logo_2.png", "/images/web_logo_3.png"];
	animRandomExcute('#corner_img_div', '#corner_img', imgArr);
	$('#corner_img_div').hover(function(){
		shankeImage(this);
	});
});

function shankeImage(imgObj){
	$(imgObj).rotate(-15);
	$(imgObj).rotate({
     	bind: {
           mouseover : function() {
                $(this).rotate({animateTo:5});
           },
           mouseout : function() {
                $(this).rotate({animateTo:-15});
           }
     	}
	});
}

function animRandomExcute(divId, imgId, imgArr){
	var randNumAnim = Math.floor(Math.random() *10)+1;
	var randNumImg = Math.floor(Math.random() *10)+1;

	switch(randNumAnim){
		case 1:
		case 7:
			$(divId).fadeIn(3000);
		break;
		case 2:
		case 8:
			$(divId).slideToggle("slow");
		break;
		case 3:
		case 9:			
			$(divId).css("opacity", "0.2");
			$(divId).fadeTo(3000, 1.0);
		break;
		case 4:
			$(divId).toggle(3000, "swing", function(){
				$(this).addClass("w3-grayscale").css("transform", "scale(1.1)");
			});
		break;
		case 5:
			$(divId).slideToggle(3000, function(){
				$(this).addClass("w3-sepia-min").css("transform", "scale(1.1)").rotate({animateTo:-5})
			});
		break;
		case 6:
			$(divId).slideToggle(2000, function(){
				$(this).addClass("w3-grayscale").fadeOut(1000).fadeIn(1000).fadeOut(1000).fadeIn(1000);
			});
		break;
		default:
			$(divId).slideToggle("slow");		
		break;
	}

	$(imgId).attr("src", imgArr[(randNumImg%3)]);
}