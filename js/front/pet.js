$(function(){

	var loginChk = $('#loginChk').val();
	var pageName = $('#pageName').val();

	$('#banner_2_2_2').click(function(){
		location.href="/home/index/2";
		if(loginChk=='sucess'){
			location.href="/home/haveLogin/"+$('#memberLevel').val()+"/"+$('#userId').val()+"/2";
		}
	});

	$('#banner_2_2_3').click(function(){
		location.href="/home/index/3";
		if(loginChk=='sucess'){
			location.href="/home/haveLogin/"+$('#memberLevel').val()+"/"+$('#userId').val()+"/3";
		}
	});

	$('#banner_2_2_4').click(function(){
		location.href="/home/index/4";
		if(loginChk=='sucess'){
			location.href="/home/haveLogin/"+$('#memberLevel').val()+"/"+$('#userId').val()+"/4";
		}
	});

	$('#banner_2_2_5').click(function(){

		if(typeof loginChk === "undefined"){ //當值為undefined
			checkNaviStateMyFavorist();
		}else{
			if(loginChk == "sucess"){
				location.href="/message_board/index/"+$('#userId').val();
			}
		}

	});

	$('#banner_2_3').click(function(){
		location.href="/home/index/1";
		if($('#loginChk').val()=='sucess'){
			location.href="/home/haveLogin/"+$('#memberLevel').val()+"/"+$('#userId').val()+"/1";
		}
	});
	
});

//我最愛的寵物頁未登入檢查(有右側登入框時)

function checkNaviStateMyFavorist(){

		swal({ 
		  title: "請注意...!", 
		  text: "請先登入或申請加入會員!!才能使用導覽列留言板的功能喔...", 
		  type: "warning",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		});

		return false;
}