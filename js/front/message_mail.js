$(function(){
	// $("#formSubmit").click(function(){
	// 	checkForm();
	// });
});

function checkForm(){
	var mailFromName=$('#mailFromName').val();
	var mailFrom=$('#mailFrom').val();
	var input_content = CKEDITOR.instances.input_content.getData();

	if(mailFromName == ""){
		swal(
		{ 
			title: "請注意...!", 
			text: "請填寫您的暱稱!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
		},
		function(){
			$('#mailFromName').css("background-color","#D6D6FF");
		}
		);
		return false;
	}
	
	if(mailFrom == ""){
		swal({ 
			title: "請注意...!", 
			text: "請填寫您的聯絡信箱!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
		},
		function(){
			$('#mailFrom').css("background-color","#D6D6FF");
		}
		);
		return false;
	}else{
		// alert(checkmail(mailFrom));
		if(checkmail(mailFrom)){ //待處理
			swal({ 
				title: "請注意...!", 
				text: "填寫信箱格式有誤!請再次核對!", 
				type: "warning",
				confirmButtonColor: "#DD6B55",
				closeOnConfirm: true
			},
			function(){
				$('#mailFrom').css("background-color","#D6D6FF");
			}
			);
			return false;
		}

	}
	
	if(input_content == ""){
		swal({ 
			title: "請注意...!", 
			text: "請填寫詢問的內容!12121", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
		},
		function(){
			$('#input_content').css("background-color","#D6D6FF");
		}
		);
		return false;
	}else{

		swal(
		{ 
			title: "---確認---", 
			text: "此信件內容正確嗎?即將寄送出去了...", 
			type: "info",
			confirmButtonColor: "#DD6B55",
			showCancelButton: true,
			confirmButtonText: '確定寄送',
			cancelButton: '取消',
			cancelButtonText: '取消寄送',
			closeOnConfirm: false,
			closeOnCancel: false
		},
		function(isConfirm){

			if(isConfirm){

				swal({ 
				title: "請注意...!", 
				text: "已寄送!", 
				type: "success",
				confirmButtonColor: "#DD6B55",
				closeOnConfirm: true
				},
				function(){
					$('#formMessageMail').submit();
				}
				);
				return true;

			}else{
				swal("---確認---","已取消!","error");
				return false;
			}

		});

	}

}