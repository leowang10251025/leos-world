$(function(){

	//留言處理
	var dataStatus=$('#dataStatus').val();

	if(dataStatus=="isUpdate"){
		
		swal({ 
		  title: "通知您...!", 
		  text: "留言寫入成功!歡迎繼續留言~謝謝!", 
		  type: "info",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		});
		return false;

	}else if(dataStatus=="notUpdate"){
		
		swal({ 
		  title: "請注意...!", 
		  text: "留言寫入失敗!請跟系統管理者反映~抱歉!", 
		  type: "warning",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		});
		return false;
	}

	//寄信狀態處理
	if($('#sendStatus').val() == "sendSuccess"){

		swal({ 
			title: "通知您...!", 
			text: "您聯絡管理員的信件已寄出，\n謝謝您的來信!!", 
			type: "info",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){

			}
		);

		return false;

	}else if($('#sendStatus').val() == "sendFail"){

		swal({ 
			title: "請注意...!", 
			text: "非常抱歉，您聯絡管理員的信件寄送出了些問題，\n請聯繫系統人員處理...，感謝您!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){

			}
		);

		return false;

	}

	//登入留言板管理頁處理
	var loginStatus = $('#loginStatus').val();

	if(loginStatus == "loginSucess"){

		swal({ 
		  title: "提醒您...!", 
		  text: "登入成功!請使用管理頁面功能!", 
		  type: "info",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		});

		return false;

	}else if(loginStatus == "loginFail"){

		swal({ 
		  title: "請注意...!", 
		  text: "登入失敗!請先確認你的密碼或帳號正確性!", 
		  type: "warning",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		},
		function(){
			location.href = "/message_board/index/"+$('#userId').val()+"/1/0/notLogin/0";
		});

		return false;
	}else if(loginStatus == "logoutSucess"){

		swal({ 
		  title: "提醒您...!", 
		  text: "登出成功!如需使用功能請重新登入!", 
		  type: "info",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		});

		return false;
	}
	

	$('#username').change(function(){
		checkAdminAcc($('#username').val());
	});

	$('#passwd').change(function(){
		if($('#username').val() != ""){
			checkAdminPw($('#username').val(), $('#passwd').val());
		}
	});

});

//留言板登入管理
function checkSubForm(){

	if($('#username').val() == ""){

		swal({ 
		  title: "請注意...!", 
		  text: "請填入留言板管理帳號!!", 
		  type: "warning",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		},
		function(){
			$('#username').css("background-color","#D6D6FF");
		});

		return false;
	}

	if($('#passwd').val() == ""){

		swal({ 
		  title: "請注意...!", 
		  text: "請填入留言板管理密碼!!", 
		  type: "warning",
		  confirmButtonColor: "#DD6B55",
		  closeOnConfirm: true
		},
		function(){
			$('#passwd').css("background-color","#D6D6FF");
		});

		return false;
	}

}


function checkAdminAcc(admin_acc){
	$.ajax({
        type: "post", 
        url: '/message_board/checkAdminAcc',
        data: {
            admin_acc: admin_acc
        },
        cache: false,
        // dataType: "json",
        success: function (response) {

        	if(response == ""){
        		swal({ 
				  title: "請注意...!", 
				  text: "您輸入的管理者帳號有誤或沒填入資料，\n請確認輸入是否正確，謝謝!!", 
				  type: "warning",
				  confirmButtonColor: "#DD6B55",
				  closeOnConfirm: true
				},
				function(){
					$('#username').css("background-color","#D6D6FF");
				}
				);
        	}else{
        		$('#chkLoginName').val("success");
        	}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}

function checkAdminPw(admin_acc, admin_pw){	
	$.ajax({
        type: "post", 
        url: '/message_board/checkAdminPw',
        data: {
            admin_acc: admin_acc,
            admin_pw: admin_pw
        },
        cache: false,
        // dataType: "json",
        success: function (response) {
        	if(response == "notEqual"){
        		swal({ 
				  title: "請注意...!", 
				  text: "您輸入的管理者密碼:\""+admin_pw+"\"有誤，\n請確認輸入是否正確，謝謝!!", 
				  type: "warning",
				  confirmButtonColor: "#DD6B55",
				  closeOnConfirm: true
				},
				function(){
					$('#passwd').css("background-color","#D6D6FF");
				}
				);
        	}else{
        		$('#chkLoginPw').val("success");
        	}
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}