$(function() {
    var addEduNum = 1;
    var addWorkExpNum = 1;
    var maxEduNum = 5;
    var validWorkingTimeArr = [];
    var validEducationTimeArr = [];
    var workingTermStartArr = [];
    var workingTermEndArr = [];
    var workingTermStartEditArr = [];
    var workingTermEndEditArr = [];
    var educationTermStartArr = [];
    var educationTermEndArr = [];
    var totalTermMonthArr = [];
    var totalTermMonthEditArr = [];
    var basicId = $("#basicId").val();
    var userId = $('#userId').val();
    var memberLevel = $('#memberLevel').val();
    var birthDayVal = $('#birthDayVal').val();

    //當"最後學經歷"的相關欄位改變時
    $('#school-name-1').on("change", function() {
        $('#highest-education-name').text($(this).val());
    });
    $('#dept-name-1').on("change", function() {
        $('#dept-highest-education').text($(this).val());
    });
    $('#company-name-1').on("change", function() {
        $('#last-company-name').text($(this).val());
    });
    $('#job-name-1').on("change", function() {
        $("#last-job-name").text($(this).val());
    });

    //居住鄉鎮市選項改變時
    $("#living-city-position").on("change", function() {
        let optionCount = $("#living-district-position").find("option");
        if (optionCount.length != 0) {
            $('#living-district-position option').remove();
        }
        selectDistrict('#living-district-position', $(this).val(), "請選擇居住的行政區");
    });

    //個人資料:年齡欄位年份等選擇改變時
    birthSelector("year", "#birth-year-selector", "#birth-month-selector", birthDayVal);
    birthSelector("month", "#birth-year-selector", "#birth-month-selector", birthDayVal);

    ageGenarator("#birth-year-selector", "#birth-year-selector", "#birth-month-selector", "#birth-day-selector");
    ageGenarator("#birth-month-selector", "#birth-year-selector", "#birth-month-selector", "#birth-day-selector");
    ageGenarator("#birth-day-selector", "#birth-year-selector", "#birth-month-selector", "#birth-day-selector");

    //個人資料:居住地址前段的生成
    addressPreGenerator(0, "#living-city-position option:selected", "#living-district-position option:selected");
    addressPreGenerator(1, "#living-city-position option:selected", "#living-district-position option:selected");

    //軍種顯示(選擇役畢後)
    $("#military-service").hide();
    $("input[name='military-service-status']").on("click", function() {
        if ($(this).val() == 2) {
            $("#military-service").show();
        } else {
            $("#military-service").hide();
        }
    });
    //免役的原因顯示
    $("#exempt-service-reason-1").hide();
    $("#exempt-service-reason-2").hide();
    $("input[name='military-service-status']").on("click", function() {
        if ($(this).val() == 3) {
            $("#exempt-service-reason-1").show();
            $("#exempt-service-reason-2").show();
        } else {
            $("#exempt-service-reason-1").hide();
            $("#exempt-service-reason-2").hide();
        }
    });
    //其他聯絡方式顯示
    otherOptionDisplay("#other-contact-method", "input[name*='contact-method']", 'contact-method-3');
    //其他駕照類型顯示
    otherOptionDisplay("#other-driving-licence", "input[name*='driving-licence']", 'driving-licence-3');
    //其他交通工具顯示
    otherOptionDisplay("#other-driving-tool", "input[name*='driving-tool']", 'driving-tool-3');

    //工作總年資計算(未新增工作經歷時)
    totalTermCaculateBeforeAdd(totalTermMonthArr, "startTime", "#working-term-start-1", "#working-term-end-1");
    totalTermCaculateBeforeAdd(totalTermMonthArr, "endTime", "#working-term-start-1", "#working-term-end-1");

    //新增學歷資料
    $('#add-education').on("click", function() {
        let data = [];
        if (basicId != "") { //編輯時，各項目需的ID尾碼需加上原本的
            addEduNum = parseInt($('div[id*=edu-info]').length);
        }
        addEduNum++;
        data.push(addEduNum);
        $('#education').attr("class", "collapse show");
        $.post('/autobiography/addEducation', { data }, function(html) {
            $('#education').append(html);
        });

        if (addEduNum == maxEduNum) {
            swal({
                title: "提醒您...!",
                text: "新增的學歷最多不超過5筆!~謝謝!",
                type: "info",
                confirmButtonColor: "#DD6B55",
                closeOnConfirm: true
            });
        }

        $.each($("div[class*='remove-education']"), function(index, el) {
            let id = el.getAttribute('id');
            let idNum = parseInt(id.charAt(id.length - 1));
            $("#remove-education-" + idNum).on("click", function() {
                $("#edu-info-" + idNum).remove();
            });
        });

        //學歷的時間長度計算與檢查
        educationTermTextGenerator('input[id*="learning-term-start"]');
        educationTermTextGenerator('input[id*="learning-term-end"]');
    });

    //新增工作經歷資料
    $('#add-work-experience').on("click", function() {
        let data = [];
        if (basicId != "") { //編輯時，各項目需的ID尾碼需加上原本的
            addWorkExpNum = parseInt($('div[id*=work-experience-info]').length);
        }
        addWorkExpNum++;
        data.push(addWorkExpNum);
        $('#work-experience').attr("class", "collapse show");
        $.post('/autobiography/addWorkExperience', { data }, function(html) {
            $('#work-experience').append(html);
        });

        $.each($("div[class*='remove-work-experience']"), function(index, el) {
            let id = el.getAttribute('id');
            let idNum = parseInt(id.charAt(id.length - 1));
            $("#remove-work-experience-" + idNum).on("click", function() {
                $("#work-experience-info-" + idNum).remove();
            });
        });

        //當工作經歷的"工作地點(城市)"欄位改變時
        $('select[id*="working-position-city"]').each(function(i, el) {
            $("#" + el.getAttribute("id")).on("change", function() {
                let id = $(this).attr('id');
                let idNum = parseInt(id.charAt(id.length - 1));
                let optionCount = $('select[id="working-position-district-' + idNum + '"]').find("option");
                if (optionCount.length != 0) {
                    $('select[id="working-position-district-' + idNum + '"] option').remove();
                }
                selectDistrict('#working-position-district-' + idNum, $(this).val(), "請選擇工作的行政區");
            });
        });

        //工作經歷的時間長度計算與檢查
        workingTermTextGenerator('input[id*="working-term-start"]');
        workingTermTextGenerator('input[id*="working-term-end"]');

        //工作總年資計算(新增工作經歷時)
        totalTermCaculateAfterAdd(totalTermMonthArr, "startTime", workingTermStartArr, workingTermEndArr);
        totalTermCaculateAfterAdd(totalTermMonthArr, "endTime", workingTermStartArr, workingTermEndArr);

    });

    //學歷的時間長度計算與檢查(未新增學歷時)
    educationTermTextGenerator("#learning-term-start-1");
    educationTermTextGenerator('#learning-term-end-1');

    //工作經歷的時間長度計算與檢查(未新增工作經歷時)
    workingTermTextGenerator("#working-term-start-1");
    workingTermTextGenerator("#working-term-end-1");

    //輔助說明區塊呈現
    $("#headingOne, #headingTwo, #headingThree").hover(
        function() {
            $("#pop_assist_dialog").show();
            let id = $(this).attr("id");

            let headingNum = 0;
            switch (id) {
                case "headingOne":
                    headingNum = 1;
                    $("#pop_assist_dialog").css("background-image", "url(/images/pop_menu_bg_6_1.png)");
                    break;
                case "headingTwo":
                    headingNum = 2;
                    $("#pop_assist_dialog").css("background-image", "url(/images/pop_menu_bg_6_2.png)");
                    break;
                case "headingThree":
                    headingNum = 3;
                    $("#pop_assist_dialog").css("background-image", "url(/images/pop_menu_bg_6_3.png)");
                    break;
            }
            $.post("/autobiography/showAssistAreaTitle", { "headingNum": headingNum }, function(res) {
                $("#assist-area-title").text(res);
            });
            $.post("/autobiography/showAssistAreaText", { "headingNum": headingNum }, function(res) {
                $("#assist-area-text").text(res);
            });
        },
        function() {
            $("#pop_assist_dialog").hide();
        }
    );

    //表單送出時
    $(".btn-submit").on("click", function(e) {
        let resultStr = '';
        let resultValid = false;
        /** 資料檢查 **/
        //工作經歷:時間區段開始與結束
        resultStr += timeRangeCheck("working", validWorkingTimeArr, 'input[id*="working-term-start"]:not([id*="data"])', workingTermStartArr, 'input[id*="working-term-end"]:not([id*="data"])', workingTermEndArr, resultStr);
        //學歷:時間區段開始與結束
        resultStr += timeRangeCheck("education", validEducationTimeArr, 'input[id*="learning-term-start"]:not([id*="data"])', educationTermStartArr, 'input[id*="learning-term-end"]:not([id*="data"])', educationTermEndArr, resultStr);

        if (!validWorkingTimeArr.includes(false) && !validEducationTimeArr.includes(false)) {
            resultValid = true;
        }

        if (resultValid) {
            let msg = '';
            let postUrl = '/autobiography/edit';
            let submitType = "新增";

            if (basicId != "" || basicId != undefined) {
                postUrl += '/' + basicId;
                submitType = "修改";
            }

            if (basicId == 1) {
                $("#pop_autobiography_login").fadeIn(3000);
                $("#login-submit").on("click", function() {
                    $.post("/autobiography/passwordValid", {
                        "account": $("#login-account").val(),
                        "password": $("#login-password").val()
                    }, function(res) {
                        if (res == 'sucess') {
                            swal({
                                title: "提醒您...!",
                                text: "您已被允許再次嘗試修改...",
                                type: "info",
                                confirmButtonColor: "#DD6B55",
                                closeOnConfirm: true
                            }, function() {
                                formSubmitAndValid(postUrl, "#form-autobio", msg, submitType, '');
                            });
                        } else {
                            swal({
                                title: "請注意...!",
                                text: res,
                                type: "warning",
                                confirmButtonColor: "#DD6B55",
                                closeOnConfirm: true
                            }, function() {
                                return false;
                            });
                        }
                    });
                });
            } else {
                formSubmitAndValid(postUrl, "#form-autobio", msg, submitType, '');
            }
            return false;
        } else {
            if (resultStr != "") { //前端檢查
                swal({
                    title: "請注意...!",
                    text: resultStr,
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: true
                });
                return false;
            }
        }
    });

    //新增一筆的轉址
    $(".btn-add-data").on("click", function() {
        window.open('/autobiography', '新的一份履歷');
    });

    //編輯時資料呈現
    let educationData = $("input[name='educationData']");
    let workingExpData = $("input[name='workingExpData']");

    if (basicId != "") {
        // ** 基本資料部分 **
        if (birthDayVal != "" || birthDayVal != undefined) {
            $.post('/autobiography/selectDay', { 'selectYear': $("#birth-year-selector").val(), 'selectMonth': $("#birth-month-selector").val() }, function(res) {
                if (!isNaN(parseInt(res))) {
                    let html = "";
                    for (let day = 1; day <= res; day++) {
                        if (parseInt(birthDayVal) == day) {
                            html = '<option value="' + day + '" selected>' + day + '</option>';
                        } else {
                            html = '<option value="' + day + '">' + day + '</option>';
                        }
                        $("#birth-day-selector").append(html);
                    }
                }
            });
        }
        if ($("#contact-method-3").attr("checked") == "checked") {
            $("#other-contact-method").show();
        } else {
            $("#other-contact-method").hide();
            $("#other-contact-method").val("");
        }

        if ($("#driving-licence-3").attr("checked") == "checked") {
            $("#other-driving-licence").show();
        } else {
            $("#other-driving-licence").hide();
            $("#other-driving-licence").val("");
        }

        if ($("#driving-tool-3").attr("checked") == "checked") {
            $("#other-driving-tool").show();
        } else {
            $("#other-driving-tool").hide();
            $("#other-driving-tool").val("");
        }

        if ($("input[name='military-service-status'][value='2']").attr("checked") == "checked") {
            $("#military-service").show();
            $("#exempt-service-reason-2").val("");
        }
        if ($("input[name='military-service-status'][value='3']").attr("checked") == "checked") {
            $("#exempt-service-reason-1").show();
            $("#exempt-service-reason-2").show();
        }

        // ** 學經歷部分 **
        let districtIdArr = [];
        $('input[id*=working-position-district]').each(function(i, e) {
            districtIdArr[(i + 1)] = $(this).val();
        });

        //學歷預設資料
        educationData.each(function(i, e) {
            let eid = $("#" + e.getAttribute('id')).attr("id");
            let elementId = eid.substr(0, eid.length - 5);
            let elementValue = $("#" + e.getAttribute('id')).val();
            if (eid.indexOf("graduate-status") > -1) {
                let checkedElement = $("input[name='" + elementId + "']:eq(" + (elementValue - 1) + ")");
                checkedElement.attr("checked", true);
            } else {
                $("#" + elementId).val(elementValue);
            }
        });

        //工作經歷預設資料
        workingExpData.each(function(i, e) {
            let eid = $("#" + e.getAttribute('id')).attr("id");
            let elementId = eid.substr(0, eid.length - 5);
            let elementValue = $("#" + e.getAttribute('id')).val();
            let tmpArr = [];
            $("#" + elementId).val(elementValue);

            if (eid.indexOf("working-position-city") > -1) {
                let idNum = elementId.substr(elementId.length - 1, elementId.length);
                $.post('/autobiography/selectDistrict', { 'selectId': elementValue }, function(res) {
                    tmpArr = JSON.parse(res);
                    if ($('#working-position-district-' + idNum).find("option").length > 0) {
                        $('#working-position-district-' + idNum + " option").remove();
                    }
                    $('#working-position-district-' + idNum).append("<option value=" + 0 + ">請選擇工作的行政區</option>");
                    $.each(tmpArr, function(i, v) {
                        let html = '';
                        if (districtIdArr[idNum] == v.id) {
                            html = '<option value="' + v.id + '" selected>' + v.name + "</option>";
                        } else {
                            html = '<option value="' + v.id + '">' + v.name + "</option>";
                        }
                        $('#working-position-district-' + idNum).append(html);
                    });
                });
            }
        });

        //工作經歷:當選擇城市選項改變時
        $("select[id*='working-position-city']").each(function(i, e) {
            $(this).on("change", function() {
                let selectorId = "select[id*='working-position-district-" + (i + 1) + "']";
                let selectedId = $(this).val();
                $(selectorId).find("option").remove();
                selectDistrict(selectorId, selectedId, "請選擇工作的行政區域");
            });
        });

        let workingTermStartElements = $("input[id*=working-term-start]:not([id*=data])");
        let workingTermEndElements = $("input[id*=working-term-end]:not([id*=data])");
        workingTermStartElements.each(function(i, e) {
            workingTermStartEditArr.push($("#" + e.getAttribute("id")).val());
        });
        workingTermEndElements.each(function(i, e) {
            workingTermEndEditArr.push($("#" + e.getAttribute("id")).val());
        });

        //工作總年資計算(編輯工作經歷時)
        totalTermCaculateAsEdit(totalTermMonthEditArr, workingTermStartEditArr, workingTermEndEditArr);
    }

    //履歷頁面登出
    $("#btn-logout").on("click", function() {
        $.post("/autobiography/logoutAutobiographyEdit", {}, function(res) {
            if (res == "sucess1") { //會員身分還未登出
                location.href = "/home/haveLogin/" + memberLevel + "/" + userId + "/1";
            } else if (res == "sucess2") { //會員身分已登出
                location.href = "/home";
            } else {
                swal({
                    title: "請注意...!",
                    text: "登出動作失敗，請洽系統管理員!",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: true
                }, function() {
                    return false;
                });
            }
        });
    });
});

function birthSelector(firstSelectKind, selectId1, selectId2) {

    let elementId = '';
    switch (firstSelectKind) {
        case "year":
            elementId = selectId1;
            break;
        case "month":
            elementId = selectId2;
            break;
    }

    $(elementId).on("change", function() {
        if ($("#birth-day-selector").find("option").length > 1) {
            $("#birth-day-selector option").remove();
        }
        $.post('/autobiography/selectDay', { 'selectYear': $(selectId1).val(), 'selectMonth': $(selectId2).val() }, function(res) {
            if (!isNaN(parseInt(res))) {
                let html = "";
                for (let day = 1; day <= res; day++) {
                    html = '<option value="' + day + '">' + day + '</option>';
                    $("#birth-day-selector").append(html);
                }
            } else {
                let focusElementId = "";
                let isFocus = false;
                if (res == '請選擇出生年份!') {
                    focusElementId = "#birth-year-selector";
                    isFocus = true;
                }
                if (res == '請選擇出生月份!') {
                    focusElementId = "#birth-month-selector";
                    isFocus = true;
                }

                swal({
                    title: "請注意...!",
                    text: res,
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: true
                }, function() {
                    if (isFocus) {
                        $(focusElementId).focus();
                        $(focusElementId).css("background-color", "#b3ccff");
                    }
                });
                return false;
            }
        });
    });
}

function selectDistrict(selectorId, selectedId, defaultItemText) {
    let tmpArr = [];
    $.post('/autobiography/selectDistrict', { 'selectId': selectedId }, function(res) {
        tmpArr = JSON.parse(res);
        $(selectorId).append("<option value=" + 0 + ">" + defaultItemText + "</option>");
        $.each(tmpArr, function(i, v) {
            $(selectorId).append('<option value="' + v.id + '">' + v.name + "</option>");
        });
    });
}

function ageGenarator(changeElementId, selectYearId, selectMonthId, selectDayId) {
    $(changeElementId).on("change", function() {
        let selectYear = $(selectYearId).val();
        let selectMonth = $(selectMonthId).val();
        let selectDay = $(selectDayId).val();
        let nowTime = new Date();
        if (selectYear != 0 && selectMonth != 0 && selectDay != 0) {
            if (selectDay == null) { selectDay = 1; }
            let birthDay = new Date(selectYear + "/" + selectMonth + "/" + selectDay);
            $("#age").val(parseInt(Math.abs(nowTime - birthDay) / (86400000 * 365.25)));
        }
    });
}

function addressPreGenerator(elementOrder, changeElementId1, changeElementId2) {
    let changeElement = "";
    switch (elementOrder) {
        case 0:
            changeElement = "#living-city-position"
            break;
        case 1:
            changeElement = "#living-district-position"
            break;
    }

    $(changeElement).on("change", function() {
        let el1 = $(changeElementId1);
        let el2 = $(changeElementId2);
        if (el1.val() != 0) {
            $("#address-1").val(el1.text());
            if (el2.val() == undefined) {
                $("#address-2").val($("#living-district-position option:first").text());
            } else {
                $("#address-2").val(el2.text());
            }
        }
    });
}

function otherOptionDisplay(displayElementId, relatedGroupElementId, clickElementId) {
    $(displayElementId).hide();
    $(relatedGroupElementId).each(function() {
        $(this).on("change", function(event) {
            if ($(this).val() == 3) {
                if (document.getElementById(clickElementId).checked) {
                    $(displayElementId).show();
                } else {
                    $(displayElementId).hide();
                }
            } else {
                $(displayElementId).hide();
            }
        })
    });
}

function workingTermTextGenerator(elementId) {
    $(elementId).each(function(i, el) {
        let id = el.getAttribute('id');
        $("#" + id).on("change", function() {
            let idNum = parseInt(id.charAt(id.length - 1));
            let workingTimeStart = $("#working-term-start-" + idNum).val();
            let workingTimeEnd = $("#working-term-end-" + idNum).val();
            let notice = timeRangeValiator("working", workingTimeStart, workingTimeEnd, idNum);
            if (notice == "") {
                let term = workingTermCaculator(workingTimeStart, workingTimeEnd);
                $("#working-time-range-" + idNum).text(term);
            } else {
                $("#working-time-range-" + idNum).text(notice);
            }
        });
    });
}

function educationTermTextGenerator(elementId) {
    $(elementId).each(function(i, el) {
        let id = el.getAttribute('id');
        $("#" + id).on("change", function() {
            let idNum = parseInt(id.charAt(id.length - 1));
            let learningTimeStart = $("#learning-term-start-" + idNum).val();
            let learningTimeEnd = $("#learning-term-end-" + idNum).val();
            let notice = timeRangeValiator("education", learningTimeStart, learningTimeEnd, idNum);
            if (notice == "") {
                let term = workingTermCaculator(learningTimeStart, learningTimeEnd);
                $("#education-time-range-" + idNum).text(term);
            } else {
                $("#education-time-range-" + idNum).text(notice);
            }
        });
    });
}

function workingTermCaculator(startDate, endDate) {
    let startDateArr = startDate.split("-");
    let endDateArr = endDate.split("-");
    let newStartDate = new Date(startDateArr[1] + "/" + startDateArr[2] + "/" + startDateArr[0]);
    let newEndDate = new Date(endDateArr[1] + "/" + endDateArr[2] + "/" + endDateArr[0]);
    let termMonth = parseInt(Math.abs(newEndDate - newStartDate) / (86400000 * 30));
    return termMonthToYear(termMonth);
}

function workingTotalTermCaculator(startDate, endDate) {
    let termMonth = 0;
    if (startDate != undefined && endDate != undefined) {
        let startDateArr = startDate.split("-");
        let endDateArr = endDate.split("-");
        let newStartDate = new Date(startDateArr[1] + "/" + startDateArr[2] + "/" + startDateArr[0]);
        let newEndDate = new Date(endDateArr[1] + "/" + endDateArr[2] + "/" + endDateArr[0]);
        termMonth = parseInt(Math.abs(newEndDate - newStartDate) / (86400000 * 30));
    }
    return termMonth;
}

function totalTermCaculateBeforeAdd(totalTermMonthArr, checkTerm, targetStartId, targetEndId) {

    let elementId = '';

    switch (checkTerm) {
        case "startTime":
            elementId = targetStartId;
            break;
        case "endTime":
            elementId = targetEndId;
            break;
    }

    $(elementId).on("change", function() {

        let startTime = $(targetStartId).val();
        let endTime = $(targetEndId).val();
        let timeType = "";
        let totalMonth = 0;

        switch (checkTerm) {
            case "startTime":
                timeType = endTime;
                break;
            case "endTime":
                timeType = startTime;
                break;
        }

        if (timeType != undefined || timeType != "") {
            totalTermMonthArr[0] = workingTotalTermCaculator(startTime, endTime);
            totalTermMonthArr.forEach(element => totalMonth += element);
            $("#working-experince-term").text(termMonthToYear(totalMonth));
            $("#total-term").text(termMonthToYear(totalMonth));
        }
    });
}

function totalTermCaculateAfterAdd(totalTermMonthArr, checkTerm, workingTermStartArr, workingTermEndArr) {

    let elementId = '';
    let targetArr1 = [];
    let targetArr2 = [];

    switch (checkTerm) {
        case "startTime":
            elementId = 'input[id*="working-term-start"]';
            targetArr1 = workingTermStartArr;
            targetArr2 = workingTermEndArr;
            break;
        case "endTime":
            elementId = 'input[id*="working-term-end"]';
            targetArr1 = workingTermEndArr;
            targetArr2 = workingTermStartArr;
            break;
    }

    $(elementId).each(function(i, el) {
        if (i > 0) {
            let item = $("#" + el.getAttribute("id"));
            item.on("change", function() {
                let totalMonth = 0;
                if (item.val() != "") {
                    targetArr1[i] = item.val();
                }
                if (targetArr2[i] != undefined || targetArr2[i] != "") {
                    totalTermMonthArr[i] = workingTotalTermCaculator(targetArr1[i], targetArr2[i]);
                }
                totalTermMonthArr.forEach(element => totalMonth += element);
                $("#working-experince-term").text(termMonthToYear(totalMonth));
                $("#total-term").text(termMonthToYear(totalMonth));
            });
        }
    });
}

function totalTermCaculateAsEdit(totalTermMonthArr, workingTermStartArr, workingTermEndArr) {

    let totalMonth = 0;
    $.each(workingTermStartArr, function(index, val) {
        if (workingTermEndArr[index] != undefined || workingTermEndArr[index] != "") {
            totalTermMonthArr[index] = workingTotalTermCaculator(val, workingTermEndArr[index]);
        }
    });
    $.each(totalTermMonthArr, function(index, val) {
        totalMonth += val;
    });

    $("#working-experince-term").text(termMonthToYear(totalMonth));
    $("#total-term").text(termMonthToYear(totalMonth));
}

function termMonthToYear(termMonth) {
    let termYear = parseFloat(Math.abs(termMonth) / (12));
    let termArr = termYear.toString().split(".");
    let strMonth = termMonth - (parseInt(termArr[0]) * 12);
    let result = "";
    if (!isNaN(termArr[0]) && !isNaN(strMonth)) {
        result = termArr[0] + " 年 " + strMonth + " 個月";
    } else {
        result = "0 年 0 個月";
    }
    return result;
}

function timeRangeValiator(type, timeStart, timeEnd, idNum = 0) {
    let noticeMsg = "";
    let typeStr = "";
    let resultStr = "";

    switch (type) {
        case "working":
            typeStr = "工作經歷";
            break;
        case "education":
            typeStr = "學歷";
            break;
    }

    if (idNum != 0) {
        resultStr = typeStr + idNum;
    } else {
        resultStr = typeStr;
    }

    if (timeStart != "") {
        if (timeEnd != "") {
            if (Date.parse(timeStart).valueOf() < Date.parse(timeEnd).valueOf()) {
                let term = workingTermCaculator(timeStart, timeEnd);
                $("#working-time-range-" + idNum).text(term);
            } else {
                noticeMsg += resultStr + "的結束時間不得小於開始時間!";
            }
        } else {
            noticeMsg += resultStr + "的結束時間不得為空!";
        }
    } else {
        noticeMsg += resultStr + "的開始時間不得為空!";
    }
    return noticeMsg;
}

function timeRangeCheck(type, validArr, termStartIds, termStartArr, termEndIds, termEndArr, resultStr) {

    let elementStartId = "";
    let elementEndId = "";
    let notice = "";
    let valid = false;

    while (validArr.length) {
        validArr.pop(); //檢查結果陣列初始清空
    }

    termStartArr = $(termStartIds);
    termEndArr = $(termEndIds);

    switch (type) {
        case "working":
            elementStartId = "#working-term-start-";
            elementEndId = "#working-term-end-";
            break;
        case "education":
            elementStartId = "#learning-term-start-";
            elementEndId = "#learning-term-end-";
            break;
    }

    $.each(termStartArr, function(i, el) {
        let id = el.getAttribute('id');
        let idNum = parseInt(id.charAt(id.length - 1));
        let timeStart = $(elementStartId + idNum).val();
        let timeEnd = $(elementEndId + idNum).val();

        notice += timeRangeValiator(type, timeStart, timeEnd, idNum);

        if (notice == "") {
            valid = true;
        } else {
            valid = false;
            resultStr += notice + "\n";
        }
        validArr.push(valid);
    });

    return resultStr;
}

function checkNaviState() {

    if (document.formReg.loginName.value == "" || document.formReg.loginPW.value == "") {
        swal({
                title: "提醒你...",
                text: "請先登入或申請加入會員!!才能使用導覽列留言板的功能喔...",
                type: "info",
                confirmButtonColor: "#DD6B55",
                closeOnConfirm: true
            },
            function() {
                $('#loginName').css("background-color", "#D6D6FF");
                $('#loginPW').css("background-color", "#D6D6FF");
            }
        );
        return false;
    }
    if (document.formReg.loginName.value != "" || document.formReg.loginPW.value != "") {
        swal({
                title: "提醒你...",
                text: "請由頁面右側區塊的\"Login\"字樣登入會員帳號!!",
                type: "info",
                confirmButtonColor: "#DD6B55",
                closeOnConfirm: true
            },
            function() {
                $('#loginName').css("background-color", "#D6D6FF");
            }
        );
        return false;
    }

}

function formSubmitAndValid(postUrl, sendFormId, msg, submitType, directUrl) {
    $.post(postUrl, $(sendFormId).serialize(), function(res) {
        switch (res) {
            case "fail":
                msg = submitType + "資料失敗，請洽系統管理員!";
                break;
            case "illegal":
                msg = "非法資料傳遞!";
                break;
            default:
                directUrl = '/autobiography/index/' + res;
                msg = submitType + "資料成功!!";
                break;
        }

        swal({
                title: "請注意...!",
                text: msg,
                type: "warning",
                confirmButtonColor: "#DD6B55",
                closeOnConfirm: true
            },
            function() {
                if (directUrl != '') {
                    location.href = directUrl;
                    $("#pop_autobiography_login").fadeOut(1500);
                }
            }
        );
    });
}