$(function(){

	if($('#sendStatus').val() == "sendSuccess"){

		swal({ 
			title: "通知您...!", 
			text: "您聯絡管理員的信件已寄出，\n謝謝您的來信!!", 
			type: "info",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){

			}
		);

		return false;

	}else if($('#sendStatus').val() == "sendFail"){

		swal({ 
			title: "請注意...!", 
			text: "非常抱歉，您聯絡管理員的信件寄送出了些問題，\n請再寄送一次或以電話、留言板告知此問題...", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
			},
			function(){

			}
		);

		return false;

	}

  $("#next-step").on("click", function(){
    if($('#numPage').val() == $('#totalPages').val()){
      swal({ 
        title: "提醒您...!", 
        text: "這是最後一頁資料了喔!!", 
        type: "warning",
        confirmButtonColor: "#DD6B55",
        closeOnConfirm: true
        },
        function(){
  
        }
      );
      return false;
    }
  });

  $("#previous-step").on("click", function(){
    if($('#numPage').val() == 1){
      swal({ 
        title: "提醒您...!", 
        text: "這是第一頁資料了喔!!", 
        type: "warning",
        confirmButtonColor: "#DD6B55",
        closeOnConfirm: true
        },
        function(){
  
        }
      );
      return false;
    }
  });

});