			function checkForm(x){

					var Str=document.getElementById(x).value;
					if(Str=="solve"){
						swal({ 
							  title: "恭喜你...!", 
							  text: "找到解決方法了!(即將返回原頁面)", 
							  type: "info",
							  showCancelButton: true,
							  confirmButtonColor: "#DD6B55",
							  cancelButtonText: "不，等等",
							  confirmButtonText: "好的",
							  closeOnConfirm: false,
							  closeOnCancel: false
							},
							function(isConfirm){
								if(isConfirm){
									swal({ 
									  title: "準備...", 
									  text: "回原本瀏覽頁瞜!", 
									  type: "info",
									  confirmButtonColor: "#DD6B55",
									  confirmButtonText: "好的",
									  closeOnConfirm: false,
									},
									function(isConfirm){
										if(isConfirm){
											history.go(-1);
										}
									});
								}else{
									swal("等等...!","我還有問題喔!","error");
									location.href="#";
								}
								
							}
						);
						return false;
					}
					else if(Str=="issue"){

						swal({ 
							  title: "還是找不到方法嗎?", 
							  text: "請把問題寫下來並告訴我們...謝謝!", 
							  type: "info",
							  showCancelButton: true,
							  confirmButtonColor: "#DD6B55",
							  cancelButtonText: "不，可能可以!",
							  confirmButtonText: "好的，我了解",
							  closeOnConfirm: false,
							  closeOnCancel: false
							},
							function(isConfirm){
								if(isConfirm){
									swal({ 
									  title: "那麼請在下方...", 
									  text: "按回覆鈕並填寫，感謝你!", 
									  type: "info",
									  confirmButtonColor: "#DD6B55",
									  confirmButtonText: "好的",
									  closeOnConfirm: true,
									},
									function(isConfirm){
										if(isConfirm){
											location.href="#reply_button1";
											document.getElementById('reply_button1').focus();
										}
									});
								}else{
									swal(
									  {
									  	  title: "難道...", 
										  text: "你還想在這繼續找方法嗎?", 
										  type: "info",
										  confirmButtonColor: "#DD6B55",
										  confirmButtonText: "是的，努力中",
										  closeOnConfirm: false,
									  });
								}							
							}
						);
						return false;
					}
					else if(Str=="reply"){
						document.location.href="/post/index/"+$('#userId').val()+'/helper';
						return false;
					}
					
				}