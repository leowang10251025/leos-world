$(function() {

    var loginChk = $('#loginChk').val(); //會員登入檢查
    var boardAdminLogin = $('#boardAdminLogin').val(); //管理留言板頁面檢查
    var loginAutobiography = $('#loginAutobiography').val(); //履歷編修權限登入檢查

    $('#banner_2_2_2').click(function() {
        location.href = "/home/index/2";
        if (loginChk == 'sucess') {
            location.href = "/home/haveLogin/" + $('#memberLevel').val() + "/" + $('#userId').val() + "/2";
        }
    });

    $('#banner_2_2_3').click(function() {
        location.href = "/home/index/3";
        if (loginChk == 'sucess') {
            location.href = "/home/haveLogin/" + $('#memberLevel').val() + "/" + $('#userId').val() + "/3";
        }
    });

    $('#banner_2_2_4').click(function() {
        location.href = "/home/index/4";
        if (loginChk == 'sucess') {
            location.href = "/home/haveLogin/" + $('#memberLevel').val() + "/" + $('#userId').val() + "/4";
        }
    });

    $('#banner_2_2_5').click(function() {

        if (typeof loginChk === "undefined") { //當值為undefined
            checkNaviState();
        } else {
            if (boardAdminLogin == "sucess") {
                location.href = "/message_board/index/" + $('#userId').val() + "/1/0/loginSucess/0";
            } else if (boardAdminLogin == "false") {
                location.href = "/message_board/index/" + $('#userId').val() + "/1/0/notLogin/0"
            }
        }

    });

    $('#banner_2_3').click(function() {
        location.href = "/home/index/1";
        if ($('#loginChk').val() == 'sucess') {
            location.href = "/home/haveLogin/" + $('#memberLevel').val() + "/" + $('#userId').val() + "/1";
        }
    });

    $('#banner_2_3_layer1').click(function() {
        if ($('#loginChk').val() == 'sucess') {
            location.href = "/home/haveLogin/" + $('#memberLevel').val() + "/" + $('#userId').val() + "/1";
        }
    });

    $('#banner_2_3_layer2').click(function() {
        location.href = '/post/index/' + $('#userId').val() + '/board';
    });

    $('#menuBtn_login').click(function() {
        loginMember($("#loginChk").val());
    });

    $('#menuBtn_center').click(function() {
        if ($('#memberLevel').val() == 'admin') {
            location.href = "/member_admin/index/" + $('#userId').val() + "/1";
        } else {
            location.href = "/member_center/index/" + $('#userId').val() + "/1";
        }
    });

    $('#menuBtn_forgotPw').click(function() {
        location.href = "/admin_passmail";
    });

    $('#menuBtn_applyMember').click(function() {
        location.href = "/member_join";
    });

    $('#menuBtn_adminInfo').click(function() {
        adminInfo();
    });

    $('#menuBtn_adminInfoModify').click(function() {
        location.href = "/member_admin_update/index/" + $('#userId').val();
    });

    $('#menuBtn_memberInfo').click(function() {
        memberInfo();
    });

    $('#menuBtn_memberInfoModify').click(function() {
        location.href = "/member_center_update/index/" + $('#userId').val();
    });

    $('#menuBtn_sendPw').click(function() {
        sendPassword();
    });

    $('#menuBtn_applyNotice').click(function() {
        applyNotice();
    });

    $('#menuBtn_boardAdmin').click(function() {
        loginBoardAdmin();
    });

    $('#menuBtn_autobiography').click(function() {
        $("#autobiography-pw-dialog").fadeIn(3000);
        $('#pop_menu_login').fadeOut(1000);
        $('#pop_menu_login_pet').fadeOut(1000);
        $('#pop_menu_adminInfo').fadeOut(1000);
        $('#pop_menu_memberInfo').fadeOut(1000);
        $('#pop_menu_forgotPw').fadeOut(1000);
        $('#pop_menu_applyNotice').fadeOut(1000);
        $('#pop_menu_loginBoardAdmin').fadeOut(1000);
        $('#corner_img_div').fadeOut(1000);

        if (loginAutobiography == "" || loginAutobiography == undefined) { //未登入檢驗
            $(".password-submit").on("click", function() {
                $.post("/autobiography/passwordValid", {
                        "account": $(".account-input").val(),
                        "password": $(".password-input").val()
                    },
                    function(res) {
                        if (res == 'sucess') {
                            swal({
                                title: "提醒您...!",
                                text: "登入成功，歡迎閱覽!",
                                type: "info",
                                confirmButtonColor: "#DD6B55",
                                closeOnConfirm: true
                            }, function() {
                                location.href = "/autobiography/index/1";
                            });
                        } else {
                            swal({
                                title: "請注意...!",
                                text: res,
                                type: "warning",
                                confirmButtonColor: "#DD6B55",
                                closeOnConfirm: true
                            }, function() {
                                return false;
                            });
                        }
                    }
                );
                return false;
            });
        } else { //已曾登入成功則直接轉址
            if (loginAutobiography == "sucess") {
                location.href = "/autobiography/index/1";
            }
        }
    });
});

function loginMember(loginChk) {
    $('#pop_menu_login').fadeIn(3000);
    $('#pop_menu_login_pet').fadeIn(3000);
    $('#pop_menu_adminInfo').fadeOut(1000);
    $('#pop_menu_memberInfo').fadeOut(1000);
    $('#pop_menu_forgotPw').fadeOut(1000);
    $('#pop_menu_applyNotice').fadeOut(1000);
    $('#pop_menu_loginBoardAdmin').fadeOut(1000);
    $('#corner_img_div').fadeOut(1000);
    $("#autobiography-pw-dialog").fadeOut(1000);
}

function adminInfo() {
    $('#pop_menu_adminInfo').fadeIn(3000);
    $('#pop_menu_login').fadeOut(1000);
    $('#corner_img_div').fadeOut(1000);
    $("#autobiography-pw-dialog").fadeOut(1000);
}

function memberInfo() {
    $('#pop_menu_memberInfo').fadeIn(3000);
    $('#pop_menu_login').fadeOut(1000);
    $('#corner_img_div').fadeOut(1000);
    $("#autobiography-pw-dialog").fadeOut(1000);
}

function sendPassword() {
    $('#pop_menu_forgotPw').fadeIn(3000);
    $('#pop_menu_login').fadeOut(1000);
    $('#corner_img_div').fadeOut(1000);
    $("#autobiography-pw-dialog").fadeOut(1000);
}

function applyNotice() {
    $('#pop_menu_applyNotice').fadeIn(3000);
    $('#pop_menu_login').fadeOut(1000);
    $('#corner_img_div').fadeOut(1000);
    $("#autobiography-pw-dialog").fadeOut(1000);
}

function loginBoardAdmin() {
    $('#pop_menu_loginBoardAdmin').fadeIn(3000);
    $('#pop_menu_login').fadeOut(1000);
    $('#corner_img_div').fadeOut(1000);
    $("#autobiography-pw-dialog").fadeOut(1000);
}