$(function(){

});

function checkForm(){
	var board_subject=$('#board_subject').val();
	var board_name=$('#board_name').val();
	var board_sex=$("input[type='radio'][name='board_sex']:checked").val();
	var board_mail=$('#board_mail').val();
	var board_web=$('#board_web').val();
	var board_content=CKEDITOR.instances.board_content.getData();
	var board_id=$('#board_id').val();
	
	if(board_subject == ""){
		swal(
		{ 
			title: "請注意...!", 
			text: "請填寫標題!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
		},
		function(){
			$('#board_subject').css("background-color","#D6D6FF");
		}
		);
		return false;
	}else if(board_subject.length > 35){
		swal(
		{ 
			title: "請注意...!", 
			text: "標題輸入字數請勿超過35個字!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
		},
		function(){
			$('#board_subject').css("background-color","#D6D6FF");
		}
		);
		return false;
	}
	
	if(board_name == ""){
		swal({ 
			title: "請注意...!", 
			text: "請填寫姓名!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
		},
		function(){
			$('#board_name').css("background-color","#D6D6FF");
		}
		);
		return false;
	}
	
	if(board_mail == ""){
		swal({ 
			title: "請注意...!", 
			text: "請填寫電子郵件!", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
		},
		function(){
			$('#board_mail').css("background-color","#D6D6FF");
		}
		);
		return false;
	}else{

		if(checkmail(board_mail)){ //待處理
			return false;
		}

	}

	if(board_content == ""){
		swal(
		{ 
			title: "請注意...!", 
			text: "請填寫留言內容", 
			type: "warning",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: true
		},
		function(){
			$('#board_content').css("background-color","#D6D6FF");
		}
		);
		return false;
	}else{

		swal({ 
			title: "---確認---", 
			text: "請問更新此筆資料嗎?", 
			type: "info",
			confirmButtonColor: "#DD6B55",
			showCancelButton: true,
			confirmButtonText: '確定更新',
			cancelButton: '取消',
			cancelButtonText: '取消更新',
			closeOnConfirm: false,
			closeOnCancel: false
		},
		function(isConfirm){

			if(isConfirm){

				swal({ 
				title: "請注意...!", 
				text: "已更新!", 
				type: "success",
				confirmButtonColor: "#DD6B55",
				closeOnConfirm: true
				},
				function(){
					$('#formAdminFix').submit();
				}
				);
				return true;

			}else{
				swal("---確認---","已取消!","error");
				return false;
			}

		});

		return false;

	}

	return false;
}

function checkmail(myMail){
	var filter=/^([a-z0-9_\.\-])+\@([a-z0-9\-]+\.)+([a-z0-9\-]{2,4})+$/;
	if(filter.test(myMail.value)){
		return true;
	}

	swal({ 
		title: "請注意...!", 
		text: "電子郵件格式不正確!", 
		type: "warning",
		confirmButtonColor: "#DD6B55",
		closeOnConfirm: true
		},
		function(){
			$('#board_mail').css("background-color","#D6D6FF");
		}
	);
	return false;
}