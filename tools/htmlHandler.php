<?php

	class HtmlHandler{

		function __construct(){
			
		}

		function print_htmlContent($fileName){
			$content="";
			if(file_exists($fileName)){
				$file=fopen($fileName, "r");
				if($file!=null){
					while(!feof($file)){
						$content.=fgets($file);
					}
				}
			}

			return $content;
		}
	}

?>