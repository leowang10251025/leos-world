(function($) {
    var indexCtrl = function() {
        var shift = false;
        var capslock = false;
        //找到input文字框
        var keyUpdate = function() {
                $write = $(this);
            }
            //點選key小圖示虛擬鍵盤進行隱藏或顯示
        var keyToggle = function() {
                if ($('.keys').hasClass('temp_hidden')) {
                    $('.keys').removeClass('temp_hidden');
                } else {
                    $('.keys').addClass('temp_hidden');
                }
            }
            //點選鍵盤按鍵的處理
        var keyOperate = function() {
            var $this = $(this);
            character = $this.html();
            // 點選Shift鍵進行大小寫的處理
            if ($this.hasClass('left-shift') || $this.hasClass('right-shift')) {
                $('.letter').toggleClass('uppercase');
                $('.symbol span').toggle();
                shift = (shift === true) ? false : true;
                capslock = false;
                return false;
            }
            // 點選Caps鍵進行大寫鎖定和解除大寫鎖定的處理
            if ($this.hasClass('capslock')) {
                $('.letter').toggleClass('uppercase');
                capslock = true;
                return false;
            }
            // 點選Delete鍵進行刪除處理
            if ($this.hasClass('delete')) {
                var html = $write.val();
                $write.val(html.substr(0, html.length - 1));
                return false;
            }
            // 特殊鍵的處理
            if ($this.hasClass('symbol')) character = $('span:visible', $this).html();
            if ($this.hasClass('space')) character = ' ';
            if ($this.hasClass('tab')) character = "\t";
            if ($this.hasClass('return')) character = "\n";
            // Uppercase letter
            if ($this.hasClass('uppercase')) character = character.toUpperCase();
            // Remove shift once a key is clicked.
            if (shift === true) {
                $('.symbol span').toggle();
                if (capslock === false) $('.letter').toggleClass('uppercase');
                shift = false;
            }
            // 點選後的字元拼接
            $write.val($write.val() + character);
        }
        var loadEvent = function() {
            $('.keyboard').bind("click", keyUpdate);
            $("#key_icon").bind("click", keyToggle);
            $("#container li").bind("click", keyOperate);
        }
        return {
            init: function() {
                loadEvent();
            }
        }
    }();
    indexCtrl.init();
});